    int lda, ldb, ldc;
    gen_leading_dim<TransformA>( m, k, lda );
    gen_leading_dim<TransformB>( k, n, ldb );
    gen_leading_dim<matrix_transform_t::NonTranspose>( m, n, ldc );
    int ms, ks, ns;
    int md, kd, nd;
    int mr, kr, nr;
    mr = m % ( 4 ), kr = k % ( 4 ), nr = n % ( 4 );
    md = m - mr, kd = k - kr, nd = n - nr;
    ms=md, ks=kd, ns=nd;
    value_t *d_a_0, *d_a_1, *d_a_2, *d_a_3;
    stra4_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 0, d_a, &d_a_0 );
    stra4_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 1, d_a, &d_a_1 );
    stra4_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 0, d_a, &d_a_2 );
    stra4_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 1, d_a, &d_a_3 );
    ms=ms/2, ks=ks/2, ns=ns/2;
    value_t *d_a_0_0, *d_a_0_1, *d_a_0_2, *d_a_0_3;
    stra4_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 0, d_a_0, &d_a_0_0 );
    stra4_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 1, d_a_0, &d_a_0_1 );
    stra4_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 0, d_a_0, &d_a_0_2 );
    stra4_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 1, d_a_0, &d_a_0_3 );
    value_t *d_a_1_0, *d_a_1_1, *d_a_1_2, *d_a_1_3;
    stra4_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 0, d_a_1, &d_a_1_0 );
    stra4_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 1, d_a_1, &d_a_1_1 );
    stra4_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 0, d_a_1, &d_a_1_2 );
    stra4_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 1, d_a_1, &d_a_1_3 );
    value_t *d_a_2_0, *d_a_2_1, *d_a_2_2, *d_a_2_3;
    stra4_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 0, d_a_2, &d_a_2_0 );
    stra4_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 1, d_a_2, &d_a_2_1 );
    stra4_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 0, d_a_2, &d_a_2_2 );
    stra4_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 1, d_a_2, &d_a_2_3 );
    value_t *d_a_3_0, *d_a_3_1, *d_a_3_2, *d_a_3_3;
    stra4_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 0, d_a_3, &d_a_3_0 );
    stra4_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 1, d_a_3, &d_a_3_1 );
    stra4_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 0, d_a_3, &d_a_3_2 );
    stra4_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 1, d_a_3, &d_a_3_3 );
    ms=md, ks=kd, ns=nd;
    value_t *d_b_0, *d_b_1, *d_b_2, *d_b_3;
    stra4_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 0, d_b, &d_b_0 );
    stra4_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 1, d_b, &d_b_1 );
    stra4_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 0, d_b, &d_b_2 );
    stra4_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 1, d_b, &d_b_3 );
    ms=ms/2, ks=ks/2, ns=ns/2;
    value_t *d_b_0_0, *d_b_0_1, *d_b_0_2, *d_b_0_3;
    stra4_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 0, d_b_0, &d_b_0_0 );
    stra4_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 1, d_b_0, &d_b_0_1 );
    stra4_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 0, d_b_0, &d_b_0_2 );
    stra4_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 1, d_b_0, &d_b_0_3 );
    value_t *d_b_1_0, *d_b_1_1, *d_b_1_2, *d_b_1_3;
    stra4_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 0, d_b_1, &d_b_1_0 );
    stra4_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 1, d_b_1, &d_b_1_1 );
    stra4_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 0, d_b_1, &d_b_1_2 );
    stra4_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 1, d_b_1, &d_b_1_3 );
    value_t *d_b_2_0, *d_b_2_1, *d_b_2_2, *d_b_2_3;
    stra4_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 0, d_b_2, &d_b_2_0 );
    stra4_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 1, d_b_2, &d_b_2_1 );
    stra4_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 0, d_b_2, &d_b_2_2 );
    stra4_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 1, d_b_2, &d_b_2_3 );
    value_t *d_b_3_0, *d_b_3_1, *d_b_3_2, *d_b_3_3;
    stra4_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 0, d_b_3, &d_b_3_0 );
    stra4_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 1, d_b_3, &d_b_3_1 );
    stra4_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 0, d_b_3, &d_b_3_2 );
    stra4_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 1, d_b_3, &d_b_3_3 );
    ms=md, ks=kd, ns=nd;
    accum_t *d_c_0, *d_c_1, *d_c_2, *d_c_3;
    stra4_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 0, d_c, &d_c_0 );
    stra4_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 1, d_c, &d_c_1 );
    stra4_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 0, d_c, &d_c_2 );
    stra4_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 1, d_c, &d_c_3 );
    ms=ms/2, ks=ks/2, ns=ns/2;
    accum_t *d_c_0_0, *d_c_0_1, *d_c_0_2, *d_c_0_3;
    stra4_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 0, d_c_0, &d_c_0_0 );
    stra4_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 1, d_c_0, &d_c_0_1 );
    stra4_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 0, d_c_0, &d_c_0_2 );
    stra4_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 1, d_c_0, &d_c_0_3 );
    accum_t *d_c_1_0, *d_c_1_1, *d_c_1_2, *d_c_1_3;
    stra4_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 0, d_c_1, &d_c_1_0 );
    stra4_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 1, d_c_1, &d_c_1_1 );
    stra4_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 0, d_c_1, &d_c_1_2 );
    stra4_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 1, d_c_1, &d_c_1_3 );
    accum_t *d_c_2_0, *d_c_2_1, *d_c_2_2, *d_c_2_3;
    stra4_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 0, d_c_2, &d_c_2_0 );
    stra4_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 1, d_c_2, &d_c_2_1 );
    stra4_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 0, d_c_2, &d_c_2_2 );
    stra4_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 1, d_c_2, &d_c_2_3 );
    accum_t *d_c_3_0, *d_c_3_1, *d_c_3_2, *d_c_3_3;
    stra4_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 0, d_c_3, &d_c_3_0 );
    stra4_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 1, d_c_3, &d_c_3_1 );
    stra4_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 0, d_c_3, &d_c_3_2 );
    stra4_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 1, d_c_3, &d_c_3_3 );
    ms=ms/2, ks=ks/2, ns=ns/2;


    // M0 = (1.0 * d_a_0_0 + 1.0 * d_a_0_3 + 1.0 * d_a_3_0 + 1.0 * d_a_3_3) * (1.0 * d_b_0_0 + 1.0 * d_b_0_3 + 1.0 * d_b_3_0 + 1.0 * d_b_3_3);  d_c_0_0 += 1.0 * M0;  d_c_0_3 += 1.0 * M0;  d_c_3_0 += 1.0 * M0;  d_c_3_3 += 1.0 * M0;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_0( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0_0, d_a_0_3, d_a_3_0, d_a_3_3, d_b_0_0, d_b_0_3, d_b_3_0, d_b_3_3, d_c_0_0, d_c_0_3, d_c_3_0, d_c_3_3 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_0);

    // M1 = (1.0 * d_a_0_2 + 1.0 * d_a_0_3 + 1.0 * d_a_3_2 + 1.0 * d_a_3_3) * (1.0 * d_b_0_0 + 1.0 * d_b_3_0);  d_c_0_2 += 1.0 * M1;  d_c_0_3 += -1.0 * M1;  d_c_3_2 += 1.0 * M1;  d_c_3_3 += -1.0 * M1;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_1( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0_2, d_a_0_3, d_a_3_2, d_a_3_3, d_b_0_0, d_b_3_0, NULL, NULL, d_c_0_2, d_c_3_2, d_c_0_3, d_c_3_3 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 1, 1, 1, 0, 0, 1, 1, -1, -1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_1);

    // M2 = (1.0 * d_a_0_0 + 1.0 * d_a_3_0) * (1.0 * d_b_0_1 + -1.0 * d_b_0_3 + 1.0 * d_b_3_1 + -1.0 * d_b_3_3);  d_c_0_1 += 1.0 * M2;  d_c_0_3 += 1.0 * M2;  d_c_3_1 += 1.0 * M2;  d_c_3_3 += 1.0 * M2;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_2( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0_0, d_a_3_0, NULL, NULL, d_b_0_1, d_b_3_1, d_b_0_3, d_b_3_3, d_c_0_1, d_c_0_3, d_c_3_1, d_c_3_3 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 0, 0, 1, -1, -1, 1, 1, 1, 1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_2);

    // M3 = (1.0 * d_a_0_3 + 1.0 * d_a_3_3) * (-1.0 * d_b_0_0 + 1.0 * d_b_0_2 + -1.0 * d_b_3_0 + 1.0 * d_b_3_2);  d_c_0_0 += 1.0 * M3;  d_c_0_2 += 1.0 * M3;  d_c_3_0 += 1.0 * M3;  d_c_3_2 += 1.0 * M3;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_3( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0_3, d_a_3_3, NULL, NULL, d_b_0_2, d_b_3_2, d_b_0_0, d_b_3_0, d_c_0_0, d_c_0_2, d_c_3_0, d_c_3_2 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 0, 0, 1, -1, -1, 1, 1, 1, 1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_3);

    // M4 = (1.0 * d_a_0_0 + 1.0 * d_a_0_1 + 1.0 * d_a_3_0 + 1.0 * d_a_3_1) * (1.0 * d_b_0_3 + 1.0 * d_b_3_3);  d_c_0_0 += -1.0 * M4;  d_c_0_1 += 1.0 * M4;  d_c_3_0 += -1.0 * M4;  d_c_3_1 += 1.0 * M4;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_4( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0_0, d_a_0_1, d_a_3_0, d_a_3_1, d_b_0_3, d_b_3_3, NULL, NULL, d_c_0_1, d_c_3_1, d_c_0_0, d_c_3_0 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 1, 1, 1, 0, 0, 1, 1, -1, -1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_4);

    // M5 = (-1.0 * d_a_0_0 + 1.0 * d_a_0_2 + -1.0 * d_a_3_0 + 1.0 * d_a_3_2) * (1.0 * d_b_0_0 + 1.0 * d_b_0_1 + 1.0 * d_b_3_0 + 1.0 * d_b_3_1);  d_c_0_3 += 1.0 * M5;  d_c_3_3 += 1.0 * M5;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_5( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0_2, d_a_3_2, d_a_0_0, d_a_3_0, d_b_0_0, d_b_0_1, d_b_3_0, d_b_3_1, d_c_0_3, d_c_3_3, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, -1, -1, 1, 1, 1, 1, 1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_5);

    // M6 = (1.0 * d_a_0_1 + -1.0 * d_a_0_3 + 1.0 * d_a_3_1 + -1.0 * d_a_3_3) * (1.0 * d_b_0_2 + 1.0 * d_b_0_3 + 1.0 * d_b_3_2 + 1.0 * d_b_3_3);  d_c_0_0 += 1.0 * M6;  d_c_3_0 += 1.0 * M6;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_6( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0_1, d_a_3_1, d_a_0_3, d_a_3_3, d_b_0_2, d_b_0_3, d_b_3_2, d_b_3_3, d_c_0_0, d_c_3_0, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, -1, -1, 1, 1, 1, 1, 1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_6);

    // M7 = (1.0 * d_a_2_0 + 1.0 * d_a_2_3 + 1.0 * d_a_3_0 + 1.0 * d_a_3_3) * (1.0 * d_b_0_0 + 1.0 * d_b_0_3);  d_c_2_0 += 1.0 * M7;  d_c_2_3 += 1.0 * M7;  d_c_3_0 += -1.0 * M7;  d_c_3_3 += -1.0 * M7;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_7( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_2_0, d_a_2_3, d_a_3_0, d_a_3_3, d_b_0_0, d_b_0_3, NULL, NULL, d_c_2_0, d_c_2_3, d_c_3_0, d_c_3_3 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 1, 1, 1, 0, 0, 1, 1, -1, -1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_7);

    // M8 = (1.0 * d_a_2_2 + 1.0 * d_a_2_3 + 1.0 * d_a_3_2 + 1.0 * d_a_3_3) * (1.0 * d_b_0_0);  d_c_2_2 += 1.0 * M8;  d_c_2_3 += -1.0 * M8;  d_c_3_2 += -1.0 * M8;  d_c_3_3 += 1.0 * M8;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_8( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_2_2, d_a_2_3, d_a_3_2, d_a_3_3, d_b_0_0, NULL, NULL, NULL, d_c_2_2, d_c_3_3, d_c_2_3, d_c_3_2 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 1, 1, 0, 0, 0, 1, 1, -1, -1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_8);

    // M9 = (1.0 * d_a_2_0 + 1.0 * d_a_3_0) * (1.0 * d_b_0_1 + -1.0 * d_b_0_3);  d_c_2_1 += 1.0 * M9;  d_c_2_3 += 1.0 * M9;  d_c_3_1 += -1.0 * M9;  d_c_3_3 += -1.0 * M9;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_9( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_2_0, d_a_3_0, NULL, NULL, d_b_0_1, d_b_0_3, NULL, NULL, d_c_2_1, d_c_2_3, d_c_3_1, d_c_3_3 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 0, 0, -1, 0, 0, 1, 1, -1, -1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_9);

    // M10 = (1.0 * d_a_2_3 + 1.0 * d_a_3_3) * (-1.0 * d_b_0_0 + 1.0 * d_b_0_2);  d_c_2_0 += 1.0 * M10;  d_c_2_2 += 1.0 * M10;  d_c_3_0 += -1.0 * M10;  d_c_3_2 += -1.0 * M10;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_10( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_2_3, d_a_3_3, NULL, NULL, d_b_0_2, d_b_0_0, NULL, NULL, d_c_2_0, d_c_2_2, d_c_3_0, d_c_3_2 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 0, 0, -1, 0, 0, 1, 1, -1, -1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_10);

    // M11 = (1.0 * d_a_2_0 + 1.0 * d_a_2_1 + 1.0 * d_a_3_0 + 1.0 * d_a_3_1) * (1.0 * d_b_0_3);  d_c_2_0 += -1.0 * M11;  d_c_2_1 += 1.0 * M11;  d_c_3_0 += 1.0 * M11;  d_c_3_1 += -1.0 * M11;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_11( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_2_0, d_a_2_1, d_a_3_0, d_a_3_1, d_b_0_3, NULL, NULL, NULL, d_c_2_1, d_c_3_0, d_c_2_0, d_c_3_1 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 1, 1, 0, 0, 0, 1, 1, -1, -1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_11);

    // M12 = (-1.0 * d_a_2_0 + 1.0 * d_a_2_2 + -1.0 * d_a_3_0 + 1.0 * d_a_3_2) * (1.0 * d_b_0_0 + 1.0 * d_b_0_1);  d_c_2_3 += 1.0 * M12;  d_c_3_3 += -1.0 * M12;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_12( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_2_2, d_a_3_2, d_a_2_0, d_a_3_0, d_b_0_0, d_b_0_1, NULL, NULL, d_c_2_3, d_c_3_3, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, -1, -1, 1, 0, 0, 1, -1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_12);

    // M13 = (1.0 * d_a_2_1 + -1.0 * d_a_2_3 + 1.0 * d_a_3_1 + -1.0 * d_a_3_3) * (1.0 * d_b_0_2 + 1.0 * d_b_0_3);  d_c_2_0 += 1.0 * M13;  d_c_3_0 += -1.0 * M13;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_13( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_2_1, d_a_3_1, d_a_2_3, d_a_3_3, d_b_0_2, d_b_0_3, NULL, NULL, d_c_2_0, d_c_3_0, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, -1, -1, 1, 0, 0, 1, -1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_13);

    // M14 = (1.0 * d_a_0_0 + 1.0 * d_a_0_3) * (1.0 * d_b_1_0 + 1.0 * d_b_1_3 + -1.0 * d_b_3_0 + -1.0 * d_b_3_3);  d_c_1_0 += 1.0 * M14;  d_c_1_3 += 1.0 * M14;  d_c_3_0 += 1.0 * M14;  d_c_3_3 += 1.0 * M14;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_14( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0_0, d_a_0_3, NULL, NULL, d_b_1_0, d_b_1_3, d_b_3_0, d_b_3_3, d_c_1_0, d_c_1_3, d_c_3_0, d_c_3_3 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 0, 0, 1, -1, -1, 1, 1, 1, 1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_14);

    // M15 = (1.0 * d_a_0_2 + 1.0 * d_a_0_3) * (1.0 * d_b_1_0 + -1.0 * d_b_3_0);  d_c_1_2 += 1.0 * M15;  d_c_1_3 += -1.0 * M15;  d_c_3_2 += 1.0 * M15;  d_c_3_3 += -1.0 * M15;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_15( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0_2, d_a_0_3, NULL, NULL, d_b_1_0, d_b_3_0, NULL, NULL, d_c_1_2, d_c_3_2, d_c_1_3, d_c_3_3 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 0, 0, -1, 0, 0, 1, 1, -1, -1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_15);

    // M16 = (1.0 * d_a_0_0) * (1.0 * d_b_1_1 + -1.0 * d_b_1_3 + -1.0 * d_b_3_1 + 1.0 * d_b_3_3);  d_c_1_1 += 1.0 * M16;  d_c_1_3 += 1.0 * M16;  d_c_3_1 += 1.0 * M16;  d_c_3_3 += 1.0 * M16;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_16( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0_0, NULL, NULL, NULL, d_b_1_1, d_b_3_3, d_b_1_3, d_b_3_1, d_c_1_1, d_c_1_3, d_c_3_1, d_c_3_3 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 0, 0, 0, 1, -1, -1, 1, 1, 1, 1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_16);

    // M17 = (1.0 * d_a_0_3) * (-1.0 * d_b_1_0 + 1.0 * d_b_1_2 + 1.0 * d_b_3_0 + -1.0 * d_b_3_2);  d_c_1_0 += 1.0 * M17;  d_c_1_2 += 1.0 * M17;  d_c_3_0 += 1.0 * M17;  d_c_3_2 += 1.0 * M17;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_17( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0_3, NULL, NULL, NULL, d_b_1_2, d_b_3_0, d_b_1_0, d_b_3_2, d_c_1_0, d_c_1_2, d_c_3_0, d_c_3_2 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 0, 0, 0, 1, -1, -1, 1, 1, 1, 1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_17);

    // M18 = (1.0 * d_a_0_0 + 1.0 * d_a_0_1) * (1.0 * d_b_1_3 + -1.0 * d_b_3_3);  d_c_1_0 += -1.0 * M18;  d_c_1_1 += 1.0 * M18;  d_c_3_0 += -1.0 * M18;  d_c_3_1 += 1.0 * M18;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_18( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0_0, d_a_0_1, NULL, NULL, d_b_1_3, d_b_3_3, NULL, NULL, d_c_1_1, d_c_3_1, d_c_1_0, d_c_3_0 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 0, 0, -1, 0, 0, 1, 1, -1, -1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_18);

    // M19 = (-1.0 * d_a_0_0 + 1.0 * d_a_0_2) * (1.0 * d_b_1_0 + 1.0 * d_b_1_1 + -1.0 * d_b_3_0 + -1.0 * d_b_3_1);  d_c_1_3 += 1.0 * M19;  d_c_3_3 += 1.0 * M19;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_19( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0_2, d_a_0_0, NULL, NULL, d_b_1_0, d_b_1_1, d_b_3_0, d_b_3_1, d_c_1_3, d_c_3_3, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, -1, 0, 0, 1, -1, -1, 1, 1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_19);

    // M20 = (1.0 * d_a_0_1 + -1.0 * d_a_0_3) * (1.0 * d_b_1_2 + 1.0 * d_b_1_3 + -1.0 * d_b_3_2 + -1.0 * d_b_3_3);  d_c_1_0 += 1.0 * M20;  d_c_3_0 += 1.0 * M20;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_20( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0_1, d_a_0_3, NULL, NULL, d_b_1_2, d_b_1_3, d_b_3_2, d_b_3_3, d_c_1_0, d_c_3_0, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, -1, 0, 0, 1, -1, -1, 1, 1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_20);

    // M21 = (1.0 * d_a_3_0 + 1.0 * d_a_3_3) * (-1.0 * d_b_0_0 + -1.0 * d_b_0_3 + 1.0 * d_b_2_0 + 1.0 * d_b_2_3);  d_c_0_0 += 1.0 * M21;  d_c_0_3 += 1.0 * M21;  d_c_2_0 += 1.0 * M21;  d_c_2_3 += 1.0 * M21;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_21( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_3_0, d_a_3_3, NULL, NULL, d_b_2_0, d_b_2_3, d_b_0_0, d_b_0_3, d_c_0_0, d_c_0_3, d_c_2_0, d_c_2_3 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 0, 0, 1, -1, -1, 1, 1, 1, 1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_21);

    // M22 = (1.0 * d_a_3_2 + 1.0 * d_a_3_3) * (-1.0 * d_b_0_0 + 1.0 * d_b_2_0);  d_c_0_2 += 1.0 * M22;  d_c_0_3 += -1.0 * M22;  d_c_2_2 += 1.0 * M22;  d_c_2_3 += -1.0 * M22;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_22( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_3_2, d_a_3_3, NULL, NULL, d_b_2_0, d_b_0_0, NULL, NULL, d_c_0_2, d_c_2_2, d_c_0_3, d_c_2_3 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 0, 0, -1, 0, 0, 1, 1, -1, -1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_22);

    // M23 = (1.0 * d_a_3_0) * (-1.0 * d_b_0_1 + 1.0 * d_b_0_3 + 1.0 * d_b_2_1 + -1.0 * d_b_2_3);  d_c_0_1 += 1.0 * M23;  d_c_0_3 += 1.0 * M23;  d_c_2_1 += 1.0 * M23;  d_c_2_3 += 1.0 * M23;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_23( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_3_0, NULL, NULL, NULL, d_b_0_3, d_b_2_1, d_b_0_1, d_b_2_3, d_c_0_1, d_c_0_3, d_c_2_1, d_c_2_3 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 0, 0, 0, 1, -1, -1, 1, 1, 1, 1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_23);

    // M24 = (1.0 * d_a_3_3) * (1.0 * d_b_0_0 + -1.0 * d_b_0_2 + -1.0 * d_b_2_0 + 1.0 * d_b_2_2);  d_c_0_0 += 1.0 * M24;  d_c_0_2 += 1.0 * M24;  d_c_2_0 += 1.0 * M24;  d_c_2_2 += 1.0 * M24;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_24( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_3_3, NULL, NULL, NULL, d_b_0_0, d_b_2_2, d_b_0_2, d_b_2_0, d_c_0_0, d_c_0_2, d_c_2_0, d_c_2_2 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 0, 0, 0, 1, -1, -1, 1, 1, 1, 1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_24);

    // M25 = (1.0 * d_a_3_0 + 1.0 * d_a_3_1) * (-1.0 * d_b_0_3 + 1.0 * d_b_2_3);  d_c_0_0 += -1.0 * M25;  d_c_0_1 += 1.0 * M25;  d_c_2_0 += -1.0 * M25;  d_c_2_1 += 1.0 * M25;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_25( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_3_0, d_a_3_1, NULL, NULL, d_b_2_3, d_b_0_3, NULL, NULL, d_c_0_1, d_c_2_1, d_c_0_0, d_c_2_0 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 0, 0, -1, 0, 0, 1, 1, -1, -1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_25);

    // M26 = (-1.0 * d_a_3_0 + 1.0 * d_a_3_2) * (-1.0 * d_b_0_0 + -1.0 * d_b_0_1 + 1.0 * d_b_2_0 + 1.0 * d_b_2_1);  d_c_0_3 += 1.0 * M26;  d_c_2_3 += 1.0 * M26;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_26( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_3_2, d_a_3_0, NULL, NULL, d_b_2_0, d_b_2_1, d_b_0_0, d_b_0_1, d_c_0_3, d_c_2_3, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, -1, 0, 0, 1, -1, -1, 1, 1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_26);

    // M27 = (1.0 * d_a_3_1 + -1.0 * d_a_3_3) * (-1.0 * d_b_0_2 + -1.0 * d_b_0_3 + 1.0 * d_b_2_2 + 1.0 * d_b_2_3);  d_c_0_0 += 1.0 * M27;  d_c_2_0 += 1.0 * M27;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_27( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_3_1, d_a_3_3, NULL, NULL, d_b_2_2, d_b_2_3, d_b_0_2, d_b_0_3, d_c_0_0, d_c_2_0, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, -1, 0, 0, 1, -1, -1, 1, 1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_27);

    // M28 = (1.0 * d_a_0_0 + 1.0 * d_a_0_3 + 1.0 * d_a_1_0 + 1.0 * d_a_1_3) * (1.0 * d_b_3_0 + 1.0 * d_b_3_3);  d_c_0_0 += -1.0 * M28;  d_c_0_3 += -1.0 * M28;  d_c_1_0 += 1.0 * M28;  d_c_1_3 += 1.0 * M28;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_28( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0_0, d_a_0_3, d_a_1_0, d_a_1_3, d_b_3_0, d_b_3_3, NULL, NULL, d_c_1_0, d_c_1_3, d_c_0_0, d_c_0_3 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 1, 1, 1, 0, 0, 1, 1, -1, -1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_28);

    // M29 = (1.0 * d_a_0_2 + 1.0 * d_a_0_3 + 1.0 * d_a_1_2 + 1.0 * d_a_1_3) * (1.0 * d_b_3_0);  d_c_0_2 += -1.0 * M29;  d_c_0_3 += 1.0 * M29;  d_c_1_2 += 1.0 * M29;  d_c_1_3 += -1.0 * M29;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_29( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0_2, d_a_0_3, d_a_1_2, d_a_1_3, d_b_3_0, NULL, NULL, NULL, d_c_0_3, d_c_1_2, d_c_0_2, d_c_1_3 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 1, 1, 0, 0, 0, 1, 1, -1, -1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_29);

    // M30 = (1.0 * d_a_0_0 + 1.0 * d_a_1_0) * (1.0 * d_b_3_1 + -1.0 * d_b_3_3);  d_c_0_1 += -1.0 * M30;  d_c_0_3 += -1.0 * M30;  d_c_1_1 += 1.0 * M30;  d_c_1_3 += 1.0 * M30;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_30( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0_0, d_a_1_0, NULL, NULL, d_b_3_1, d_b_3_3, NULL, NULL, d_c_1_1, d_c_1_3, d_c_0_1, d_c_0_3 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 0, 0, -1, 0, 0, 1, 1, -1, -1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_30);

    // M31 = (1.0 * d_a_0_3 + 1.0 * d_a_1_3) * (-1.0 * d_b_3_0 + 1.0 * d_b_3_2);  d_c_0_0 += -1.0 * M31;  d_c_0_2 += -1.0 * M31;  d_c_1_0 += 1.0 * M31;  d_c_1_2 += 1.0 * M31;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_31( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0_3, d_a_1_3, NULL, NULL, d_b_3_2, d_b_3_0, NULL, NULL, d_c_1_0, d_c_1_2, d_c_0_0, d_c_0_2 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 0, 0, -1, 0, 0, 1, 1, -1, -1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_31);

    // M32 = (1.0 * d_a_0_0 + 1.0 * d_a_0_1 + 1.0 * d_a_1_0 + 1.0 * d_a_1_1) * (1.0 * d_b_3_3);  d_c_0_0 += 1.0 * M32;  d_c_0_1 += -1.0 * M32;  d_c_1_0 += -1.0 * M32;  d_c_1_1 += 1.0 * M32;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_32( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0_0, d_a_0_1, d_a_1_0, d_a_1_1, d_b_3_3, NULL, NULL, NULL, d_c_0_0, d_c_1_1, d_c_0_1, d_c_1_0 );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 1, 1, 0, 0, 0, 1, 1, -1, -1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_32);

    // M33 = (-1.0 * d_a_0_0 + 1.0 * d_a_0_2 + -1.0 * d_a_1_0 + 1.0 * d_a_1_2) * (1.0 * d_b_3_0 + 1.0 * d_b_3_1);  d_c_0_3 += -1.0 * M33;  d_c_1_3 += 1.0 * M33;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_33( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0_2, d_a_1_2, d_a_0_0, d_a_1_0, d_b_3_0, d_b_3_1, NULL, NULL, d_c_1_3, d_c_0_3, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, -1, -1, 1, 0, 0, 1, -1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_33);

    // M34 = (1.0 * d_a_0_1 + -1.0 * d_a_0_3 + 1.0 * d_a_1_1 + -1.0 * d_a_1_3) * (1.0 * d_b_3_2 + 1.0 * d_b_3_3);  d_c_0_0 += -1.0 * M34;  d_c_1_0 += 1.0 * M34;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_34( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0_1, d_a_1_1, d_a_0_3, d_a_1_3, d_b_3_2, d_b_3_3, NULL, NULL, d_c_1_0, d_c_0_0, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, -1, -1, 1, 0, 0, 1, -1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_34);

    // M35 = (-1.0 * d_a_0_0 + -1.0 * d_a_0_3 + 1.0 * d_a_2_0 + 1.0 * d_a_2_3) * (1.0 * d_b_0_0 + 1.0 * d_b_0_3 + 1.0 * d_b_1_0 + 1.0 * d_b_1_3);  d_c_3_0 += 1.0 * M35;  d_c_3_3 += 1.0 * M35;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_35( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_2_0, d_a_2_3, d_a_0_0, d_a_0_3, d_b_0_0, d_b_0_3, d_b_1_0, d_b_1_3, d_c_3_0, d_c_3_3, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, -1, -1, 1, 1, 1, 1, 1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_35);

    // M36 = (-1.0 * d_a_0_2 + -1.0 * d_a_0_3 + 1.0 * d_a_2_2 + 1.0 * d_a_2_3) * (1.0 * d_b_0_0 + 1.0 * d_b_1_0);  d_c_3_2 += 1.0 * M36;  d_c_3_3 += -1.0 * M36;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_36( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_2_2, d_a_2_3, d_a_0_2, d_a_0_3, d_b_0_0, d_b_1_0, NULL, NULL, d_c_3_2, d_c_3_3, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, -1, -1, 1, 0, 0, 1, -1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_36);

    // M37 = (-1.0 * d_a_0_0 + 1.0 * d_a_2_0) * (1.0 * d_b_0_1 + -1.0 * d_b_0_3 + 1.0 * d_b_1_1 + -1.0 * d_b_1_3);  d_c_3_1 += 1.0 * M37;  d_c_3_3 += 1.0 * M37;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_37( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_2_0, d_a_0_0, NULL, NULL, d_b_0_1, d_b_1_1, d_b_0_3, d_b_1_3, d_c_3_1, d_c_3_3, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, -1, 0, 0, 1, -1, -1, 1, 1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_37);

    // M38 = (-1.0 * d_a_0_3 + 1.0 * d_a_2_3) * (-1.0 * d_b_0_0 + 1.0 * d_b_0_2 + -1.0 * d_b_1_0 + 1.0 * d_b_1_2);  d_c_3_0 += 1.0 * M38;  d_c_3_2 += 1.0 * M38;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_38( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_2_3, d_a_0_3, NULL, NULL, d_b_0_2, d_b_1_2, d_b_0_0, d_b_1_0, d_c_3_0, d_c_3_2, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, -1, 0, 0, 1, -1, -1, 1, 1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_38);

    // M39 = (-1.0 * d_a_0_0 + -1.0 * d_a_0_1 + 1.0 * d_a_2_0 + 1.0 * d_a_2_1) * (1.0 * d_b_0_3 + 1.0 * d_b_1_3);  d_c_3_0 += -1.0 * M39;  d_c_3_1 += 1.0 * M39;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_39( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_2_0, d_a_2_1, d_a_0_0, d_a_0_1, d_b_0_3, d_b_1_3, NULL, NULL, d_c_3_1, d_c_3_0, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, -1, -1, 1, 0, 0, 1, -1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_39);

    // M40 = (1.0 * d_a_0_0 + -1.0 * d_a_0_2 + -1.0 * d_a_2_0 + 1.0 * d_a_2_2) * (1.0 * d_b_0_0 + 1.0 * d_b_0_1 + 1.0 * d_b_1_0 + 1.0 * d_b_1_1);  d_c_3_3 += 1.0 * M40;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_40( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0_0, d_a_2_2, d_a_0_2, d_a_2_0, d_b_0_0, d_b_0_1, d_b_1_0, d_b_1_1, d_c_3_3, NULL, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, -1, -1, 1, 1, 1, 1, 0, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_40);

    // M41 = (-1.0 * d_a_0_1 + 1.0 * d_a_0_3 + 1.0 * d_a_2_1 + -1.0 * d_a_2_3) * (1.0 * d_b_0_2 + 1.0 * d_b_0_3 + 1.0 * d_b_1_2 + 1.0 * d_b_1_3);  d_c_3_0 += 1.0 * M41;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_41( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0_3, d_a_2_1, d_a_0_1, d_a_2_3, d_b_0_2, d_b_0_3, d_b_1_2, d_b_1_3, d_c_3_0, NULL, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, -1, -1, 1, 1, 1, 1, 0, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_41);

    // M42 = (1.0 * d_a_1_0 + 1.0 * d_a_1_3 + -1.0 * d_a_3_0 + -1.0 * d_a_3_3) * (1.0 * d_b_2_0 + 1.0 * d_b_2_3 + 1.0 * d_b_3_0 + 1.0 * d_b_3_3);  d_c_0_0 += 1.0 * M42;  d_c_0_3 += 1.0 * M42;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_42( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_1_0, d_a_1_3, d_a_3_0, d_a_3_3, d_b_2_0, d_b_2_3, d_b_3_0, d_b_3_3, d_c_0_0, d_c_0_3, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, -1, -1, 1, 1, 1, 1, 1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_42);

    // M43 = (1.0 * d_a_1_2 + 1.0 * d_a_1_3 + -1.0 * d_a_3_2 + -1.0 * d_a_3_3) * (1.0 * d_b_2_0 + 1.0 * d_b_3_0);  d_c_0_2 += 1.0 * M43;  d_c_0_3 += -1.0 * M43;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_43( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_1_2, d_a_1_3, d_a_3_2, d_a_3_3, d_b_2_0, d_b_3_0, NULL, NULL, d_c_0_2, d_c_0_3, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, -1, -1, 1, 0, 0, 1, -1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_43);

    // M44 = (1.0 * d_a_1_0 + -1.0 * d_a_3_0) * (1.0 * d_b_2_1 + -1.0 * d_b_2_3 + 1.0 * d_b_3_1 + -1.0 * d_b_3_3);  d_c_0_1 += 1.0 * M44;  d_c_0_3 += 1.0 * M44;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_44( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_1_0, d_a_3_0, NULL, NULL, d_b_2_1, d_b_3_1, d_b_2_3, d_b_3_3, d_c_0_1, d_c_0_3, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, -1, 0, 0, 1, -1, -1, 1, 1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_44);

    // M45 = (1.0 * d_a_1_3 + -1.0 * d_a_3_3) * (-1.0 * d_b_2_0 + 1.0 * d_b_2_2 + -1.0 * d_b_3_0 + 1.0 * d_b_3_2);  d_c_0_0 += 1.0 * M45;  d_c_0_2 += 1.0 * M45;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_45( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_1_3, d_a_3_3, NULL, NULL, d_b_2_2, d_b_3_2, d_b_2_0, d_b_3_0, d_c_0_0, d_c_0_2, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, -1, 0, 0, 1, -1, -1, 1, 1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_45);

    // M46 = (1.0 * d_a_1_0 + 1.0 * d_a_1_1 + -1.0 * d_a_3_0 + -1.0 * d_a_3_1) * (1.0 * d_b_2_3 + 1.0 * d_b_3_3);  d_c_0_0 += -1.0 * M46;  d_c_0_1 += 1.0 * M46;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_46( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_1_0, d_a_1_1, d_a_3_0, d_a_3_1, d_b_2_3, d_b_3_3, NULL, NULL, d_c_0_1, d_c_0_0, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, -1, -1, 1, 0, 0, 1, -1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_46);

    // M47 = (-1.0 * d_a_1_0 + 1.0 * d_a_1_2 + 1.0 * d_a_3_0 + -1.0 * d_a_3_2) * (1.0 * d_b_2_0 + 1.0 * d_b_2_1 + 1.0 * d_b_3_0 + 1.0 * d_b_3_1);  d_c_0_3 += 1.0 * M47;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_47( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_1_2, d_a_3_0, d_a_1_0, d_a_3_2, d_b_2_0, d_b_2_1, d_b_3_0, d_b_3_1, d_c_0_3, NULL, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, -1, -1, 1, 1, 1, 1, 0, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_47);

    // M48 = (1.0 * d_a_1_1 + -1.0 * d_a_1_3 + -1.0 * d_a_3_1 + 1.0 * d_a_3_3) * (1.0 * d_b_2_2 + 1.0 * d_b_2_3 + 1.0 * d_b_3_2 + 1.0 * d_b_3_3);  d_c_0_0 += 1.0 * M48;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_48( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_1_1, d_a_3_3, d_a_1_3, d_a_3_1, d_b_2_2, d_b_2_3, d_b_3_2, d_b_3_3, d_c_0_0, NULL, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, -1, -1, 1, 1, 1, 1, 0, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_48);

