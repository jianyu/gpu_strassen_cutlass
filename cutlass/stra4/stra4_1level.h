    int lda, ldb, ldc;
    gen_leading_dim<TransformA>( m, k, lda );
    gen_leading_dim<TransformB>( k, n, ldb );
    gen_leading_dim<matrix_transform_t::NonTranspose>( m, n, ldc );
    int ms, ks, ns;
    int md, kd, nd;
    int mr, kr, nr;
    mr = m % ( 2 ), kr = k % ( 2 ), nr = n % ( 2 );
    md = m - mr, kd = k - kr, nd = n - nr;
    ms=md, ks=kd, ns=nd;
    value_t *d_a_0, *d_a_1, *d_a_2, *d_a_3;
    stra4_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 0, d_a, &d_a_0 );
    stra4_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 1, d_a, &d_a_1 );
    stra4_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 0, d_a, &d_a_2 );
    stra4_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 1, d_a, &d_a_3 );
    ms=md, ks=kd, ns=nd;
    value_t *d_b_0, *d_b_1, *d_b_2, *d_b_3;
    stra4_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 0, d_b, &d_b_0 );
    stra4_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 1, d_b, &d_b_1 );
    stra4_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 0, d_b, &d_b_2 );
    stra4_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 1, d_b, &d_b_3 );
    ms=md, ks=kd, ns=nd;
    accum_t *d_c_0, *d_c_1, *d_c_2, *d_c_3;
    stra4_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 0, d_c, &d_c_0 );
    stra4_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 1, d_c, &d_c_1 );
    stra4_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 0, d_c, &d_c_2 );
    stra4_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 1, d_c, &d_c_3 );
    ms=ms/2, ks=ks/2, ns=ns/2;

    // M0 = (1 * d_a_0 + 1 * d_a_3) * (1 * d_b_0 + 1 * d_b_3);  d_c_0 += 1 * M0;  d_c_3 += 1 * M0;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_0( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0, d_a_3, NULL, NULL, d_b_0, d_b_3, NULL, NULL, d_c_0, d_c_3, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_0);

    // M1 = (1 * d_a_2 + 1 * d_a_3) * (1 * d_b_0);  d_c_2 += 1 * M1;  d_c_3 += -1 * M1;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_1( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_2, d_a_3, NULL, NULL, d_b_0, NULL, NULL, NULL, d_c_2, d_c_3, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 0, 0, 0, 0, 0, 1, -1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_1);

    // M2 = (1 * d_a_0) * (1 * d_b_1 + -1 * d_b_3);  d_c_1 += 1 * M2;  d_c_3 += 1 * M2;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_2( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0, NULL, NULL, NULL, d_b_1, d_b_3, NULL, NULL, d_c_1, d_c_3, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 0, 0, 0, -1, 0, 0, 1, 1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_2);

    // M3 = (1 * d_a_3) * (-1 * d_b_0 + 1 * d_b_2);  d_c_0 += 1 * M3;  d_c_2 += 1 * M3;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_3( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_3, NULL, NULL, NULL, d_b_2, d_b_0, NULL, NULL, d_c_0, d_c_2, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 0, 0, 0, -1, 0, 0, 1, 1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_3);

    // M4 = (1 * d_a_0 + 1 * d_a_1) * (1 * d_b_3);  d_c_0 += -1 * M4;  d_c_1 += 1 * M4;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_4( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_0, d_a_1, NULL, NULL, d_b_3, NULL, NULL, NULL, d_c_1, d_c_0, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 0, 0, 0, 0, 0, 1, -1, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_4);

    // M5 = (-1 * d_a_0 + 1 * d_a_2) * (1 * d_b_0 + 1 * d_b_1);  d_c_3 += 1 * M5;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_5( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_2, d_a_0, NULL, NULL, d_b_0, d_b_1, NULL, NULL, d_c_3, NULL, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, -1, 0, 0, 1, 0, 0, 1, 0, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_5);

    // M6 = (1 * d_a_1 + -1 * d_a_3) * (1 * d_b_2 + 1 * d_b_3);  d_c_0 += 1 * M6;
    stra4_param_pack<value_t, accum_t, epilogue_op_t> stra4_pack_6( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, d_a_1, d_a_3, NULL, NULL, d_b_2, d_b_3, NULL, NULL, d_c_0, NULL, NULL, NULL );
    stra4_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, -1, 0, 0, 1, 0, 0, 1, 0, 0, 0><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra4_pack_6);

