    int lda, ldb, ldc;
    gen_leading_dim<TransformA>( m, k, lda );
    gen_leading_dim<TransformB>( k, n, ldb );
    gen_leading_dim<matrix_transform_t::NonTranspose>( m, n, ldc );
    int ms, ks, ns;
    int md, kd, nd;
    int mr, kr, nr;
    mr = m % ( 2 ), kr = k % ( 2 ), nr = n % ( 2 );
    md = m - mr, kd = k - kr, nd = n - nr;
    ms=md, ks=kd, ns=nd;
    value_t *d_a_0, *d_a_1, *d_a_2, *d_a_3;
    stra2_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 0, d_a, &d_a_0 );
    stra2_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 1, d_a, &d_a_1 );
    stra2_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 0, d_a, &d_a_2 );
    stra2_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 1, d_a, &d_a_3 );
    ms=md, ks=kd, ns=nd;
    value_t *d_b_0, *d_b_1, *d_b_2, *d_b_3;
    stra2_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 0, d_b, &d_b_0 );
    stra2_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 1, d_b, &d_b_1 );
    stra2_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 0, d_b, &d_b_2 );
    stra2_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 1, d_b, &d_b_3 );
    ms=md, ks=kd, ns=nd;
    accum_t *d_c_0, *d_c_1, *d_c_2, *d_c_3;
    stra2_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 0, d_c, &d_c_0 );
    stra2_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 1, d_c, &d_c_1 );
    stra2_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 0, d_c, &d_c_2 );
    stra2_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 1, d_c, &d_c_3 );
    ms=ms/2, ks=ks/2, ns=ns/2;


    // M0 = (1 * a_0 + 1 * a_3) * (1 * b_0 + 1 * b_3);  c_0 += 1 * M0;  c_3 += 1 * M0;
    {
    thrust::device_vector<value_t *> d_a_list0(2); d_a_list0[0] = d_a_0; d_a_list0[1] = d_a_3;
    thrust::device_vector<value_t *> d_b_list0(2); d_b_list0[0] = d_b_0; d_b_list0[1] = d_b_3;
    thrust::device_vector<accum_t *> d_c_list0(2); d_c_list0[0] = d_c_0; d_c_list0[1] = d_c_3;
    thrust::device_vector<value_t> coeff_a_list0(2); coeff_a_list0[0] = 1;
    thrust::device_vector<value_t> coeff_b_list0(2); coeff_b_list0[0] = 1;
    thrust::device_vector<accum_t> coeff_c_list0(2); coeff_c_list0[0] = 1; coeff_c_list0[1] = 1;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_0( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list0[0] ), thrust::raw_pointer_cast( &coeff_a_list0[0] ), thrust::raw_pointer_cast( &d_b_list0[0] ), thrust::raw_pointer_cast( &coeff_b_list0[0] ), thrust::raw_pointer_cast( &d_c_list0[0] ), thrust::raw_pointer_cast( &coeff_c_list0[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,2,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_0);
    }

    // M1 = (1 * a_2 + 1 * a_3) * (1 * b_0);  c_2 += 1 * M1;  c_3 += -1 * M1;
    {
    thrust::device_vector<value_t *> d_a_list1(2); d_a_list1[0] = d_a_2; d_a_list1[1] = d_a_3;
    thrust::device_vector<value_t *> d_b_list1(1); d_b_list1[0] = d_b_0;
    thrust::device_vector<accum_t *> d_c_list1(2); d_c_list1[0] = d_c_2; d_c_list1[1] = d_c_3;
    thrust::device_vector<value_t> coeff_a_list1(2); coeff_a_list1[0] = 1;
    thrust::device_vector<value_t> coeff_b_list1(1);
    thrust::device_vector<accum_t> coeff_c_list1(2); coeff_c_list1[0] = 1; coeff_c_list1[1] = -1;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_1( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list1[0] ), thrust::raw_pointer_cast( &coeff_a_list1[0] ), thrust::raw_pointer_cast( &d_b_list1[0] ), thrust::raw_pointer_cast( &coeff_b_list1[0] ), thrust::raw_pointer_cast( &d_c_list1[0] ), thrust::raw_pointer_cast( &coeff_c_list1[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,1,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_1);
    }

    // M2 = (1 * a_0) * (1 * b_1 + -1 * b_3);  c_1 += 1 * M2;  c_3 += 1 * M2;
    {
    thrust::device_vector<value_t *> d_a_list2(1); d_a_list2[0] = d_a_0;
    thrust::device_vector<value_t *> d_b_list2(2); d_b_list2[0] = d_b_1; d_b_list2[1] = d_b_3;
    thrust::device_vector<accum_t *> d_c_list2(2); d_c_list2[0] = d_c_1; d_c_list2[1] = d_c_3;
    thrust::device_vector<value_t> coeff_a_list2(1);
    thrust::device_vector<value_t> coeff_b_list2(2); coeff_b_list2[0] = -1;
    thrust::device_vector<accum_t> coeff_c_list2(2); coeff_c_list2[0] = 1; coeff_c_list2[1] = 1;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_2( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list2[0] ), thrust::raw_pointer_cast( &coeff_a_list2[0] ), thrust::raw_pointer_cast( &d_b_list2[0] ), thrust::raw_pointer_cast( &coeff_b_list2[0] ), thrust::raw_pointer_cast( &d_c_list2[0] ), thrust::raw_pointer_cast( &coeff_c_list2[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1,2,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_2);
    }

    // M3 = (1 * a_3) * (-1 * b_0 + 1 * b_2);  c_0 += 1 * M3;  c_2 += 1 * M3;
    {
    thrust::device_vector<value_t *> d_a_list3(1); d_a_list3[0] = d_a_3;
    thrust::device_vector<value_t *> d_b_list3(2); d_b_list3[0] = d_b_2; d_b_list3[1] = d_b_0;
    thrust::device_vector<accum_t *> d_c_list3(2); d_c_list3[0] = d_c_0; d_c_list3[1] = d_c_2;
    thrust::device_vector<value_t> coeff_a_list3(1);
    thrust::device_vector<value_t> coeff_b_list3(2); coeff_b_list3[0] = -1;
    thrust::device_vector<accum_t> coeff_c_list3(2); coeff_c_list3[0] = 1; coeff_c_list3[1] = 1;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_3( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list3[0] ), thrust::raw_pointer_cast( &coeff_a_list3[0] ), thrust::raw_pointer_cast( &d_b_list3[0] ), thrust::raw_pointer_cast( &coeff_b_list3[0] ), thrust::raw_pointer_cast( &d_c_list3[0] ), thrust::raw_pointer_cast( &coeff_c_list3[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1,2,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_3);
    }

    // M4 = (1 * a_0 + 1 * a_1) * (1 * b_3);  c_0 += -1 * M4;  c_1 += 1 * M4;
    {
    thrust::device_vector<value_t *> d_a_list4(2); d_a_list4[0] = d_a_0; d_a_list4[1] = d_a_1;
    thrust::device_vector<value_t *> d_b_list4(1); d_b_list4[0] = d_b_3;
    thrust::device_vector<accum_t *> d_c_list4(2); d_c_list4[0] = d_c_1; d_c_list4[1] = d_c_0;
    thrust::device_vector<value_t> coeff_a_list4(2); coeff_a_list4[0] = 1;
    thrust::device_vector<value_t> coeff_b_list4(1);
    thrust::device_vector<accum_t> coeff_c_list4(2); coeff_c_list4[0] = 1; coeff_c_list4[1] = -1;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_4( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list4[0] ), thrust::raw_pointer_cast( &coeff_a_list4[0] ), thrust::raw_pointer_cast( &d_b_list4[0] ), thrust::raw_pointer_cast( &coeff_b_list4[0] ), thrust::raw_pointer_cast( &d_c_list4[0] ), thrust::raw_pointer_cast( &coeff_c_list4[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,1,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_4);
    }

    // M5 = (-1 * a_0 + 1 * a_2) * (1 * b_0 + 1 * b_1);  c_3 += 1 * M5;
    {
    thrust::device_vector<value_t *> d_a_list5(2); d_a_list5[0] = d_a_2; d_a_list5[1] = d_a_0;
    thrust::device_vector<value_t *> d_b_list5(2); d_b_list5[0] = d_b_0; d_b_list5[1] = d_b_1;
    thrust::device_vector<accum_t *> d_c_list5(1); d_c_list5[0] = d_c_3;
    thrust::device_vector<value_t> coeff_a_list5(2); coeff_a_list5[0] = -1;
    thrust::device_vector<value_t> coeff_b_list5(2); coeff_b_list5[0] = 1;
    thrust::device_vector<accum_t> coeff_c_list5(1); coeff_c_list5[0] = 1;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_5( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list5[0] ), thrust::raw_pointer_cast( &coeff_a_list5[0] ), thrust::raw_pointer_cast( &d_b_list5[0] ), thrust::raw_pointer_cast( &coeff_b_list5[0] ), thrust::raw_pointer_cast( &d_c_list5[0] ), thrust::raw_pointer_cast( &coeff_c_list5[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,2,1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_5);
    }

    // M6 = (1 * a_1 + -1 * a_3) * (1 * b_2 + 1 * b_3);  c_0 += 1 * M6;
    {
    thrust::device_vector<value_t *> d_a_list6(2); d_a_list6[0] = d_a_1; d_a_list6[1] = d_a_3;
    thrust::device_vector<value_t *> d_b_list6(2); d_b_list6[0] = d_b_2; d_b_list6[1] = d_b_3;
    thrust::device_vector<accum_t *> d_c_list6(1); d_c_list6[0] = d_c_0;
    thrust::device_vector<value_t> coeff_a_list6(2); coeff_a_list6[0] = -1;
    thrust::device_vector<value_t> coeff_b_list6(2); coeff_b_list6[0] = 1;
    thrust::device_vector<accum_t> coeff_c_list6(1); coeff_c_list6[0] = 1;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_6( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list6[0] ), thrust::raw_pointer_cast( &coeff_a_list6[0] ), thrust::raw_pointer_cast( &d_b_list6[0] ), thrust::raw_pointer_cast( &coeff_b_list6[0] ), thrust::raw_pointer_cast( &d_c_list6[0] ), thrust::raw_pointer_cast( &coeff_c_list6[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,2,1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_6);
    }

