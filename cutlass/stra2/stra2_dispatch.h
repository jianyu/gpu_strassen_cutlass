
#pragma once

/**
 * \file
 * GEMM kernel entrypoint and dispatch stub
 */

#include <thrust/device_vector.h>

#include <stdint.h>

#include "../util/util.h"
#include "../gemm/block_task.h"
#include "../gemm/block_task_wmma.h"
#include "../gemm/grid_raster.h"
#include "../gemm/dispatch_policies.h"
#include "../gemm/k_split_control.h"


#include "../gemm/dispatch.h"

#include "stra2_block_task.h"
//#include "stra_block_task_wmma.h"


namespace cutlass {
namespace gemm {


/******************************************************************************
 * param_pack
 ******************************************************************************/

/**
 * Parameter-pack structure
 *
 * Kernel launch latency is reduced when kernel arguments are wrapped into
 * a single parameter
 */
template <
    typename value_t,
    typename accum_t,
    typename epilogue_op_t>
struct stra2_param_pack
{
    int m;                      ///< Height in rows of op(A) and C
    int n;                      ///< Width in columns of op(B) and C
    int k;                      ///< Width in columns of op(A) and height in rows of op(B)
    int lda;
    int ldb;
    int ldc;
    k_split_control k_split;    ///< Abstraction for controlling inter-block k-splitting
    value_t **d_a_list;               ///< the list of Pointer to matrix A array values
    value_t *coeff_a_list;           ///< the list of coeff to A
    value_t **d_b_list;               ///< the list of Pointer to matrix B array values
    value_t *coeff_b_list;           ///< the list of coeff to B
    accum_t **d_c_list;               ///< the list of Pointer to matrix C array values
    accum_t *coeff_c_list;           ///< the list of coeff to C
    epilogue_op_t epilogue_op;

    stra2_param_pack(
        int m,                      ///< Height in rows of op(A) and C
        int n,                      ///< Width in columns of op(B) and C
        int k,                      ///< Width in columns of op(A) and height in rows of op(B)
        int lda, int ldb, int ldc,
        k_split_control k_split,    ///< Abstraction for controlling inter-block k-splitting
        epilogue_op_t op,           ///< Epilogue operation to update matrix C
        value_t **d_a_list,              ///< the list of Pointer to matrix A array values
        value_t *coeff_a_list,          ///< the list of coeff to A
        value_t **d_b_list,              ///< the list of Pointer to matrix B array values
        value_t *coeff_b_list,          ///< the list of coeff to B
        accum_t **d_c_list,              ///< the list of Pointer to matrix C array values
        accum_t *coeff_c_list          ///< the list of coeff to C
        )
    :
        m(m),
        n(n),
        k(k),
        lda(lda), ldb(ldb), ldc(ldc),
        k_split(k_split),
        epilogue_op(op),
        d_a_list(d_a_list),
        coeff_a_list(coeff_a_list),
        d_b_list(d_b_list),
        coeff_b_list(coeff_b_list),
        d_c_list(d_c_list),
        coeff_c_list(coeff_c_list)
    {
        //d_a_list = reinterpret_cast<value_t **>(d_a_list);
        //d_b_list = reinterpret_cast<value_t **>(d_b_list);
        //d_c_list = reinterpret_cast<accum_t **>(d_c_list);

        //d_a_list = d_a_list;
        //d_b_list = d_b_list;
        //d_c_list = d_c_list;

        //printf( "Hello\n" );
    }

};


/******************************************************************************
 * Conditionally select the appropriate GEMM threadblock task
 ******************************************************************************/

/// Conditional selection for block task
template <
    math_operation_class_t      math_op,            ///<
    typename                    block_task_policy_t,  ///< Parameterization of block_task_policy
    typename                    value_t,            ///< Multiplicand value type (matrices A and B)
    typename                    accum_t,            ///< Accumulator value type (matrix C and scalars)
    matrix_transform_t::kind_t  TransformA,         ///< View transform enumerant for matrix A
    int                         LdgAlignA,          ///< Alignment (in bytes) for A operand
    matrix_transform_t::kind_t  TransformB,         ///< View transform enumerant for matrix B
    int                         LdgAlignB,          ///< Alignment (in bytes) for B operand
    typename                    epilogue_op_t,      ///< Epilogue operation applied to GEMM
    int                         LdgAlignC,          ///< Alignment (in bytes) for C operand
    bool                        AllowRaggedTiles,    ///< Whether GEMM supports matrix sizes other than multiple of BlockItems{XY}
    int NA,
    int NB,
    int NC
>
struct stra2_gemm_block_task;

/// Scalar math operations
template <
    typename                    block_task_policy_t,  ///< Parameterization of block_task_policy
    typename                    value_t,            ///< Multiplicand value type (matrices A and B)
    typename                    accum_t,            ///< Accumulator value type (matrix C and scalars)
    matrix_transform_t::kind_t  TransformA,         ///< View transform enumerant for matrix A
    int                         LdgAlignA,          ///< Alignment (in bytes) for A operand
    matrix_transform_t::kind_t  TransformB,         ///< View transform enumerant for matrix B
    int                         LdgAlignB,          ///< Alignment (in bytes) for B operand
    typename                    epilogue_op_t,      ///< Epilogue operation applied to GEMM
    int                         LdgAlignC,          ///< Alignment (in bytes) for C operand
    bool                        AllowRaggedTiles,   ///< Whether GEMM supports matrix sizes other than multiple of BlockItems{XY}
    int NA,
    int NB,
    int NC
>
struct stra2_gemm_block_task<
    math_operation_class_t::scalar,
    block_task_policy_t,
    value_t,
    accum_t,
    TransformA,
    LdgAlignA,
    TransformB,
    LdgAlignB,
    epilogue_op_t,
    LdgAlignC,
    AllowRaggedTiles,
    NA,
    NB,
    NC
>
{
    //printf( "math_op_scalar\n" );
    // Parameterize task type
    typedef stra2_block_task<
            block_task_policy_t,
            value_t,
            accum_t,
            TransformA,
            LdgAlignA,
            TransformB,
            LdgAlignB,
            epilogue_op_t,
            LdgAlignC,
            AllowRaggedTiles,
            NA,
            NB,
            NC
            > type;
};

/// Matrix math operations
template <
    typename                    block_task_policy_t,  ///< Parameterization of block_task_policy
    typename                    value_t,            ///< Multiplicand value type (matrices A and B)
    typename                    accum_t,            ///< Accumulator value type (matrix C and scalars)
    matrix_transform_t::kind_t  TransformA,         ///< View transform enumerant for matrix A
    int                         LdgAlignA,          ///< Alignment (in bytes) for A operand
    matrix_transform_t::kind_t  TransformB,         ///< View transform enumerant for matrix B
    int                         LdgAlignB,          ///< Alignment (in bytes) for B operand
    typename                    epilogue_op_t,      ///< Epilogue operation applied to GEMM
    int                         LdgAlignC,          ///< Alignment (in bytes) for C operand
    bool                        AllowRaggedTiles,   ///< Whether GEMM supports matrix sizes other than multiple of BlockItems{XY}
    int NA, 
    int NB, 
    int NC  
>
struct stra2_gemm_block_task<
    math_operation_class_t::matrix,
    block_task_policy_t,
    value_t,
    accum_t,
    TransformA,
    LdgAlignA,
    TransformB,
    LdgAlignB,
    epilogue_op_t,
    LdgAlignC,
    AllowRaggedTiles,
    NA,
    NB,
    NC
>
{
    //printf( "math_op_matrix\n" );

#if defined(WMMA)   // conditional compilation with WMMA headers

    /* Jianyu TO MODIFY: wmma is not complete*/

    // Parameterize task type
    typedef stra2_block_task_wmma<
            block_task_policy_t,
            value_t,
            accum_t,
            TransformA,
            LdgAlignA,
            TransformB,
            LdgAlignB,
            epilogue_op_t,
            LdgAlignC,
            AllowRaggedTiles,
            NA,
            NB,
            NC
            > type;

#endif
};

/******************************************************************************
 * GEMM kernel entrypoint
 ******************************************************************************/

/**
 * GEMM kernel
 *
 * NB: Not sure why NVVM is doing stuff with "__launch_bounds__" instead of just
 * passing it along to PTXAS, but it is currently resulting in less optimal codegen
 */
template <
    math_operation_class_t      math_op,            ///< Indicates which class of math operation to select
    typename                    block_task_policy_t,  ///< Parameterization of block_task_policy
    matrix_transform_t::kind_t  TransformA,         ///< Transformation op for matrix A
    int                         LdgAlignA,          ///< Alignment of A matrix elements in bytes
    matrix_transform_t::kind_t  TransformB,         ///< Transformation op for matrix B
    int                         LdgAlignB,          ///< Alignment of B matrix elements in bytes
    typename                    value_t,            ///< Multiplicand value type (matrices A and B)
    typename                    accum_t,            ///< Accumulator value type (matrix C and scalars)
    typename                    epilogue_op_t,      ///< Epilogue operation applied to update matrix C
    int                         LdgAlignC,          ///< Alignment of C elements in bytes
    bool                        AllowRaggedTiles,   ///< Boolean to indicate whether AllowRaggedTiles handling is enabled
    int NA, 
    int NB, 
    int NC  
>
__global__ void stra2_kernel(stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack)
{
    // Parameterize task type
    typedef typename stra2_gemm_block_task<
        math_op,
        block_task_policy_t,
        value_t,
        accum_t,
        TransformA,
        LdgAlignA,
        TransformB,
        LdgAlignB,
        epilogue_op_t,
        LdgAlignC,
        AllowRaggedTiles,
        NA,
        NB,
        NC
        >::type stra2_block_task_t;

    // Declare statically-allocated shared storage
    __shared__ typename stra2_block_task_t::scratch_storage_t smem;

    //printf( "kernel: Before block_task_t\n" );



    //if ( blockIdx.x == 0 && blockIdx.y == 0 && blockIdx.z == 0 && threadIdx.x == 0 )
    //{
    //    printf( "d_a_list[0][0]: %f\n", stra2_pack.d_a_list[0][0] );
    //}


    // Construct and run the task
    stra2_block_task_t(
        &smem,
        stra2_pack.d_a_list, stra2_pack.coeff_a_list,
        stra2_pack.d_b_list, stra2_pack.coeff_b_list,
        stra2_pack.d_c_list, stra2_pack.coeff_c_list,
        stra2_pack.epilogue_op,
        stra2_pack.m,
        stra2_pack.n,
        stra2_pack.k,        
        stra2_pack.lda, stra2_pack.ldb, stra2_pack.ldc,
        stra2_pack.k_split).run();
}


/******************************************************************************
 * Launch configuration description returned to the caller
 ******************************************************************************/

/*
/// Return details about the launch configuration to the caller
struct launch_configuration
{
    //
    // Data members
    //

    /// cudaError_t resulting from grid launch
    cudaError_t result;

    /// Extent of a thread block's partition along the GEMM K-axis
    int split_k;

    /// Kernel grid extents in thread blocks
    dim3 grid;

    /// Thread block extents in threads
    dim3 block;

    //
    // Methods
    //

    /// Constructor
    launch_configuration():
        result(cudaSuccess),
        split_k(0),
        grid(0, 0, 0),
        block(0, 0, 0) {

    }

    /// Conversion from cudaError_t
    launch_configuration(cudaError_t result):
        result(result),
        split_k(1),
        grid(0, 0, 0),
        block(0, 0, 0) {

    }

    /// Launch configuration for Cutlass kernels
    launch_configuration(
        cudaError_t result,
        int split_k,
        dim3 grid,
        dim3 block
    ):
        result(result),
        split_k(split_k),
        grid(grid),
        block(block) {

    }
};
*/

template<
    matrix_transform_t::kind_t  TransformM
    >
inline void gen_leading_dim(
        int m, int n,
        int &ldM
        ) 
{
    if ( TransformM == matrix_transform_t::NonTranspose ) {
        ldM = m;
    } else {
        ldM = n;
    }
}


template<
    typename T,
    matrix_transform_t::kind_t  TransformM        ///< Transformation op for matrix M
    >
inline void stra2_acquire_mpart(
        int m, int n, int ldM,
        int x, int y, int i, int j,
        T* srcM, T** dstM
        )
{
    if ( TransformM == matrix_transform_t::NonTranspose ) {
        *dstM = &srcM[ ( m / x * i ) * 1 + ( n / y * j ) * ldM ]; //srcM( m/x*i, n/y*j )
    } else { // Transpose
        *dstM = &srcM[ ( m / x * i ) * ldM + ( n / y * j ) * 1 ]; //srcM( m/x*i, n/y*j )
    }
}


/******************************************************************************
 * Dispatch stub
 ******************************************************************************/

/**
 * GEMM dispatch stub
 *
 * This function also serves as the autotuning entrypoint to evaluate different
 * tuning parameterizations of kernel.
 */
template <
    math_operation_class_t      math_op,            ///< Indicates which class of math operation to select
    typename                    block_task_policy_t,  ///< Parameterization of block_task_policy
    matrix_transform_t::kind_t  TransformA,         ///< Transformation op for matrix A
    int                         LdgAlignA,          ///< Alignment of A matrix elements in bytes
    matrix_transform_t::kind_t  TransformB,         ///< Transformation op for matrix B
    int                         LdgAlignB,          ///< Alignment of B matrix elements in bytes
    typename                    value_t,            ///< Multiplicand value type (matrices A and B)
    typename                    accum_t,            ///< Accumulator value type (matrix C and scalars)
    typename                    epilogue_op_t,      ///< Epilogue operation
    int                         LdgAlignC,          ///< Alignment of C matrix elements in bytes
    bool                        AllowRaggedTiles,   ///< Boolean to indicate whether AllowRaggedTiles handling is enabled
    typename                    kernel_ptr_t>       ///< GEMM kernel function pointer type
launch_configuration stra2_dispatch(
    kernel_ptr_t    kernel_ptr,                     ///< GEMM kernel function pointer
    int             m,                              ///< Height in rows of op(A) and C
    int             n,                              ///< Width in columns of op(B) and C
    int             k,                              ///< Width in columns of op(A) and height in rows of op(B)
    epilogue_op_t   epilogue_op,                    ///< Epilogue operation to update matrix C
    value_t         *d_a,                           ///< Device pointer to matrix A array values
    value_t         *d_b,                           ///< Device pointer to matrix B array values
    accum_t         *d_c,                           ///< Device pointer to matrix C array values
    cudaStream_t    stream = 0,                     ///< CUDA stream to launch kernels within.  Default is stream<sub>0</sub>.
    bool            debug_synchronous = true)       ///< Whether or not to synchronize the stream after every kernel launch
                                                    ///  to check for errors.  Also causes launch configurations to be printed
                                                    ///  to the console if DEBUG is defined.  Default is \p false.
{
    // Thread block rasterization type
    typedef grid_raster<
            block_task_policy_t::BlockItemsY,
            block_task_policy_t::BlockItemsX,
            TransformA,
            TransformB,
            block_task_policy_t::RasterStrategy>
        grid_raster_t;

    launch_configuration config;

    // Compute block dims
    config.block = dim3(block_task_policy_t::BlockThreads);

    // Compute shared memory
    int dynamic_smem_bytes = 0;

   auto kernel_ptr2 = stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2, 2, 2>;

    // Compute occupancy
    // Returns in *numBlocks the maximum number of active blocks per streaming multiprocessor for the device function.
    // Residency is the number of thread blocks that can run concurrently on the SM
    // Actually this is residency
    int max_sm_occupancy;
    if (CUDA_PERROR_DEBUG(config.result = cudaOccupancyMaxActiveBlocksPerMultiprocessor(
        &max_sm_occupancy,
        kernel_ptr2,
        config.block.x * config.block.y,
        dynamic_smem_bytes)))
    {
        return config;
    }

    // Compute grid extents
    config.grid = grid_raster_t::grid_dims(m / 4, n / 4);

    // Get SM count
    int sm_count;
    if (CUDA_PERROR_DEBUG(config.result = get_sm_count(sm_count)))
        return config;

    // Get k-split flag storage (TODO: make a pool)
    int *d_flags;
    if (CUDA_PERROR_DEBUG(config.result = cudaGetSymbolAddress((void**) &d_flags, d_flags_split_k)))
        return config;


    // Construct k-split coordinator
    k_split_control k_split0(
        d_flags,
        sm_count,
        max_sm_occupancy,
        k / 4,
        block_task_policy_t::BlockItemsK,
        config.block,
        config.grid);     // in,out

    // Construct k-split coordinator
    k_split_control k_split1(
        d_flags + NumFlagsSplitK,
        k_split0);     // in,out

    config.split_k = k_split0.split_k;

    // Log kernel configuration
    if (debug_synchronous)
    {
        // Compute tiling efficiency
        float block_tiling_efficiency = float(block_task_policy_t::BlockItemsY * block_task_policy_t::BlockItemsX) /
            float(block_task_policy_t::BlockItemsY + block_task_policy_t::BlockItemsX);

        float tiling_efficiency = block_tiling_efficiency;

        float wave_efficiency = k_split0.get_wave_efficiency(
            sm_count, max_sm_occupancy, config.block, config.grid);

        CUDA_LOG_DEBUG("Final wave_efficiency %.4f, tiling_efficiency %.4f\n",
            wave_efficiency, tiling_efficiency);

        CUDA_LOG_DEBUG("Invoking kernel<<<(%d, %d, %d), (%d.y,%d.x), %d, %lld>>>(), %d SM occupancy, %d split_k\n",
            config.grid.x, config.grid.y, config.grid.z,
            config.block.y, config.block.x,
            dynamic_smem_bytes,
            (long long) stream,
            max_sm_occupancy,
            k_split0.split_k);
    }


    // Prepare k-split coordinator
    if (CUDA_PERROR_DEBUG(config.result = k_split0.prepare(stream, debug_synchronous)))
    {
        return config;
    }

    //std::cout << "Before Invoke kernel" << std::endl;

    //// Invoke kernel
    //kernel_ptr<<< config.grid, config.block, dynamic_smem_bytes, stream >>>(pack);



    ////cudaProfilerStart();
    //#include "stra2_1level.h"
    #include "stra2_2level.h"
    ////cudaProfilerStop();


    /*
    int lda, ldb, ldc;
    value_t *d_a00, *d_a01, *d_a10, *d_a11;
    value_t *d_b00, *d_b01, *d_b10, *d_b11;
    if ( TransformA == matrix_transform_t::NonTranspose ) {
        lda = m;
        d_a00 = d_a;
        d_a01 = d_a + k / 2 * lda;
        d_a10 = d_a + m / 2;
        d_a11 = d_a01 + m / 2;
    } else { // DOUBLE CHECK
        lda = k;
        d_a00 = d_a;
        d_a01 = d_a + k / 2;
        d_a10 = d_a + m / 2 * lda;
        d_a11 = d_a10 + k / 2;
    }
    if ( TransformB == matrix_transform_t::NonTranspose ) {
        ldb = k;
        d_b00 = d_b;
        d_b01 = d_b + n / 2 * ldb;
        d_b10 = d_b + k / 2;
        d_b11 = d_b01 + k / 2;
    } else { // DOUBLE CHECK
        ldb = n;
        d_b00 = d_b;
        d_b01 = d_b + n / 2;
        d_b10 = d_b + k / 2 * ldb;
        d_b11 = d_b10 + n / 2;
    }
    accum_t *d_c00, *d_c01, *d_c10, *d_c11;
    ldc = m;
    d_c00 = d_c;
    d_c01 = d_c + n / 2 * ldc;
    d_c10 = d_c + m / 2;
    d_c11 = d_c01 + m / 2;


    {
    //printf( "before kernel: d_a00[0]: %f\n", d_a00[0] );

    //value_t *d_a_list0[2] = { d_a00, d_a11 };
    //value_t *d_b_list0[2] = { d_b00, d_b11 };
    //accum_t *d_c_list0[2] = { d_c00, d_c11 };
    //value_t coeff_a_list0[2] = {1, 1};
    //value_t coeff_b_list0[2] = {1, 1};
    //accum_t coeff_c_list0[2] = {1, 1};

    //thrust::device_vector<value_t *> d_a_list0 = { d_a00, d_a11 };
    //thrust::device_vector<value_t *> d_b_list0 = { d_b00, d_b11 };
    //thrust::device_vector<accum_t *> d_c_list0 = { d_c00, d_c11 };
    thrust::device_vector<value_t *> d_a_list0(2); d_a_list0[0] = d_a00; d_a_list0[1] = d_a11;
    thrust::device_vector<value_t *> d_b_list0(2); d_b_list0[0] = d_b00; d_b_list0[1] = d_b11;
    thrust::device_vector<accum_t *> d_c_list0(2); d_c_list0[0] = d_c00; d_c_list0[1] = d_c11;
    thrust::device_vector<value_t> coeff_a_list0(1); coeff_a_list0[0] = 1;
    thrust::device_vector<value_t> coeff_b_list0(1); coeff_b_list0[0] = 1;
    thrust::device_vector<accum_t> coeff_c_list0(2); coeff_c_list0[0] = 1; coeff_c_list0[1] = 1;

    //value_t *coeff_a_list0 = (value_t *)&d_flags[NumFlagsSplitK];
    //coeff_a_list0[0] = 1; coeff_a_list0[1] = 1;
    //value_t *coeff_b_list0 = coeff_a_list0 + 2;
    //coeff_b_list0[0] = 1; coeff_b_list0[1] = 1;
    //value_t *coeff_c_list0 = coeff_b_list0 + 2;
    //coeff_c_list0[0] = 1; coeff_c_list0[1] = 1;

    //value_t **p_d_a_list = d_a_list0;
    //value_t *coeff_a_list = coeff_a_list0;
    ////printf( "before kernel: p_d_a_list[0][0]: %f\n", p_d_a_list[0][0] );
    //printf( "before kernel: coeff_a_list[0]: %f\n", coeff_a_list[0] );


    //printf( "before kernel: d_a_list0[0][0]: %f\n", d_a_list0[0][0] );

    //printf( "m: %d; n: %d; k: %d\n", m, n, k );
    //d_a00[0] = 0.0;

    //std::cout << "Before Invoke kernel" << std::endl;
    //typedef stra_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles> kernel3_type;

    // Construct parameter-pack
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack(
        m / 2, n / 2, k / 2,
        lda, ldb, ldc,
        k_split0,
        epilogue_op,
        thrust::raw_pointer_cast( &d_a_list0[0] ), thrust::raw_pointer_cast( &coeff_a_list0[0] ),
        thrust::raw_pointer_cast( &d_b_list0[0] ), thrust::raw_pointer_cast( &coeff_b_list0[0] ),
        thrust::raw_pointer_cast( &d_c_list0[0] ), thrust::raw_pointer_cast( &coeff_c_list0[0] )
        );


    ////if ( blockIdx.x == 0 && blockIdx.y == 0 && blockIdx.z == 0 && threadIdx.x == 0 )
    //{
    //    printf( "before kernel: d_a_list[0][0]: %f\n", stra2_pack.d_a_list[0][0] );
    //}


    //printf( "Before Invoke kernel\n" );

    // Invoke kernel
    //kernel_ptr<<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra_pack);
    //stra_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra_pack);
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2, 2, 2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>( stra2_pack );

    }
    */

    //printf( "After Invoke kernel\n" );

    // Check for failure to launch
    if (CUDA_PERROR_DEBUG(config.result = cudaPeekAtLastError()))
        return config;

    // Sync the stream if specified to flush runtime errors
    if (debug_synchronous && (CUDA_PERROR_DEBUG(config.result = cudaStreamSynchronize(stream))))
        return config;

    return config;
}


/******************************************************************************
 * GEMM
 ******************************************************************************/

/**
 * Computes gemm on device matrices
 */
template <
    tiling_strategy::kind_t      TilingStrategy,    ///< Tile-sizing classification
    math_operation_class_t      math_op,        ///< Indicates which class of math operation to select
    matrix_transform_t::kind_t  TransformA,     ///< Transformation op for matrix A
    int                         LdgAlignA,      ///< Alignment (in bytes) of A operand
    matrix_transform_t::kind_t  TransformB,     ///< Transformation op for matrix B
    int                         LdgAlignB,      ///< Alignment (in bytes) of B operand
    typename                    value_t,        ///< Multiplicand value type (matrices A and B)
    typename                    accum_t,        ///< Accumulator value type (matrix C and scalars)
    typename                    epilogue_op_t,  ///< Epilogue operation to update matrix C
    int                         LdgAlignC>      ///< Alignment (in bytes) of C operand
launch_configuration stra2_device_gemm(
    int             m,                          ///< Height in rows of op(A) and C
    int             n,                          ///< Width in columns of op(B) and C
    int             k,                          ///< Width in columns of op(A) and height in rows of op(B)
    epilogue_op_t   epilogue_op,                ///< Epilogue operation to update matrix C
    value_t         *d_a,                       ///< Device pointer to matrix A array values
    value_t         *d_b,                       ///< Device pointer to matrix B array values
    accum_t         *d_c,                       ///< Device pointer to matrix C array values
    cudaStream_t    stream = 0,                 ///< CUDA stream to launch kernels within.  Default is stream<sub>0</sub>.
    bool            debug_synchronous = false)  ///< Whether or not to synchronize the stream after every kernel launch to
                                                ///  check for errors.  Also causes launch configurations to be printed to
                                                ///  the console if DEBUG is defined.  Default is \p false.
{
    // Parameterize an task policy type
    // (TODO: use a policy dispatch mechanism based upon SM version)
    typedef gemm_policy<value_t, accum_t, TransformA, TransformB, TilingStrategy> block_task_policy_t;

    //std::cout << "Enter device_gemm" << std::endl;

    // AllowRaggedTiles-tile check
    if ((m % block_task_policy_t::BlockItemsY != 0) ||
        (n % block_task_policy_t::BlockItemsX != 0) ||
        (k % block_task_policy_t::BlockItemsK != 0))
    {
        // Needs ragged tile-handling
        static const bool AllowRaggedTiles = true;

        //std::cout << "device_gemm: tile-handling" << std::endl;

        return stra2_dispatch<math_op, block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles>(
            stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2, 2, 2>,
            m,
            n,
            k,
            epilogue_op,
            d_a,
            d_b,
            d_c,
            stream,
            debug_synchronous);
    }
    else
    {
        // Does not need ragged tile-handling
        static const bool AllowRaggedTiles = false;

        //std::cout << "device_gemm: no tile-handling" << std::endl;

        return stra2_dispatch<math_op, block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles>(
            stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2, 2, 2>,
            m,
            n,
            k,
            epilogue_op,
            d_a,
            d_b,
            d_c,
            stream,
            debug_synchronous);
    }


}


} // namespace gemm
} // namespace cutlass
