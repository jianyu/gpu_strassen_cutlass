import sys

from common import is_one, is_negone, is_nonzero, write_line, write_break, data_access, transpose, printmat, writeCoeffs, phantomMatMul, parse_coeff, read_coeffs, writeFMM, writePartition, writeEquation, getSubMatName, getBlockName, getName, generateCoeffs, exp_dim, contain_nontrivial

def arith_expression_pntr(coeff, mat_name, ind, incm):
    ''' Return the arithmetic expression needed for multiplying coeff by value
    in a string of expressions.

    coeff is the coefficient
    value is a string representing the value to be multiplied by coeff
    place is the place in the arithmetic expression
    '''
    if incm == '1':
        value = '%s%d_pntr[ i ]'% ( mat_name, ind )
    else:
        value = '%s%d_pntr[ i * %s ]'% ( mat_name, ind, incm )
    if is_one(coeff):
         expr = ' %s' % value
    elif is_negone(coeff):
        expr = ' - %s' % value
    else:
        #print "coeff is not +-1!"
        expr = ' (double)(%s) * %s' % (coeff, value)

    if ind != 0 and not is_negone(coeff):
        return ' +' + expr
    return expr


def create_straprim_caller( myfile, coeffs, dims, num_multiplies, level=1 ):
    ''' Generate all of the recursive multiplication calls.

    myfile is the file to which we are writing
    coeffs is the set of all coefficients
    dims is a 3-tuple (m, k, n) of the dimensions of the problem
    '''
    for i in xrange(len(coeffs[0][0])):
        a_coeffs = [c[i] for c in coeffs[0]]
        b_coeffs = [c[i] for c in coeffs[1]]
        c_coeffs = [c[i] for c in coeffs[2]]


        write_straprim_caller(myfile, i, a_coeffs, b_coeffs, c_coeffs, dims, num_multiplies, level)

def write_stra_mat( myfile, coeff_idx, coeffs, idx, dim_name, dims, level ):
    mat_name = ( getName( coeff_idx ) )[ 0 ]
    nonzero_coeffs = [coeff for coeff in coeffs if is_nonzero(coeff)]
    add = 'stra_matrix_view<T,' + str(len(nonzero_coeffs)) + '> '
    add += mat_name + 'v' + str(idx) + '({' + dim_name + '}, {'
    #add += ', '.join( ['const_cast<T*>(%s)' % (getSubMatName(coeff_idx, i, dims, level) ) for i, c in enumerate(coeffs) if is_nonzero(c)] )
    add += ', '.join( ['const_cast<T*>(%s)' % (getBlockName(coeff_idx, i, dims, level) ) for i, c in enumerate(coeffs) if is_nonzero(c)] )
    add += '}, {'
    add += ', '.join( [ str(c) for i, c in enumerate(coeffs) if is_nonzero(c) ] )
    add += '}, {rs_' + mat_name + ', cs_' + mat_name +'});'

    write_line(myfile, 1, add)

def reorder_coeff( coeffs ):
    id_coeff_pair_list = [ (i, c) for i, c in enumerate(coeffs) if is_nonzero(c) ]
    id_coeff_pair_list.sort( key=lambda x: x[1], reverse=True )
    if ( len( id_coeff_pair_list ) == 0 ):
        print "No elements in (mat_id, coeff) pair"
    #if  ( id_coeff_pair_list[0][1] != '1' ):
    if  ( not is_one( id_coeff_pair_list[0][1] ) ):
        print coeffs
        print id_coeff_pair_list
        print "All coefficients are less than 1"
    return id_coeff_pair_list

def add_coeff_arg( mat_id, coeffs, level, index ):
    coeff_list = [ c for i, c in reorder_coeff( coeffs ) ]
    start_idx = 0
    my_type = 'value_t'
    if ( mat_id == 0 or mat_id == 1 ):
        if ( not is_one( coeff_list[0] ) ):
            print "The first coeff is not 1!"
            exit( 0 )
        start_idx = 1
    else:
        my_type = 'accum_t'

    add = 'thrust::device_vector<{2}> coeff_{3}_list{0}({1});'.format( index, len(reorder_coeff(coeffs)), my_type, (getName(mat_id))[0] )
    list_idx = 0
    for c in coeff_list[start_idx:]:
        add += ' coeff_{0}_list{1}[{2}] = {3};'.format( (getName(mat_id))[0],  index, list_idx, c )
        list_idx += 1

    #print "arg_len:" + str(arg_len)
    #print "coeff_list:" + str( len( coeff_list ) )

    #arg_len = 2**level
    #for i in range( arg_len - len( coeff_list ) ):
    #    if len(add) == 0:
    #        add += '0'
    #    else:
    #        add += ', 0'
    return add

def add_submat_arg( mat_id, coeffs, level, dims, index ):
    if ( mat_id == 0 or mat_id == 1 ):
        my_type = 'value_t'
    else:
        my_type = 'accum_t'
    add =  'thrust::device_vector<{2} *> d_{3}_list{0}({1});'.format( index, len(reorder_coeff(coeffs)), my_type, (getName(mat_id))[0] )
    list_idx = 0
    for i, c in reorder_coeff( coeffs ):
        add += ' d_{0}_list{1}[{3}] = d_{2};'.format( (getName(mat_id))[0],  index, getBlockName( mat_id, i, dims, level ), list_idx )
        list_idx += 1
    #add = ', '.join( [ '%s' % getBlockName( mat_id, i, dims, level ) \
    #                           for i, c in reorder_coeff( coeffs ) ] )

    #for i in range( 2**level - len(  reorder_coeff( coeffs ) ) ):
    #    add += ', NULL'
    #add += ')'
    return add

#def add_coeff_arg( ):
#

def write_straprim_caller(myfile, index, a_coeffs, b_coeffs, c_coeffs, dims, num_multiplies, level=1):
    comment = '// M%d = (' % (index)
    comment += ' + '.join([str(c) + ' * %s' % getBlockName( 0, i, dims, level ) \
                               for i, c in enumerate(a_coeffs) if is_nonzero(c)])
    comment += ') * ('
    comment += ' + '.join([str(c) + ' * %s' % getBlockName( 1, i, dims, level ) \
                               for i, c in enumerate(b_coeffs) if is_nonzero(c)])
    comment += '); '
    comment += '; '.join([' %s += %s * M%d' % ( getBlockName( 2, i, dims, level ), c, index ) for i, c in enumerate(c_coeffs) if is_nonzero(c)])
    comment += ';'
    write_line(myfile, 1, comment)

    write_line( myfile, 1, '{' )

    #write_stra_mat( myfile, 0, a_coeffs, index, 'ms, ks', dims, level )
    #write_stra_mat( myfile, 1, b_coeffs, index, 'ks, ns', dims, level )
    #write_stra_mat( myfile, 2, c_coeffs, index, 'ms, ns', dims, level )

    add = add_submat_arg( 0, a_coeffs, level, dims, index )
    write_line(myfile, 1, add)
    add = add_submat_arg( 1, b_coeffs, level, dims, index )
    write_line(myfile, 1, add)
    add = add_submat_arg( 2, c_coeffs, level, dims, index )
    write_line(myfile, 1, add)

    add = add_coeff_arg( 0, a_coeffs, level, index )
    write_line(myfile, 1, add)
    add = add_coeff_arg( 1, b_coeffs, level, index )
    write_line(myfile, 1, add)
    add = add_coeff_arg( 2, c_coeffs, level, index )
    write_line(myfile, 1, add)

 
    #template_coeffs = ''
    #template_coeffs += add_coeff_arg( a_coeffs, level )
    ##print template_coeffs
    #template_coeffs += ', '
    #template_coeffs += add_coeff_arg( b_coeffs, level )
    ##print template_coeffs
    #template_coeffs += ', '
    #template_coeffs += add_coeff_arg( c_coeffs, level, 1 )
    ##print template_coeffs
    ##print "####"


    add = 'stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_{0}( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, '.format( index ) 
    #add += ', '.join( [str(c) + ' * %s' % getBlockName( 0, i, dims, level ) \
    #                           for i, c in reorder_coeff( a_coeffs ) ] )

    add += 'thrust::raw_pointer_cast( &d_a_list{0}[0] ), thrust::raw_pointer_cast( &coeff_a_list{0}[0] ), thrust::raw_pointer_cast( &d_b_list{0}[0] ), thrust::raw_pointer_cast( &coeff_b_list{0}[0] ), thrust::raw_pointer_cast( &d_c_list{0}[0] ), thrust::raw_pointer_cast( &coeff_c_list{0}[0] ) );'.format( index )

    write_line(myfile, 1, add)
   


    template_coeffs = str( len( reorder_coeff( a_coeffs ) ) ) + ','
    template_coeffs += str( len( reorder_coeff( b_coeffs ) ) ) + ','
    template_coeffs += str( len( reorder_coeff( c_coeffs ) ) )

    add = 'stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, {1}><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_{0});'.format( index, template_coeffs )

    write_line( myfile, 1, add )

    write_line( myfile, 1, '}' )

    write_break( myfile )

def getNNZ ( coeffs ):
    nonzero_coeffs = [coeff for coeff in coeffs if is_nonzero(coeff)]
    nnz = len( nonzero_coeffs )
    return nnz


def gen_abc_fmm( coeff_filename, dims, level, outfilename ):

    coeffs = read_coeffs( coeff_filename )
    #print coeffs
    #print coeffs[0][0]

    #coeffs2= [ transpose( U2 ), transpose( V2 ), transpose( W2 ) ]

    with open( outfilename, 'w' ) as myfile:

        cur_coeffs = generateCoeffs( coeffs, level )
        #writeCoeffs( cur_coeffs )
        #writeEquation( cur_coeffs, dims, level )


        num_multiplies = len(cur_coeffs[0][0])

        writePartition( myfile, dims, level )

        write_break( myfile )

        create_straprim_caller( myfile, cur_coeffs, dims, num_multiplies, level )



        #writePreamble( myfile, dims )
        #writePartition( myfile, dims, level )
        #cur_coeffs = generateCoeffs( coeffs, level )
        #writeCoeffs( cur_coeffs )
        #writeEquation( cur_coeffs, dims, level )
        #writeFMM( myfile, cur_coeffs, dims, level )

def main():
    try:
        coeff_file = sys.argv[1]
        print coeff_file
        dims = tuple([int(d) for d in sys.argv[2].split(',')])
        print dims

        outfile = 'a.c'

        level = 1
        if len(sys.argv) > 3:
            level = int( sys.argv[3] )
        if len(sys.argv) > 4:
            outfile = sys.argv[4]

        print outfile

        print "level: " + str( level )
        print 'Generating code for %d x %d x %d' % dims
    except:
        raise Exception('USAGE: python abc_gen.py coeff_file m,n,p out_file')

    gen_abc_fmm( coeff_file, dims, level, outfile )

if __name__ == '__main__':
    main()

