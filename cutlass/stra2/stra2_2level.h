    int lda, ldb, ldc;
    gen_leading_dim<TransformA>( m, k, lda );
    gen_leading_dim<TransformB>( k, n, ldb );
    gen_leading_dim<matrix_transform_t::NonTranspose>( m, n, ldc );
    int ms, ks, ns;
    int md, kd, nd;
    int mr, kr, nr;
    mr = m % ( 4 ), kr = k % ( 4 ), nr = n % ( 4 );
    md = m - mr, kd = k - kr, nd = n - nr;
    ms=md, ks=kd, ns=nd;
    value_t *d_a_0, *d_a_1, *d_a_2, *d_a_3;
    stra2_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 0, d_a, &d_a_0 );
    stra2_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 1, d_a, &d_a_1 );
    stra2_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 0, d_a, &d_a_2 );
    stra2_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 1, d_a, &d_a_3 );
    ms=ms/2, ks=ks/2, ns=ns/2;
    value_t *d_a_0_0, *d_a_0_1, *d_a_0_2, *d_a_0_3;
    stra2_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 0, d_a_0, &d_a_0_0 );
    stra2_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 1, d_a_0, &d_a_0_1 );
    stra2_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 0, d_a_0, &d_a_0_2 );
    stra2_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 1, d_a_0, &d_a_0_3 );
    value_t *d_a_1_0, *d_a_1_1, *d_a_1_2, *d_a_1_3;
    stra2_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 0, d_a_1, &d_a_1_0 );
    stra2_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 1, d_a_1, &d_a_1_1 );
    stra2_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 0, d_a_1, &d_a_1_2 );
    stra2_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 1, d_a_1, &d_a_1_3 );
    value_t *d_a_2_0, *d_a_2_1, *d_a_2_2, *d_a_2_3;
    stra2_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 0, d_a_2, &d_a_2_0 );
    stra2_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 1, d_a_2, &d_a_2_1 );
    stra2_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 0, d_a_2, &d_a_2_2 );
    stra2_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 1, d_a_2, &d_a_2_3 );
    value_t *d_a_3_0, *d_a_3_1, *d_a_3_2, *d_a_3_3;
    stra2_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 0, d_a_3, &d_a_3_0 );
    stra2_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 1, d_a_3, &d_a_3_1 );
    stra2_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 0, d_a_3, &d_a_3_2 );
    stra2_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 1, d_a_3, &d_a_3_3 );
    ms=md, ks=kd, ns=nd;
    value_t *d_b_0, *d_b_1, *d_b_2, *d_b_3;
    stra2_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 0, d_b, &d_b_0 );
    stra2_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 1, d_b, &d_b_1 );
    stra2_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 0, d_b, &d_b_2 );
    stra2_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 1, d_b, &d_b_3 );
    ms=ms/2, ks=ks/2, ns=ns/2;
    value_t *d_b_0_0, *d_b_0_1, *d_b_0_2, *d_b_0_3;
    stra2_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 0, d_b_0, &d_b_0_0 );
    stra2_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 1, d_b_0, &d_b_0_1 );
    stra2_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 0, d_b_0, &d_b_0_2 );
    stra2_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 1, d_b_0, &d_b_0_3 );
    value_t *d_b_1_0, *d_b_1_1, *d_b_1_2, *d_b_1_3;
    stra2_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 0, d_b_1, &d_b_1_0 );
    stra2_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 1, d_b_1, &d_b_1_1 );
    stra2_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 0, d_b_1, &d_b_1_2 );
    stra2_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 1, d_b_1, &d_b_1_3 );
    value_t *d_b_2_0, *d_b_2_1, *d_b_2_2, *d_b_2_3;
    stra2_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 0, d_b_2, &d_b_2_0 );
    stra2_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 1, d_b_2, &d_b_2_1 );
    stra2_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 0, d_b_2, &d_b_2_2 );
    stra2_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 1, d_b_2, &d_b_2_3 );
    value_t *d_b_3_0, *d_b_3_1, *d_b_3_2, *d_b_3_3;
    stra2_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 0, d_b_3, &d_b_3_0 );
    stra2_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 1, d_b_3, &d_b_3_1 );
    stra2_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 0, d_b_3, &d_b_3_2 );
    stra2_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 1, d_b_3, &d_b_3_3 );
    ms=md, ks=kd, ns=nd;
    accum_t *d_c_0, *d_c_1, *d_c_2, *d_c_3;
    stra2_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 0, d_c, &d_c_0 );
    stra2_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 1, d_c, &d_c_1 );
    stra2_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 0, d_c, &d_c_2 );
    stra2_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 1, d_c, &d_c_3 );
    ms=ms/2, ks=ks/2, ns=ns/2;
    accum_t *d_c_0_0, *d_c_0_1, *d_c_0_2, *d_c_0_3;
    stra2_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 0, d_c_0, &d_c_0_0 );
    stra2_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 1, d_c_0, &d_c_0_1 );
    stra2_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 0, d_c_0, &d_c_0_2 );
    stra2_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 1, d_c_0, &d_c_0_3 );
    accum_t *d_c_1_0, *d_c_1_1, *d_c_1_2, *d_c_1_3;
    stra2_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 0, d_c_1, &d_c_1_0 );
    stra2_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 1, d_c_1, &d_c_1_1 );
    stra2_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 0, d_c_1, &d_c_1_2 );
    stra2_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 1, d_c_1, &d_c_1_3 );
    accum_t *d_c_2_0, *d_c_2_1, *d_c_2_2, *d_c_2_3;
    stra2_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 0, d_c_2, &d_c_2_0 );
    stra2_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 1, d_c_2, &d_c_2_1 );
    stra2_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 0, d_c_2, &d_c_2_2 );
    stra2_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 1, d_c_2, &d_c_2_3 );
    accum_t *d_c_3_0, *d_c_3_1, *d_c_3_2, *d_c_3_3;
    stra2_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 0, d_c_3, &d_c_3_0 );
    stra2_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 1, d_c_3, &d_c_3_1 );
    stra2_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 0, d_c_3, &d_c_3_2 );
    stra2_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 1, d_c_3, &d_c_3_3 );
    ms=ms/2, ks=ks/2, ns=ns/2;


    // M0 = (1.0 * a_0_0 + 1.0 * a_0_3 + 1.0 * a_3_0 + 1.0 * a_3_3) * (1.0 * b_0_0 + 1.0 * b_0_3 + 1.0 * b_3_0 + 1.0 * b_3_3);  c_0_0 += 1.0 * M0;  c_0_3 += 1.0 * M0;  c_3_0 += 1.0 * M0;  c_3_3 += 1.0 * M0;
    {
    thrust::device_vector<value_t *> d_a_list0(4); d_a_list0[0] = d_a_0_0; d_a_list0[1] = d_a_0_3; d_a_list0[2] = d_a_3_0; d_a_list0[3] = d_a_3_3;
    thrust::device_vector<value_t *> d_b_list0(4); d_b_list0[0] = d_b_0_0; d_b_list0[1] = d_b_0_3; d_b_list0[2] = d_b_3_0; d_b_list0[3] = d_b_3_3;
    thrust::device_vector<accum_t *> d_c_list0(4); d_c_list0[0] = d_c_0_0; d_c_list0[1] = d_c_0_3; d_c_list0[2] = d_c_3_0; d_c_list0[3] = d_c_3_3;
    thrust::device_vector<value_t> coeff_a_list0(4); coeff_a_list0[0] = 1.0; coeff_a_list0[1] = 1.0; coeff_a_list0[2] = 1.0;
    thrust::device_vector<value_t> coeff_b_list0(4); coeff_b_list0[0] = 1.0; coeff_b_list0[1] = 1.0; coeff_b_list0[2] = 1.0;
    thrust::device_vector<accum_t> coeff_c_list0(4); coeff_c_list0[0] = 1.0; coeff_c_list0[1] = 1.0; coeff_c_list0[2] = 1.0; coeff_c_list0[3] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_0( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list0[0] ), thrust::raw_pointer_cast( &coeff_a_list0[0] ), thrust::raw_pointer_cast( &d_b_list0[0] ), thrust::raw_pointer_cast( &coeff_b_list0[0] ), thrust::raw_pointer_cast( &d_c_list0[0] ), thrust::raw_pointer_cast( &coeff_c_list0[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,4,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_0);
    }

    // M1 = (1.0 * a_0_2 + 1.0 * a_0_3 + 1.0 * a_3_2 + 1.0 * a_3_3) * (1.0 * b_0_0 + 1.0 * b_3_0);  c_0_2 += 1.0 * M1;  c_0_3 += -1.0 * M1;  c_3_2 += 1.0 * M1;  c_3_3 += -1.0 * M1;
    {
    thrust::device_vector<value_t *> d_a_list1(4); d_a_list1[0] = d_a_0_2; d_a_list1[1] = d_a_0_3; d_a_list1[2] = d_a_3_2; d_a_list1[3] = d_a_3_3;
    thrust::device_vector<value_t *> d_b_list1(2); d_b_list1[0] = d_b_0_0; d_b_list1[1] = d_b_3_0;
    thrust::device_vector<accum_t *> d_c_list1(4); d_c_list1[0] = d_c_0_2; d_c_list1[1] = d_c_3_2; d_c_list1[2] = d_c_0_3; d_c_list1[3] = d_c_3_3;
    thrust::device_vector<value_t> coeff_a_list1(4); coeff_a_list1[0] = 1.0; coeff_a_list1[1] = 1.0; coeff_a_list1[2] = 1.0;
    thrust::device_vector<value_t> coeff_b_list1(2); coeff_b_list1[0] = 1.0;
    thrust::device_vector<accum_t> coeff_c_list1(4); coeff_c_list1[0] = 1.0; coeff_c_list1[1] = 1.0; coeff_c_list1[2] = -1.0; coeff_c_list1[3] = -1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_1( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list1[0] ), thrust::raw_pointer_cast( &coeff_a_list1[0] ), thrust::raw_pointer_cast( &d_b_list1[0] ), thrust::raw_pointer_cast( &coeff_b_list1[0] ), thrust::raw_pointer_cast( &d_c_list1[0] ), thrust::raw_pointer_cast( &coeff_c_list1[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,2,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_1);
    }

    // M2 = (1.0 * a_0_0 + 1.0 * a_3_0) * (1.0 * b_0_1 + -1.0 * b_0_3 + 1.0 * b_3_1 + -1.0 * b_3_3);  c_0_1 += 1.0 * M2;  c_0_3 += 1.0 * M2;  c_3_1 += 1.0 * M2;  c_3_3 += 1.0 * M2;
    {
    thrust::device_vector<value_t *> d_a_list2(2); d_a_list2[0] = d_a_0_0; d_a_list2[1] = d_a_3_0;
    thrust::device_vector<value_t *> d_b_list2(4); d_b_list2[0] = d_b_0_1; d_b_list2[1] = d_b_3_1; d_b_list2[2] = d_b_0_3; d_b_list2[3] = d_b_3_3;
    thrust::device_vector<accum_t *> d_c_list2(4); d_c_list2[0] = d_c_0_1; d_c_list2[1] = d_c_0_3; d_c_list2[2] = d_c_3_1; d_c_list2[3] = d_c_3_3;
    thrust::device_vector<value_t> coeff_a_list2(2); coeff_a_list2[0] = 1.0;
    thrust::device_vector<value_t> coeff_b_list2(4); coeff_b_list2[0] = 1.0; coeff_b_list2[1] = -1.0; coeff_b_list2[2] = -1.0;
    thrust::device_vector<accum_t> coeff_c_list2(4); coeff_c_list2[0] = 1.0; coeff_c_list2[1] = 1.0; coeff_c_list2[2] = 1.0; coeff_c_list2[3] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_2( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list2[0] ), thrust::raw_pointer_cast( &coeff_a_list2[0] ), thrust::raw_pointer_cast( &d_b_list2[0] ), thrust::raw_pointer_cast( &coeff_b_list2[0] ), thrust::raw_pointer_cast( &d_c_list2[0] ), thrust::raw_pointer_cast( &coeff_c_list2[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,4,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_2);
    }

    // M3 = (1.0 * a_0_3 + 1.0 * a_3_3) * (-1.0 * b_0_0 + 1.0 * b_0_2 + -1.0 * b_3_0 + 1.0 * b_3_2);  c_0_0 += 1.0 * M3;  c_0_2 += 1.0 * M3;  c_3_0 += 1.0 * M3;  c_3_2 += 1.0 * M3;
    {
    thrust::device_vector<value_t *> d_a_list3(2); d_a_list3[0] = d_a_0_3; d_a_list3[1] = d_a_3_3;
    thrust::device_vector<value_t *> d_b_list3(4); d_b_list3[0] = d_b_0_2; d_b_list3[1] = d_b_3_2; d_b_list3[2] = d_b_0_0; d_b_list3[3] = d_b_3_0;
    thrust::device_vector<accum_t *> d_c_list3(4); d_c_list3[0] = d_c_0_0; d_c_list3[1] = d_c_0_2; d_c_list3[2] = d_c_3_0; d_c_list3[3] = d_c_3_2;
    thrust::device_vector<value_t> coeff_a_list3(2); coeff_a_list3[0] = 1.0;
    thrust::device_vector<value_t> coeff_b_list3(4); coeff_b_list3[0] = 1.0; coeff_b_list3[1] = -1.0; coeff_b_list3[2] = -1.0;
    thrust::device_vector<accum_t> coeff_c_list3(4); coeff_c_list3[0] = 1.0; coeff_c_list3[1] = 1.0; coeff_c_list3[2] = 1.0; coeff_c_list3[3] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_3( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list3[0] ), thrust::raw_pointer_cast( &coeff_a_list3[0] ), thrust::raw_pointer_cast( &d_b_list3[0] ), thrust::raw_pointer_cast( &coeff_b_list3[0] ), thrust::raw_pointer_cast( &d_c_list3[0] ), thrust::raw_pointer_cast( &coeff_c_list3[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,4,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_3);
    }

    // M4 = (1.0 * a_0_0 + 1.0 * a_0_1 + 1.0 * a_3_0 + 1.0 * a_3_1) * (1.0 * b_0_3 + 1.0 * b_3_3);  c_0_0 += -1.0 * M4;  c_0_1 += 1.0 * M4;  c_3_0 += -1.0 * M4;  c_3_1 += 1.0 * M4;
    {
    thrust::device_vector<value_t *> d_a_list4(4); d_a_list4[0] = d_a_0_0; d_a_list4[1] = d_a_0_1; d_a_list4[2] = d_a_3_0; d_a_list4[3] = d_a_3_1;
    thrust::device_vector<value_t *> d_b_list4(2); d_b_list4[0] = d_b_0_3; d_b_list4[1] = d_b_3_3;
    thrust::device_vector<accum_t *> d_c_list4(4); d_c_list4[0] = d_c_0_1; d_c_list4[1] = d_c_3_1; d_c_list4[2] = d_c_0_0; d_c_list4[3] = d_c_3_0;
    thrust::device_vector<value_t> coeff_a_list4(4); coeff_a_list4[0] = 1.0; coeff_a_list4[1] = 1.0; coeff_a_list4[2] = 1.0;
    thrust::device_vector<value_t> coeff_b_list4(2); coeff_b_list4[0] = 1.0;
    thrust::device_vector<accum_t> coeff_c_list4(4); coeff_c_list4[0] = 1.0; coeff_c_list4[1] = 1.0; coeff_c_list4[2] = -1.0; coeff_c_list4[3] = -1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_4( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list4[0] ), thrust::raw_pointer_cast( &coeff_a_list4[0] ), thrust::raw_pointer_cast( &d_b_list4[0] ), thrust::raw_pointer_cast( &coeff_b_list4[0] ), thrust::raw_pointer_cast( &d_c_list4[0] ), thrust::raw_pointer_cast( &coeff_c_list4[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,2,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_4);
    }

    // M5 = (-1.0 * a_0_0 + 1.0 * a_0_2 + -1.0 * a_3_0 + 1.0 * a_3_2) * (1.0 * b_0_0 + 1.0 * b_0_1 + 1.0 * b_3_0 + 1.0 * b_3_1);  c_0_3 += 1.0 * M5;  c_3_3 += 1.0 * M5;
    {
    thrust::device_vector<value_t *> d_a_list5(4); d_a_list5[0] = d_a_0_2; d_a_list5[1] = d_a_3_2; d_a_list5[2] = d_a_0_0; d_a_list5[3] = d_a_3_0;
    thrust::device_vector<value_t *> d_b_list5(4); d_b_list5[0] = d_b_0_0; d_b_list5[1] = d_b_0_1; d_b_list5[2] = d_b_3_0; d_b_list5[3] = d_b_3_1;
    thrust::device_vector<accum_t *> d_c_list5(2); d_c_list5[0] = d_c_0_3; d_c_list5[1] = d_c_3_3;
    thrust::device_vector<value_t> coeff_a_list5(4); coeff_a_list5[0] = 1.0; coeff_a_list5[1] = -1.0; coeff_a_list5[2] = -1.0;
    thrust::device_vector<value_t> coeff_b_list5(4); coeff_b_list5[0] = 1.0; coeff_b_list5[1] = 1.0; coeff_b_list5[2] = 1.0;
    thrust::device_vector<accum_t> coeff_c_list5(2); coeff_c_list5[0] = 1.0; coeff_c_list5[1] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_5( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list5[0] ), thrust::raw_pointer_cast( &coeff_a_list5[0] ), thrust::raw_pointer_cast( &d_b_list5[0] ), thrust::raw_pointer_cast( &coeff_b_list5[0] ), thrust::raw_pointer_cast( &d_c_list5[0] ), thrust::raw_pointer_cast( &coeff_c_list5[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,4,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_5);
    }

    // M6 = (1.0 * a_0_1 + -1.0 * a_0_3 + 1.0 * a_3_1 + -1.0 * a_3_3) * (1.0 * b_0_2 + 1.0 * b_0_3 + 1.0 * b_3_2 + 1.0 * b_3_3);  c_0_0 += 1.0 * M6;  c_3_0 += 1.0 * M6;
    {
    thrust::device_vector<value_t *> d_a_list6(4); d_a_list6[0] = d_a_0_1; d_a_list6[1] = d_a_3_1; d_a_list6[2] = d_a_0_3; d_a_list6[3] = d_a_3_3;
    thrust::device_vector<value_t *> d_b_list6(4); d_b_list6[0] = d_b_0_2; d_b_list6[1] = d_b_0_3; d_b_list6[2] = d_b_3_2; d_b_list6[3] = d_b_3_3;
    thrust::device_vector<accum_t *> d_c_list6(2); d_c_list6[0] = d_c_0_0; d_c_list6[1] = d_c_3_0;
    thrust::device_vector<value_t> coeff_a_list6(4); coeff_a_list6[0] = 1.0; coeff_a_list6[1] = -1.0; coeff_a_list6[2] = -1.0;
    thrust::device_vector<value_t> coeff_b_list6(4); coeff_b_list6[0] = 1.0; coeff_b_list6[1] = 1.0; coeff_b_list6[2] = 1.0;
    thrust::device_vector<accum_t> coeff_c_list6(2); coeff_c_list6[0] = 1.0; coeff_c_list6[1] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_6( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list6[0] ), thrust::raw_pointer_cast( &coeff_a_list6[0] ), thrust::raw_pointer_cast( &d_b_list6[0] ), thrust::raw_pointer_cast( &coeff_b_list6[0] ), thrust::raw_pointer_cast( &d_c_list6[0] ), thrust::raw_pointer_cast( &coeff_c_list6[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,4,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_6);
    }

    // M7 = (1.0 * a_2_0 + 1.0 * a_2_3 + 1.0 * a_3_0 + 1.0 * a_3_3) * (1.0 * b_0_0 + 1.0 * b_0_3);  c_2_0 += 1.0 * M7;  c_2_3 += 1.0 * M7;  c_3_0 += -1.0 * M7;  c_3_3 += -1.0 * M7;
    {
    thrust::device_vector<value_t *> d_a_list7(4); d_a_list7[0] = d_a_2_0; d_a_list7[1] = d_a_2_3; d_a_list7[2] = d_a_3_0; d_a_list7[3] = d_a_3_3;
    thrust::device_vector<value_t *> d_b_list7(2); d_b_list7[0] = d_b_0_0; d_b_list7[1] = d_b_0_3;
    thrust::device_vector<accum_t *> d_c_list7(4); d_c_list7[0] = d_c_2_0; d_c_list7[1] = d_c_2_3; d_c_list7[2] = d_c_3_0; d_c_list7[3] = d_c_3_3;
    thrust::device_vector<value_t> coeff_a_list7(4); coeff_a_list7[0] = 1.0; coeff_a_list7[1] = 1.0; coeff_a_list7[2] = 1.0;
    thrust::device_vector<value_t> coeff_b_list7(2); coeff_b_list7[0] = 1.0;
    thrust::device_vector<accum_t> coeff_c_list7(4); coeff_c_list7[0] = 1.0; coeff_c_list7[1] = 1.0; coeff_c_list7[2] = -1.0; coeff_c_list7[3] = -1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_7( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list7[0] ), thrust::raw_pointer_cast( &coeff_a_list7[0] ), thrust::raw_pointer_cast( &d_b_list7[0] ), thrust::raw_pointer_cast( &coeff_b_list7[0] ), thrust::raw_pointer_cast( &d_c_list7[0] ), thrust::raw_pointer_cast( &coeff_c_list7[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,2,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_7);
    }

    // M8 = (1.0 * a_2_2 + 1.0 * a_2_3 + 1.0 * a_3_2 + 1.0 * a_3_3) * (1.0 * b_0_0);  c_2_2 += 1.0 * M8;  c_2_3 += -1.0 * M8;  c_3_2 += -1.0 * M8;  c_3_3 += 1.0 * M8;
    {
    thrust::device_vector<value_t *> d_a_list8(4); d_a_list8[0] = d_a_2_2; d_a_list8[1] = d_a_2_3; d_a_list8[2] = d_a_3_2; d_a_list8[3] = d_a_3_3;
    thrust::device_vector<value_t *> d_b_list8(1); d_b_list8[0] = d_b_0_0;
    thrust::device_vector<accum_t *> d_c_list8(4); d_c_list8[0] = d_c_2_2; d_c_list8[1] = d_c_3_3; d_c_list8[2] = d_c_2_3; d_c_list8[3] = d_c_3_2;
    thrust::device_vector<value_t> coeff_a_list8(4); coeff_a_list8[0] = 1.0; coeff_a_list8[1] = 1.0; coeff_a_list8[2] = 1.0;
    thrust::device_vector<value_t> coeff_b_list8(1);
    thrust::device_vector<accum_t> coeff_c_list8(4); coeff_c_list8[0] = 1.0; coeff_c_list8[1] = 1.0; coeff_c_list8[2] = -1.0; coeff_c_list8[3] = -1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_8( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list8[0] ), thrust::raw_pointer_cast( &coeff_a_list8[0] ), thrust::raw_pointer_cast( &d_b_list8[0] ), thrust::raw_pointer_cast( &coeff_b_list8[0] ), thrust::raw_pointer_cast( &d_c_list8[0] ), thrust::raw_pointer_cast( &coeff_c_list8[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,1,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_8);
    }

    // M9 = (1.0 * a_2_0 + 1.0 * a_3_0) * (1.0 * b_0_1 + -1.0 * b_0_3);  c_2_1 += 1.0 * M9;  c_2_3 += 1.0 * M9;  c_3_1 += -1.0 * M9;  c_3_3 += -1.0 * M9;
    {
    thrust::device_vector<value_t *> d_a_list9(2); d_a_list9[0] = d_a_2_0; d_a_list9[1] = d_a_3_0;
    thrust::device_vector<value_t *> d_b_list9(2); d_b_list9[0] = d_b_0_1; d_b_list9[1] = d_b_0_3;
    thrust::device_vector<accum_t *> d_c_list9(4); d_c_list9[0] = d_c_2_1; d_c_list9[1] = d_c_2_3; d_c_list9[2] = d_c_3_1; d_c_list9[3] = d_c_3_3;
    thrust::device_vector<value_t> coeff_a_list9(2); coeff_a_list9[0] = 1.0;
    thrust::device_vector<value_t> coeff_b_list9(2); coeff_b_list9[0] = -1.0;
    thrust::device_vector<accum_t> coeff_c_list9(4); coeff_c_list9[0] = 1.0; coeff_c_list9[1] = 1.0; coeff_c_list9[2] = -1.0; coeff_c_list9[3] = -1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_9( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list9[0] ), thrust::raw_pointer_cast( &coeff_a_list9[0] ), thrust::raw_pointer_cast( &d_b_list9[0] ), thrust::raw_pointer_cast( &coeff_b_list9[0] ), thrust::raw_pointer_cast( &d_c_list9[0] ), thrust::raw_pointer_cast( &coeff_c_list9[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,2,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_9);
    }

    // M10 = (1.0 * a_2_3 + 1.0 * a_3_3) * (-1.0 * b_0_0 + 1.0 * b_0_2);  c_2_0 += 1.0 * M10;  c_2_2 += 1.0 * M10;  c_3_0 += -1.0 * M10;  c_3_2 += -1.0 * M10;
    {
    thrust::device_vector<value_t *> d_a_list10(2); d_a_list10[0] = d_a_2_3; d_a_list10[1] = d_a_3_3;
    thrust::device_vector<value_t *> d_b_list10(2); d_b_list10[0] = d_b_0_2; d_b_list10[1] = d_b_0_0;
    thrust::device_vector<accum_t *> d_c_list10(4); d_c_list10[0] = d_c_2_0; d_c_list10[1] = d_c_2_2; d_c_list10[2] = d_c_3_0; d_c_list10[3] = d_c_3_2;
    thrust::device_vector<value_t> coeff_a_list10(2); coeff_a_list10[0] = 1.0;
    thrust::device_vector<value_t> coeff_b_list10(2); coeff_b_list10[0] = -1.0;
    thrust::device_vector<accum_t> coeff_c_list10(4); coeff_c_list10[0] = 1.0; coeff_c_list10[1] = 1.0; coeff_c_list10[2] = -1.0; coeff_c_list10[3] = -1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_10( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list10[0] ), thrust::raw_pointer_cast( &coeff_a_list10[0] ), thrust::raw_pointer_cast( &d_b_list10[0] ), thrust::raw_pointer_cast( &coeff_b_list10[0] ), thrust::raw_pointer_cast( &d_c_list10[0] ), thrust::raw_pointer_cast( &coeff_c_list10[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,2,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_10);
    }

    // M11 = (1.0 * a_2_0 + 1.0 * a_2_1 + 1.0 * a_3_0 + 1.0 * a_3_1) * (1.0 * b_0_3);  c_2_0 += -1.0 * M11;  c_2_1 += 1.0 * M11;  c_3_0 += 1.0 * M11;  c_3_1 += -1.0 * M11;
    {
    thrust::device_vector<value_t *> d_a_list11(4); d_a_list11[0] = d_a_2_0; d_a_list11[1] = d_a_2_1; d_a_list11[2] = d_a_3_0; d_a_list11[3] = d_a_3_1;
    thrust::device_vector<value_t *> d_b_list11(1); d_b_list11[0] = d_b_0_3;
    thrust::device_vector<accum_t *> d_c_list11(4); d_c_list11[0] = d_c_2_1; d_c_list11[1] = d_c_3_0; d_c_list11[2] = d_c_2_0; d_c_list11[3] = d_c_3_1;
    thrust::device_vector<value_t> coeff_a_list11(4); coeff_a_list11[0] = 1.0; coeff_a_list11[1] = 1.0; coeff_a_list11[2] = 1.0;
    thrust::device_vector<value_t> coeff_b_list11(1);
    thrust::device_vector<accum_t> coeff_c_list11(4); coeff_c_list11[0] = 1.0; coeff_c_list11[1] = 1.0; coeff_c_list11[2] = -1.0; coeff_c_list11[3] = -1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_11( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list11[0] ), thrust::raw_pointer_cast( &coeff_a_list11[0] ), thrust::raw_pointer_cast( &d_b_list11[0] ), thrust::raw_pointer_cast( &coeff_b_list11[0] ), thrust::raw_pointer_cast( &d_c_list11[0] ), thrust::raw_pointer_cast( &coeff_c_list11[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,1,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_11);
    }

    // M12 = (-1.0 * a_2_0 + 1.0 * a_2_2 + -1.0 * a_3_0 + 1.0 * a_3_2) * (1.0 * b_0_0 + 1.0 * b_0_1);  c_2_3 += 1.0 * M12;  c_3_3 += -1.0 * M12;
    {
    thrust::device_vector<value_t *> d_a_list12(4); d_a_list12[0] = d_a_2_2; d_a_list12[1] = d_a_3_2; d_a_list12[2] = d_a_2_0; d_a_list12[3] = d_a_3_0;
    thrust::device_vector<value_t *> d_b_list12(2); d_b_list12[0] = d_b_0_0; d_b_list12[1] = d_b_0_1;
    thrust::device_vector<accum_t *> d_c_list12(2); d_c_list12[0] = d_c_2_3; d_c_list12[1] = d_c_3_3;
    thrust::device_vector<value_t> coeff_a_list12(4); coeff_a_list12[0] = 1.0; coeff_a_list12[1] = -1.0; coeff_a_list12[2] = -1.0;
    thrust::device_vector<value_t> coeff_b_list12(2); coeff_b_list12[0] = 1.0;
    thrust::device_vector<accum_t> coeff_c_list12(2); coeff_c_list12[0] = 1.0; coeff_c_list12[1] = -1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_12( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list12[0] ), thrust::raw_pointer_cast( &coeff_a_list12[0] ), thrust::raw_pointer_cast( &d_b_list12[0] ), thrust::raw_pointer_cast( &coeff_b_list12[0] ), thrust::raw_pointer_cast( &d_c_list12[0] ), thrust::raw_pointer_cast( &coeff_c_list12[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,2,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_12);
    }

    // M13 = (1.0 * a_2_1 + -1.0 * a_2_3 + 1.0 * a_3_1 + -1.0 * a_3_3) * (1.0 * b_0_2 + 1.0 * b_0_3);  c_2_0 += 1.0 * M13;  c_3_0 += -1.0 * M13;
    {
    thrust::device_vector<value_t *> d_a_list13(4); d_a_list13[0] = d_a_2_1; d_a_list13[1] = d_a_3_1; d_a_list13[2] = d_a_2_3; d_a_list13[3] = d_a_3_3;
    thrust::device_vector<value_t *> d_b_list13(2); d_b_list13[0] = d_b_0_2; d_b_list13[1] = d_b_0_3;
    thrust::device_vector<accum_t *> d_c_list13(2); d_c_list13[0] = d_c_2_0; d_c_list13[1] = d_c_3_0;
    thrust::device_vector<value_t> coeff_a_list13(4); coeff_a_list13[0] = 1.0; coeff_a_list13[1] = -1.0; coeff_a_list13[2] = -1.0;
    thrust::device_vector<value_t> coeff_b_list13(2); coeff_b_list13[0] = 1.0;
    thrust::device_vector<accum_t> coeff_c_list13(2); coeff_c_list13[0] = 1.0; coeff_c_list13[1] = -1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_13( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list13[0] ), thrust::raw_pointer_cast( &coeff_a_list13[0] ), thrust::raw_pointer_cast( &d_b_list13[0] ), thrust::raw_pointer_cast( &coeff_b_list13[0] ), thrust::raw_pointer_cast( &d_c_list13[0] ), thrust::raw_pointer_cast( &coeff_c_list13[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,2,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_13);
    }

    // M14 = (1.0 * a_0_0 + 1.0 * a_0_3) * (1.0 * b_1_0 + 1.0 * b_1_3 + -1.0 * b_3_0 + -1.0 * b_3_3);  c_1_0 += 1.0 * M14;  c_1_3 += 1.0 * M14;  c_3_0 += 1.0 * M14;  c_3_3 += 1.0 * M14;
    {
    thrust::device_vector<value_t *> d_a_list14(2); d_a_list14[0] = d_a_0_0; d_a_list14[1] = d_a_0_3;
    thrust::device_vector<value_t *> d_b_list14(4); d_b_list14[0] = d_b_1_0; d_b_list14[1] = d_b_1_3; d_b_list14[2] = d_b_3_0; d_b_list14[3] = d_b_3_3;
    thrust::device_vector<accum_t *> d_c_list14(4); d_c_list14[0] = d_c_1_0; d_c_list14[1] = d_c_1_3; d_c_list14[2] = d_c_3_0; d_c_list14[3] = d_c_3_3;
    thrust::device_vector<value_t> coeff_a_list14(2); coeff_a_list14[0] = 1.0;
    thrust::device_vector<value_t> coeff_b_list14(4); coeff_b_list14[0] = 1.0; coeff_b_list14[1] = -1.0; coeff_b_list14[2] = -1.0;
    thrust::device_vector<accum_t> coeff_c_list14(4); coeff_c_list14[0] = 1.0; coeff_c_list14[1] = 1.0; coeff_c_list14[2] = 1.0; coeff_c_list14[3] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_14( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list14[0] ), thrust::raw_pointer_cast( &coeff_a_list14[0] ), thrust::raw_pointer_cast( &d_b_list14[0] ), thrust::raw_pointer_cast( &coeff_b_list14[0] ), thrust::raw_pointer_cast( &d_c_list14[0] ), thrust::raw_pointer_cast( &coeff_c_list14[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,4,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_14);
    }

    // M15 = (1.0 * a_0_2 + 1.0 * a_0_3) * (1.0 * b_1_0 + -1.0 * b_3_0);  c_1_2 += 1.0 * M15;  c_1_3 += -1.0 * M15;  c_3_2 += 1.0 * M15;  c_3_3 += -1.0 * M15;
    {
    thrust::device_vector<value_t *> d_a_list15(2); d_a_list15[0] = d_a_0_2; d_a_list15[1] = d_a_0_3;
    thrust::device_vector<value_t *> d_b_list15(2); d_b_list15[0] = d_b_1_0; d_b_list15[1] = d_b_3_0;
    thrust::device_vector<accum_t *> d_c_list15(4); d_c_list15[0] = d_c_1_2; d_c_list15[1] = d_c_3_2; d_c_list15[2] = d_c_1_3; d_c_list15[3] = d_c_3_3;
    thrust::device_vector<value_t> coeff_a_list15(2); coeff_a_list15[0] = 1.0;
    thrust::device_vector<value_t> coeff_b_list15(2); coeff_b_list15[0] = -1.0;
    thrust::device_vector<accum_t> coeff_c_list15(4); coeff_c_list15[0] = 1.0; coeff_c_list15[1] = 1.0; coeff_c_list15[2] = -1.0; coeff_c_list15[3] = -1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_15( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list15[0] ), thrust::raw_pointer_cast( &coeff_a_list15[0] ), thrust::raw_pointer_cast( &d_b_list15[0] ), thrust::raw_pointer_cast( &coeff_b_list15[0] ), thrust::raw_pointer_cast( &d_c_list15[0] ), thrust::raw_pointer_cast( &coeff_c_list15[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,2,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_15);
    }

    // M16 = (1.0 * a_0_0) * (1.0 * b_1_1 + -1.0 * b_1_3 + -1.0 * b_3_1 + 1.0 * b_3_3);  c_1_1 += 1.0 * M16;  c_1_3 += 1.0 * M16;  c_3_1 += 1.0 * M16;  c_3_3 += 1.0 * M16;
    {
    thrust::device_vector<value_t *> d_a_list16(1); d_a_list16[0] = d_a_0_0;
    thrust::device_vector<value_t *> d_b_list16(4); d_b_list16[0] = d_b_1_1; d_b_list16[1] = d_b_3_3; d_b_list16[2] = d_b_1_3; d_b_list16[3] = d_b_3_1;
    thrust::device_vector<accum_t *> d_c_list16(4); d_c_list16[0] = d_c_1_1; d_c_list16[1] = d_c_1_3; d_c_list16[2] = d_c_3_1; d_c_list16[3] = d_c_3_3;
    thrust::device_vector<value_t> coeff_a_list16(1);
    thrust::device_vector<value_t> coeff_b_list16(4); coeff_b_list16[0] = 1.0; coeff_b_list16[1] = -1.0; coeff_b_list16[2] = -1.0;
    thrust::device_vector<accum_t> coeff_c_list16(4); coeff_c_list16[0] = 1.0; coeff_c_list16[1] = 1.0; coeff_c_list16[2] = 1.0; coeff_c_list16[3] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_16( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list16[0] ), thrust::raw_pointer_cast( &coeff_a_list16[0] ), thrust::raw_pointer_cast( &d_b_list16[0] ), thrust::raw_pointer_cast( &coeff_b_list16[0] ), thrust::raw_pointer_cast( &d_c_list16[0] ), thrust::raw_pointer_cast( &coeff_c_list16[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1,4,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_16);
    }

    // M17 = (1.0 * a_0_3) * (-1.0 * b_1_0 + 1.0 * b_1_2 + 1.0 * b_3_0 + -1.0 * b_3_2);  c_1_0 += 1.0 * M17;  c_1_2 += 1.0 * M17;  c_3_0 += 1.0 * M17;  c_3_2 += 1.0 * M17;
    {
    thrust::device_vector<value_t *> d_a_list17(1); d_a_list17[0] = d_a_0_3;
    thrust::device_vector<value_t *> d_b_list17(4); d_b_list17[0] = d_b_1_2; d_b_list17[1] = d_b_3_0; d_b_list17[2] = d_b_1_0; d_b_list17[3] = d_b_3_2;
    thrust::device_vector<accum_t *> d_c_list17(4); d_c_list17[0] = d_c_1_0; d_c_list17[1] = d_c_1_2; d_c_list17[2] = d_c_3_0; d_c_list17[3] = d_c_3_2;
    thrust::device_vector<value_t> coeff_a_list17(1);
    thrust::device_vector<value_t> coeff_b_list17(4); coeff_b_list17[0] = 1.0; coeff_b_list17[1] = -1.0; coeff_b_list17[2] = -1.0;
    thrust::device_vector<accum_t> coeff_c_list17(4); coeff_c_list17[0] = 1.0; coeff_c_list17[1] = 1.0; coeff_c_list17[2] = 1.0; coeff_c_list17[3] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_17( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list17[0] ), thrust::raw_pointer_cast( &coeff_a_list17[0] ), thrust::raw_pointer_cast( &d_b_list17[0] ), thrust::raw_pointer_cast( &coeff_b_list17[0] ), thrust::raw_pointer_cast( &d_c_list17[0] ), thrust::raw_pointer_cast( &coeff_c_list17[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1,4,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_17);
    }

    // M18 = (1.0 * a_0_0 + 1.0 * a_0_1) * (1.0 * b_1_3 + -1.0 * b_3_3);  c_1_0 += -1.0 * M18;  c_1_1 += 1.0 * M18;  c_3_0 += -1.0 * M18;  c_3_1 += 1.0 * M18;
    {
    thrust::device_vector<value_t *> d_a_list18(2); d_a_list18[0] = d_a_0_0; d_a_list18[1] = d_a_0_1;
    thrust::device_vector<value_t *> d_b_list18(2); d_b_list18[0] = d_b_1_3; d_b_list18[1] = d_b_3_3;
    thrust::device_vector<accum_t *> d_c_list18(4); d_c_list18[0] = d_c_1_1; d_c_list18[1] = d_c_3_1; d_c_list18[2] = d_c_1_0; d_c_list18[3] = d_c_3_0;
    thrust::device_vector<value_t> coeff_a_list18(2); coeff_a_list18[0] = 1.0;
    thrust::device_vector<value_t> coeff_b_list18(2); coeff_b_list18[0] = -1.0;
    thrust::device_vector<accum_t> coeff_c_list18(4); coeff_c_list18[0] = 1.0; coeff_c_list18[1] = 1.0; coeff_c_list18[2] = -1.0; coeff_c_list18[3] = -1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_18( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list18[0] ), thrust::raw_pointer_cast( &coeff_a_list18[0] ), thrust::raw_pointer_cast( &d_b_list18[0] ), thrust::raw_pointer_cast( &coeff_b_list18[0] ), thrust::raw_pointer_cast( &d_c_list18[0] ), thrust::raw_pointer_cast( &coeff_c_list18[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,2,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_18);
    }

    // M19 = (-1.0 * a_0_0 + 1.0 * a_0_2) * (1.0 * b_1_0 + 1.0 * b_1_1 + -1.0 * b_3_0 + -1.0 * b_3_1);  c_1_3 += 1.0 * M19;  c_3_3 += 1.0 * M19;
    {
    thrust::device_vector<value_t *> d_a_list19(2); d_a_list19[0] = d_a_0_2; d_a_list19[1] = d_a_0_0;
    thrust::device_vector<value_t *> d_b_list19(4); d_b_list19[0] = d_b_1_0; d_b_list19[1] = d_b_1_1; d_b_list19[2] = d_b_3_0; d_b_list19[3] = d_b_3_1;
    thrust::device_vector<accum_t *> d_c_list19(2); d_c_list19[0] = d_c_1_3; d_c_list19[1] = d_c_3_3;
    thrust::device_vector<value_t> coeff_a_list19(2); coeff_a_list19[0] = -1.0;
    thrust::device_vector<value_t> coeff_b_list19(4); coeff_b_list19[0] = 1.0; coeff_b_list19[1] = -1.0; coeff_b_list19[2] = -1.0;
    thrust::device_vector<accum_t> coeff_c_list19(2); coeff_c_list19[0] = 1.0; coeff_c_list19[1] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_19( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list19[0] ), thrust::raw_pointer_cast( &coeff_a_list19[0] ), thrust::raw_pointer_cast( &d_b_list19[0] ), thrust::raw_pointer_cast( &coeff_b_list19[0] ), thrust::raw_pointer_cast( &d_c_list19[0] ), thrust::raw_pointer_cast( &coeff_c_list19[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,4,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_19);
    }

    // M20 = (1.0 * a_0_1 + -1.0 * a_0_3) * (1.0 * b_1_2 + 1.0 * b_1_3 + -1.0 * b_3_2 + -1.0 * b_3_3);  c_1_0 += 1.0 * M20;  c_3_0 += 1.0 * M20;
    {
    thrust::device_vector<value_t *> d_a_list20(2); d_a_list20[0] = d_a_0_1; d_a_list20[1] = d_a_0_3;
    thrust::device_vector<value_t *> d_b_list20(4); d_b_list20[0] = d_b_1_2; d_b_list20[1] = d_b_1_3; d_b_list20[2] = d_b_3_2; d_b_list20[3] = d_b_3_3;
    thrust::device_vector<accum_t *> d_c_list20(2); d_c_list20[0] = d_c_1_0; d_c_list20[1] = d_c_3_0;
    thrust::device_vector<value_t> coeff_a_list20(2); coeff_a_list20[0] = -1.0;
    thrust::device_vector<value_t> coeff_b_list20(4); coeff_b_list20[0] = 1.0; coeff_b_list20[1] = -1.0; coeff_b_list20[2] = -1.0;
    thrust::device_vector<accum_t> coeff_c_list20(2); coeff_c_list20[0] = 1.0; coeff_c_list20[1] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_20( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list20[0] ), thrust::raw_pointer_cast( &coeff_a_list20[0] ), thrust::raw_pointer_cast( &d_b_list20[0] ), thrust::raw_pointer_cast( &coeff_b_list20[0] ), thrust::raw_pointer_cast( &d_c_list20[0] ), thrust::raw_pointer_cast( &coeff_c_list20[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,4,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_20);
    }

    // M21 = (1.0 * a_3_0 + 1.0 * a_3_3) * (-1.0 * b_0_0 + -1.0 * b_0_3 + 1.0 * b_2_0 + 1.0 * b_2_3);  c_0_0 += 1.0 * M21;  c_0_3 += 1.0 * M21;  c_2_0 += 1.0 * M21;  c_2_3 += 1.0 * M21;
    {
    thrust::device_vector<value_t *> d_a_list21(2); d_a_list21[0] = d_a_3_0; d_a_list21[1] = d_a_3_3;
    thrust::device_vector<value_t *> d_b_list21(4); d_b_list21[0] = d_b_2_0; d_b_list21[1] = d_b_2_3; d_b_list21[2] = d_b_0_0; d_b_list21[3] = d_b_0_3;
    thrust::device_vector<accum_t *> d_c_list21(4); d_c_list21[0] = d_c_0_0; d_c_list21[1] = d_c_0_3; d_c_list21[2] = d_c_2_0; d_c_list21[3] = d_c_2_3;
    thrust::device_vector<value_t> coeff_a_list21(2); coeff_a_list21[0] = 1.0;
    thrust::device_vector<value_t> coeff_b_list21(4); coeff_b_list21[0] = 1.0; coeff_b_list21[1] = -1.0; coeff_b_list21[2] = -1.0;
    thrust::device_vector<accum_t> coeff_c_list21(4); coeff_c_list21[0] = 1.0; coeff_c_list21[1] = 1.0; coeff_c_list21[2] = 1.0; coeff_c_list21[3] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_21( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list21[0] ), thrust::raw_pointer_cast( &coeff_a_list21[0] ), thrust::raw_pointer_cast( &d_b_list21[0] ), thrust::raw_pointer_cast( &coeff_b_list21[0] ), thrust::raw_pointer_cast( &d_c_list21[0] ), thrust::raw_pointer_cast( &coeff_c_list21[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,4,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_21);
    }

    // M22 = (1.0 * a_3_2 + 1.0 * a_3_3) * (-1.0 * b_0_0 + 1.0 * b_2_0);  c_0_2 += 1.0 * M22;  c_0_3 += -1.0 * M22;  c_2_2 += 1.0 * M22;  c_2_3 += -1.0 * M22;
    {
    thrust::device_vector<value_t *> d_a_list22(2); d_a_list22[0] = d_a_3_2; d_a_list22[1] = d_a_3_3;
    thrust::device_vector<value_t *> d_b_list22(2); d_b_list22[0] = d_b_2_0; d_b_list22[1] = d_b_0_0;
    thrust::device_vector<accum_t *> d_c_list22(4); d_c_list22[0] = d_c_0_2; d_c_list22[1] = d_c_2_2; d_c_list22[2] = d_c_0_3; d_c_list22[3] = d_c_2_3;
    thrust::device_vector<value_t> coeff_a_list22(2); coeff_a_list22[0] = 1.0;
    thrust::device_vector<value_t> coeff_b_list22(2); coeff_b_list22[0] = -1.0;
    thrust::device_vector<accum_t> coeff_c_list22(4); coeff_c_list22[0] = 1.0; coeff_c_list22[1] = 1.0; coeff_c_list22[2] = -1.0; coeff_c_list22[3] = -1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_22( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list22[0] ), thrust::raw_pointer_cast( &coeff_a_list22[0] ), thrust::raw_pointer_cast( &d_b_list22[0] ), thrust::raw_pointer_cast( &coeff_b_list22[0] ), thrust::raw_pointer_cast( &d_c_list22[0] ), thrust::raw_pointer_cast( &coeff_c_list22[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,2,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_22);
    }

    // M23 = (1.0 * a_3_0) * (-1.0 * b_0_1 + 1.0 * b_0_3 + 1.0 * b_2_1 + -1.0 * b_2_3);  c_0_1 += 1.0 * M23;  c_0_3 += 1.0 * M23;  c_2_1 += 1.0 * M23;  c_2_3 += 1.0 * M23;
    {
    thrust::device_vector<value_t *> d_a_list23(1); d_a_list23[0] = d_a_3_0;
    thrust::device_vector<value_t *> d_b_list23(4); d_b_list23[0] = d_b_0_3; d_b_list23[1] = d_b_2_1; d_b_list23[2] = d_b_0_1; d_b_list23[3] = d_b_2_3;
    thrust::device_vector<accum_t *> d_c_list23(4); d_c_list23[0] = d_c_0_1; d_c_list23[1] = d_c_0_3; d_c_list23[2] = d_c_2_1; d_c_list23[3] = d_c_2_3;
    thrust::device_vector<value_t> coeff_a_list23(1);
    thrust::device_vector<value_t> coeff_b_list23(4); coeff_b_list23[0] = 1.0; coeff_b_list23[1] = -1.0; coeff_b_list23[2] = -1.0;
    thrust::device_vector<accum_t> coeff_c_list23(4); coeff_c_list23[0] = 1.0; coeff_c_list23[1] = 1.0; coeff_c_list23[2] = 1.0; coeff_c_list23[3] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_23( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list23[0] ), thrust::raw_pointer_cast( &coeff_a_list23[0] ), thrust::raw_pointer_cast( &d_b_list23[0] ), thrust::raw_pointer_cast( &coeff_b_list23[0] ), thrust::raw_pointer_cast( &d_c_list23[0] ), thrust::raw_pointer_cast( &coeff_c_list23[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1,4,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_23);
    }

    // M24 = (1.0 * a_3_3) * (1.0 * b_0_0 + -1.0 * b_0_2 + -1.0 * b_2_0 + 1.0 * b_2_2);  c_0_0 += 1.0 * M24;  c_0_2 += 1.0 * M24;  c_2_0 += 1.0 * M24;  c_2_2 += 1.0 * M24;
    {
    thrust::device_vector<value_t *> d_a_list24(1); d_a_list24[0] = d_a_3_3;
    thrust::device_vector<value_t *> d_b_list24(4); d_b_list24[0] = d_b_0_0; d_b_list24[1] = d_b_2_2; d_b_list24[2] = d_b_0_2; d_b_list24[3] = d_b_2_0;
    thrust::device_vector<accum_t *> d_c_list24(4); d_c_list24[0] = d_c_0_0; d_c_list24[1] = d_c_0_2; d_c_list24[2] = d_c_2_0; d_c_list24[3] = d_c_2_2;
    thrust::device_vector<value_t> coeff_a_list24(1);
    thrust::device_vector<value_t> coeff_b_list24(4); coeff_b_list24[0] = 1.0; coeff_b_list24[1] = -1.0; coeff_b_list24[2] = -1.0;
    thrust::device_vector<accum_t> coeff_c_list24(4); coeff_c_list24[0] = 1.0; coeff_c_list24[1] = 1.0; coeff_c_list24[2] = 1.0; coeff_c_list24[3] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_24( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list24[0] ), thrust::raw_pointer_cast( &coeff_a_list24[0] ), thrust::raw_pointer_cast( &d_b_list24[0] ), thrust::raw_pointer_cast( &coeff_b_list24[0] ), thrust::raw_pointer_cast( &d_c_list24[0] ), thrust::raw_pointer_cast( &coeff_c_list24[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1,4,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_24);
    }

    // M25 = (1.0 * a_3_0 + 1.0 * a_3_1) * (-1.0 * b_0_3 + 1.0 * b_2_3);  c_0_0 += -1.0 * M25;  c_0_1 += 1.0 * M25;  c_2_0 += -1.0 * M25;  c_2_1 += 1.0 * M25;
    {
    thrust::device_vector<value_t *> d_a_list25(2); d_a_list25[0] = d_a_3_0; d_a_list25[1] = d_a_3_1;
    thrust::device_vector<value_t *> d_b_list25(2); d_b_list25[0] = d_b_2_3; d_b_list25[1] = d_b_0_3;
    thrust::device_vector<accum_t *> d_c_list25(4); d_c_list25[0] = d_c_0_1; d_c_list25[1] = d_c_2_1; d_c_list25[2] = d_c_0_0; d_c_list25[3] = d_c_2_0;
    thrust::device_vector<value_t> coeff_a_list25(2); coeff_a_list25[0] = 1.0;
    thrust::device_vector<value_t> coeff_b_list25(2); coeff_b_list25[0] = -1.0;
    thrust::device_vector<accum_t> coeff_c_list25(4); coeff_c_list25[0] = 1.0; coeff_c_list25[1] = 1.0; coeff_c_list25[2] = -1.0; coeff_c_list25[3] = -1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_25( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list25[0] ), thrust::raw_pointer_cast( &coeff_a_list25[0] ), thrust::raw_pointer_cast( &d_b_list25[0] ), thrust::raw_pointer_cast( &coeff_b_list25[0] ), thrust::raw_pointer_cast( &d_c_list25[0] ), thrust::raw_pointer_cast( &coeff_c_list25[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,2,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_25);
    }

    // M26 = (-1.0 * a_3_0 + 1.0 * a_3_2) * (-1.0 * b_0_0 + -1.0 * b_0_1 + 1.0 * b_2_0 + 1.0 * b_2_1);  c_0_3 += 1.0 * M26;  c_2_3 += 1.0 * M26;
    {
    thrust::device_vector<value_t *> d_a_list26(2); d_a_list26[0] = d_a_3_2; d_a_list26[1] = d_a_3_0;
    thrust::device_vector<value_t *> d_b_list26(4); d_b_list26[0] = d_b_2_0; d_b_list26[1] = d_b_2_1; d_b_list26[2] = d_b_0_0; d_b_list26[3] = d_b_0_1;
    thrust::device_vector<accum_t *> d_c_list26(2); d_c_list26[0] = d_c_0_3; d_c_list26[1] = d_c_2_3;
    thrust::device_vector<value_t> coeff_a_list26(2); coeff_a_list26[0] = -1.0;
    thrust::device_vector<value_t> coeff_b_list26(4); coeff_b_list26[0] = 1.0; coeff_b_list26[1] = -1.0; coeff_b_list26[2] = -1.0;
    thrust::device_vector<accum_t> coeff_c_list26(2); coeff_c_list26[0] = 1.0; coeff_c_list26[1] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_26( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list26[0] ), thrust::raw_pointer_cast( &coeff_a_list26[0] ), thrust::raw_pointer_cast( &d_b_list26[0] ), thrust::raw_pointer_cast( &coeff_b_list26[0] ), thrust::raw_pointer_cast( &d_c_list26[0] ), thrust::raw_pointer_cast( &coeff_c_list26[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,4,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_26);
    }

    // M27 = (1.0 * a_3_1 + -1.0 * a_3_3) * (-1.0 * b_0_2 + -1.0 * b_0_3 + 1.0 * b_2_2 + 1.0 * b_2_3);  c_0_0 += 1.0 * M27;  c_2_0 += 1.0 * M27;
    {
    thrust::device_vector<value_t *> d_a_list27(2); d_a_list27[0] = d_a_3_1; d_a_list27[1] = d_a_3_3;
    thrust::device_vector<value_t *> d_b_list27(4); d_b_list27[0] = d_b_2_2; d_b_list27[1] = d_b_2_3; d_b_list27[2] = d_b_0_2; d_b_list27[3] = d_b_0_3;
    thrust::device_vector<accum_t *> d_c_list27(2); d_c_list27[0] = d_c_0_0; d_c_list27[1] = d_c_2_0;
    thrust::device_vector<value_t> coeff_a_list27(2); coeff_a_list27[0] = -1.0;
    thrust::device_vector<value_t> coeff_b_list27(4); coeff_b_list27[0] = 1.0; coeff_b_list27[1] = -1.0; coeff_b_list27[2] = -1.0;
    thrust::device_vector<accum_t> coeff_c_list27(2); coeff_c_list27[0] = 1.0; coeff_c_list27[1] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_27( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list27[0] ), thrust::raw_pointer_cast( &coeff_a_list27[0] ), thrust::raw_pointer_cast( &d_b_list27[0] ), thrust::raw_pointer_cast( &coeff_b_list27[0] ), thrust::raw_pointer_cast( &d_c_list27[0] ), thrust::raw_pointer_cast( &coeff_c_list27[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,4,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_27);
    }

    // M28 = (1.0 * a_0_0 + 1.0 * a_0_3 + 1.0 * a_1_0 + 1.0 * a_1_3) * (1.0 * b_3_0 + 1.0 * b_3_3);  c_0_0 += -1.0 * M28;  c_0_3 += -1.0 * M28;  c_1_0 += 1.0 * M28;  c_1_3 += 1.0 * M28;
    {
    thrust::device_vector<value_t *> d_a_list28(4); d_a_list28[0] = d_a_0_0; d_a_list28[1] = d_a_0_3; d_a_list28[2] = d_a_1_0; d_a_list28[3] = d_a_1_3;
    thrust::device_vector<value_t *> d_b_list28(2); d_b_list28[0] = d_b_3_0; d_b_list28[1] = d_b_3_3;
    thrust::device_vector<accum_t *> d_c_list28(4); d_c_list28[0] = d_c_1_0; d_c_list28[1] = d_c_1_3; d_c_list28[2] = d_c_0_0; d_c_list28[3] = d_c_0_3;
    thrust::device_vector<value_t> coeff_a_list28(4); coeff_a_list28[0] = 1.0; coeff_a_list28[1] = 1.0; coeff_a_list28[2] = 1.0;
    thrust::device_vector<value_t> coeff_b_list28(2); coeff_b_list28[0] = 1.0;
    thrust::device_vector<accum_t> coeff_c_list28(4); coeff_c_list28[0] = 1.0; coeff_c_list28[1] = 1.0; coeff_c_list28[2] = -1.0; coeff_c_list28[3] = -1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_28( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list28[0] ), thrust::raw_pointer_cast( &coeff_a_list28[0] ), thrust::raw_pointer_cast( &d_b_list28[0] ), thrust::raw_pointer_cast( &coeff_b_list28[0] ), thrust::raw_pointer_cast( &d_c_list28[0] ), thrust::raw_pointer_cast( &coeff_c_list28[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,2,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_28);
    }

    // M29 = (1.0 * a_0_2 + 1.0 * a_0_3 + 1.0 * a_1_2 + 1.0 * a_1_3) * (1.0 * b_3_0);  c_0_2 += -1.0 * M29;  c_0_3 += 1.0 * M29;  c_1_2 += 1.0 * M29;  c_1_3 += -1.0 * M29;
    {
    thrust::device_vector<value_t *> d_a_list29(4); d_a_list29[0] = d_a_0_2; d_a_list29[1] = d_a_0_3; d_a_list29[2] = d_a_1_2; d_a_list29[3] = d_a_1_3;
    thrust::device_vector<value_t *> d_b_list29(1); d_b_list29[0] = d_b_3_0;
    thrust::device_vector<accum_t *> d_c_list29(4); d_c_list29[0] = d_c_0_3; d_c_list29[1] = d_c_1_2; d_c_list29[2] = d_c_0_2; d_c_list29[3] = d_c_1_3;
    thrust::device_vector<value_t> coeff_a_list29(4); coeff_a_list29[0] = 1.0; coeff_a_list29[1] = 1.0; coeff_a_list29[2] = 1.0;
    thrust::device_vector<value_t> coeff_b_list29(1);
    thrust::device_vector<accum_t> coeff_c_list29(4); coeff_c_list29[0] = 1.0; coeff_c_list29[1] = 1.0; coeff_c_list29[2] = -1.0; coeff_c_list29[3] = -1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_29( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list29[0] ), thrust::raw_pointer_cast( &coeff_a_list29[0] ), thrust::raw_pointer_cast( &d_b_list29[0] ), thrust::raw_pointer_cast( &coeff_b_list29[0] ), thrust::raw_pointer_cast( &d_c_list29[0] ), thrust::raw_pointer_cast( &coeff_c_list29[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,1,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_29);
    }

    // M30 = (1.0 * a_0_0 + 1.0 * a_1_0) * (1.0 * b_3_1 + -1.0 * b_3_3);  c_0_1 += -1.0 * M30;  c_0_3 += -1.0 * M30;  c_1_1 += 1.0 * M30;  c_1_3 += 1.0 * M30;
    {
    thrust::device_vector<value_t *> d_a_list30(2); d_a_list30[0] = d_a_0_0; d_a_list30[1] = d_a_1_0;
    thrust::device_vector<value_t *> d_b_list30(2); d_b_list30[0] = d_b_3_1; d_b_list30[1] = d_b_3_3;
    thrust::device_vector<accum_t *> d_c_list30(4); d_c_list30[0] = d_c_1_1; d_c_list30[1] = d_c_1_3; d_c_list30[2] = d_c_0_1; d_c_list30[3] = d_c_0_3;
    thrust::device_vector<value_t> coeff_a_list30(2); coeff_a_list30[0] = 1.0;
    thrust::device_vector<value_t> coeff_b_list30(2); coeff_b_list30[0] = -1.0;
    thrust::device_vector<accum_t> coeff_c_list30(4); coeff_c_list30[0] = 1.0; coeff_c_list30[1] = 1.0; coeff_c_list30[2] = -1.0; coeff_c_list30[3] = -1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_30( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list30[0] ), thrust::raw_pointer_cast( &coeff_a_list30[0] ), thrust::raw_pointer_cast( &d_b_list30[0] ), thrust::raw_pointer_cast( &coeff_b_list30[0] ), thrust::raw_pointer_cast( &d_c_list30[0] ), thrust::raw_pointer_cast( &coeff_c_list30[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,2,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_30);
    }

    // M31 = (1.0 * a_0_3 + 1.0 * a_1_3) * (-1.0 * b_3_0 + 1.0 * b_3_2);  c_0_0 += -1.0 * M31;  c_0_2 += -1.0 * M31;  c_1_0 += 1.0 * M31;  c_1_2 += 1.0 * M31;
    {
    thrust::device_vector<value_t *> d_a_list31(2); d_a_list31[0] = d_a_0_3; d_a_list31[1] = d_a_1_3;
    thrust::device_vector<value_t *> d_b_list31(2); d_b_list31[0] = d_b_3_2; d_b_list31[1] = d_b_3_0;
    thrust::device_vector<accum_t *> d_c_list31(4); d_c_list31[0] = d_c_1_0; d_c_list31[1] = d_c_1_2; d_c_list31[2] = d_c_0_0; d_c_list31[3] = d_c_0_2;
    thrust::device_vector<value_t> coeff_a_list31(2); coeff_a_list31[0] = 1.0;
    thrust::device_vector<value_t> coeff_b_list31(2); coeff_b_list31[0] = -1.0;
    thrust::device_vector<accum_t> coeff_c_list31(4); coeff_c_list31[0] = 1.0; coeff_c_list31[1] = 1.0; coeff_c_list31[2] = -1.0; coeff_c_list31[3] = -1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_31( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list31[0] ), thrust::raw_pointer_cast( &coeff_a_list31[0] ), thrust::raw_pointer_cast( &d_b_list31[0] ), thrust::raw_pointer_cast( &coeff_b_list31[0] ), thrust::raw_pointer_cast( &d_c_list31[0] ), thrust::raw_pointer_cast( &coeff_c_list31[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,2,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_31);
    }

    // M32 = (1.0 * a_0_0 + 1.0 * a_0_1 + 1.0 * a_1_0 + 1.0 * a_1_1) * (1.0 * b_3_3);  c_0_0 += 1.0 * M32;  c_0_1 += -1.0 * M32;  c_1_0 += -1.0 * M32;  c_1_1 += 1.0 * M32;
    {
    thrust::device_vector<value_t *> d_a_list32(4); d_a_list32[0] = d_a_0_0; d_a_list32[1] = d_a_0_1; d_a_list32[2] = d_a_1_0; d_a_list32[3] = d_a_1_1;
    thrust::device_vector<value_t *> d_b_list32(1); d_b_list32[0] = d_b_3_3;
    thrust::device_vector<accum_t *> d_c_list32(4); d_c_list32[0] = d_c_0_0; d_c_list32[1] = d_c_1_1; d_c_list32[2] = d_c_0_1; d_c_list32[3] = d_c_1_0;
    thrust::device_vector<value_t> coeff_a_list32(4); coeff_a_list32[0] = 1.0; coeff_a_list32[1] = 1.0; coeff_a_list32[2] = 1.0;
    thrust::device_vector<value_t> coeff_b_list32(1);
    thrust::device_vector<accum_t> coeff_c_list32(4); coeff_c_list32[0] = 1.0; coeff_c_list32[1] = 1.0; coeff_c_list32[2] = -1.0; coeff_c_list32[3] = -1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_32( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list32[0] ), thrust::raw_pointer_cast( &coeff_a_list32[0] ), thrust::raw_pointer_cast( &d_b_list32[0] ), thrust::raw_pointer_cast( &coeff_b_list32[0] ), thrust::raw_pointer_cast( &d_c_list32[0] ), thrust::raw_pointer_cast( &coeff_c_list32[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,1,4><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_32);
    }

    // M33 = (-1.0 * a_0_0 + 1.0 * a_0_2 + -1.0 * a_1_0 + 1.0 * a_1_2) * (1.0 * b_3_0 + 1.0 * b_3_1);  c_0_3 += -1.0 * M33;  c_1_3 += 1.0 * M33;
    {
    thrust::device_vector<value_t *> d_a_list33(4); d_a_list33[0] = d_a_0_2; d_a_list33[1] = d_a_1_2; d_a_list33[2] = d_a_0_0; d_a_list33[3] = d_a_1_0;
    thrust::device_vector<value_t *> d_b_list33(2); d_b_list33[0] = d_b_3_0; d_b_list33[1] = d_b_3_1;
    thrust::device_vector<accum_t *> d_c_list33(2); d_c_list33[0] = d_c_1_3; d_c_list33[1] = d_c_0_3;
    thrust::device_vector<value_t> coeff_a_list33(4); coeff_a_list33[0] = 1.0; coeff_a_list33[1] = -1.0; coeff_a_list33[2] = -1.0;
    thrust::device_vector<value_t> coeff_b_list33(2); coeff_b_list33[0] = 1.0;
    thrust::device_vector<accum_t> coeff_c_list33(2); coeff_c_list33[0] = 1.0; coeff_c_list33[1] = -1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_33( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list33[0] ), thrust::raw_pointer_cast( &coeff_a_list33[0] ), thrust::raw_pointer_cast( &d_b_list33[0] ), thrust::raw_pointer_cast( &coeff_b_list33[0] ), thrust::raw_pointer_cast( &d_c_list33[0] ), thrust::raw_pointer_cast( &coeff_c_list33[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,2,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_33);
    }

    // M34 = (1.0 * a_0_1 + -1.0 * a_0_3 + 1.0 * a_1_1 + -1.0 * a_1_3) * (1.0 * b_3_2 + 1.0 * b_3_3);  c_0_0 += -1.0 * M34;  c_1_0 += 1.0 * M34;
    {
    thrust::device_vector<value_t *> d_a_list34(4); d_a_list34[0] = d_a_0_1; d_a_list34[1] = d_a_1_1; d_a_list34[2] = d_a_0_3; d_a_list34[3] = d_a_1_3;
    thrust::device_vector<value_t *> d_b_list34(2); d_b_list34[0] = d_b_3_2; d_b_list34[1] = d_b_3_3;
    thrust::device_vector<accum_t *> d_c_list34(2); d_c_list34[0] = d_c_1_0; d_c_list34[1] = d_c_0_0;
    thrust::device_vector<value_t> coeff_a_list34(4); coeff_a_list34[0] = 1.0; coeff_a_list34[1] = -1.0; coeff_a_list34[2] = -1.0;
    thrust::device_vector<value_t> coeff_b_list34(2); coeff_b_list34[0] = 1.0;
    thrust::device_vector<accum_t> coeff_c_list34(2); coeff_c_list34[0] = 1.0; coeff_c_list34[1] = -1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_34( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list34[0] ), thrust::raw_pointer_cast( &coeff_a_list34[0] ), thrust::raw_pointer_cast( &d_b_list34[0] ), thrust::raw_pointer_cast( &coeff_b_list34[0] ), thrust::raw_pointer_cast( &d_c_list34[0] ), thrust::raw_pointer_cast( &coeff_c_list34[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,2,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_34);
    }

    // M35 = (-1.0 * a_0_0 + -1.0 * a_0_3 + 1.0 * a_2_0 + 1.0 * a_2_3) * (1.0 * b_0_0 + 1.0 * b_0_3 + 1.0 * b_1_0 + 1.0 * b_1_3);  c_3_0 += 1.0 * M35;  c_3_3 += 1.0 * M35;
    {
    thrust::device_vector<value_t *> d_a_list35(4); d_a_list35[0] = d_a_2_0; d_a_list35[1] = d_a_2_3; d_a_list35[2] = d_a_0_0; d_a_list35[3] = d_a_0_3;
    thrust::device_vector<value_t *> d_b_list35(4); d_b_list35[0] = d_b_0_0; d_b_list35[1] = d_b_0_3; d_b_list35[2] = d_b_1_0; d_b_list35[3] = d_b_1_3;
    thrust::device_vector<accum_t *> d_c_list35(2); d_c_list35[0] = d_c_3_0; d_c_list35[1] = d_c_3_3;
    thrust::device_vector<value_t> coeff_a_list35(4); coeff_a_list35[0] = 1.0; coeff_a_list35[1] = -1.0; coeff_a_list35[2] = -1.0;
    thrust::device_vector<value_t> coeff_b_list35(4); coeff_b_list35[0] = 1.0; coeff_b_list35[1] = 1.0; coeff_b_list35[2] = 1.0;
    thrust::device_vector<accum_t> coeff_c_list35(2); coeff_c_list35[0] = 1.0; coeff_c_list35[1] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_35( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list35[0] ), thrust::raw_pointer_cast( &coeff_a_list35[0] ), thrust::raw_pointer_cast( &d_b_list35[0] ), thrust::raw_pointer_cast( &coeff_b_list35[0] ), thrust::raw_pointer_cast( &d_c_list35[0] ), thrust::raw_pointer_cast( &coeff_c_list35[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,4,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_35);
    }

    // M36 = (-1.0 * a_0_2 + -1.0 * a_0_3 + 1.0 * a_2_2 + 1.0 * a_2_3) * (1.0 * b_0_0 + 1.0 * b_1_0);  c_3_2 += 1.0 * M36;  c_3_3 += -1.0 * M36;
    {
    thrust::device_vector<value_t *> d_a_list36(4); d_a_list36[0] = d_a_2_2; d_a_list36[1] = d_a_2_3; d_a_list36[2] = d_a_0_2; d_a_list36[3] = d_a_0_3;
    thrust::device_vector<value_t *> d_b_list36(2); d_b_list36[0] = d_b_0_0; d_b_list36[1] = d_b_1_0;
    thrust::device_vector<accum_t *> d_c_list36(2); d_c_list36[0] = d_c_3_2; d_c_list36[1] = d_c_3_3;
    thrust::device_vector<value_t> coeff_a_list36(4); coeff_a_list36[0] = 1.0; coeff_a_list36[1] = -1.0; coeff_a_list36[2] = -1.0;
    thrust::device_vector<value_t> coeff_b_list36(2); coeff_b_list36[0] = 1.0;
    thrust::device_vector<accum_t> coeff_c_list36(2); coeff_c_list36[0] = 1.0; coeff_c_list36[1] = -1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_36( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list36[0] ), thrust::raw_pointer_cast( &coeff_a_list36[0] ), thrust::raw_pointer_cast( &d_b_list36[0] ), thrust::raw_pointer_cast( &coeff_b_list36[0] ), thrust::raw_pointer_cast( &d_c_list36[0] ), thrust::raw_pointer_cast( &coeff_c_list36[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,2,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_36);
    }

    // M37 = (-1.0 * a_0_0 + 1.0 * a_2_0) * (1.0 * b_0_1 + -1.0 * b_0_3 + 1.0 * b_1_1 + -1.0 * b_1_3);  c_3_1 += 1.0 * M37;  c_3_3 += 1.0 * M37;
    {
    thrust::device_vector<value_t *> d_a_list37(2); d_a_list37[0] = d_a_2_0; d_a_list37[1] = d_a_0_0;
    thrust::device_vector<value_t *> d_b_list37(4); d_b_list37[0] = d_b_0_1; d_b_list37[1] = d_b_1_1; d_b_list37[2] = d_b_0_3; d_b_list37[3] = d_b_1_3;
    thrust::device_vector<accum_t *> d_c_list37(2); d_c_list37[0] = d_c_3_1; d_c_list37[1] = d_c_3_3;
    thrust::device_vector<value_t> coeff_a_list37(2); coeff_a_list37[0] = -1.0;
    thrust::device_vector<value_t> coeff_b_list37(4); coeff_b_list37[0] = 1.0; coeff_b_list37[1] = -1.0; coeff_b_list37[2] = -1.0;
    thrust::device_vector<accum_t> coeff_c_list37(2); coeff_c_list37[0] = 1.0; coeff_c_list37[1] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_37( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list37[0] ), thrust::raw_pointer_cast( &coeff_a_list37[0] ), thrust::raw_pointer_cast( &d_b_list37[0] ), thrust::raw_pointer_cast( &coeff_b_list37[0] ), thrust::raw_pointer_cast( &d_c_list37[0] ), thrust::raw_pointer_cast( &coeff_c_list37[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,4,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_37);
    }

    // M38 = (-1.0 * a_0_3 + 1.0 * a_2_3) * (-1.0 * b_0_0 + 1.0 * b_0_2 + -1.0 * b_1_0 + 1.0 * b_1_2);  c_3_0 += 1.0 * M38;  c_3_2 += 1.0 * M38;
    {
    thrust::device_vector<value_t *> d_a_list38(2); d_a_list38[0] = d_a_2_3; d_a_list38[1] = d_a_0_3;
    thrust::device_vector<value_t *> d_b_list38(4); d_b_list38[0] = d_b_0_2; d_b_list38[1] = d_b_1_2; d_b_list38[2] = d_b_0_0; d_b_list38[3] = d_b_1_0;
    thrust::device_vector<accum_t *> d_c_list38(2); d_c_list38[0] = d_c_3_0; d_c_list38[1] = d_c_3_2;
    thrust::device_vector<value_t> coeff_a_list38(2); coeff_a_list38[0] = -1.0;
    thrust::device_vector<value_t> coeff_b_list38(4); coeff_b_list38[0] = 1.0; coeff_b_list38[1] = -1.0; coeff_b_list38[2] = -1.0;
    thrust::device_vector<accum_t> coeff_c_list38(2); coeff_c_list38[0] = 1.0; coeff_c_list38[1] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_38( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list38[0] ), thrust::raw_pointer_cast( &coeff_a_list38[0] ), thrust::raw_pointer_cast( &d_b_list38[0] ), thrust::raw_pointer_cast( &coeff_b_list38[0] ), thrust::raw_pointer_cast( &d_c_list38[0] ), thrust::raw_pointer_cast( &coeff_c_list38[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,4,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_38);
    }

    // M39 = (-1.0 * a_0_0 + -1.0 * a_0_1 + 1.0 * a_2_0 + 1.0 * a_2_1) * (1.0 * b_0_3 + 1.0 * b_1_3);  c_3_0 += -1.0 * M39;  c_3_1 += 1.0 * M39;
    {
    thrust::device_vector<value_t *> d_a_list39(4); d_a_list39[0] = d_a_2_0; d_a_list39[1] = d_a_2_1; d_a_list39[2] = d_a_0_0; d_a_list39[3] = d_a_0_1;
    thrust::device_vector<value_t *> d_b_list39(2); d_b_list39[0] = d_b_0_3; d_b_list39[1] = d_b_1_3;
    thrust::device_vector<accum_t *> d_c_list39(2); d_c_list39[0] = d_c_3_1; d_c_list39[1] = d_c_3_0;
    thrust::device_vector<value_t> coeff_a_list39(4); coeff_a_list39[0] = 1.0; coeff_a_list39[1] = -1.0; coeff_a_list39[2] = -1.0;
    thrust::device_vector<value_t> coeff_b_list39(2); coeff_b_list39[0] = 1.0;
    thrust::device_vector<accum_t> coeff_c_list39(2); coeff_c_list39[0] = 1.0; coeff_c_list39[1] = -1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_39( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list39[0] ), thrust::raw_pointer_cast( &coeff_a_list39[0] ), thrust::raw_pointer_cast( &d_b_list39[0] ), thrust::raw_pointer_cast( &coeff_b_list39[0] ), thrust::raw_pointer_cast( &d_c_list39[0] ), thrust::raw_pointer_cast( &coeff_c_list39[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,2,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_39);
    }

    // M40 = (1.0 * a_0_0 + -1.0 * a_0_2 + -1.0 * a_2_0 + 1.0 * a_2_2) * (1.0 * b_0_0 + 1.0 * b_0_1 + 1.0 * b_1_0 + 1.0 * b_1_1);  c_3_3 += 1.0 * M40;
    {
    thrust::device_vector<value_t *> d_a_list40(4); d_a_list40[0] = d_a_0_0; d_a_list40[1] = d_a_2_2; d_a_list40[2] = d_a_0_2; d_a_list40[3] = d_a_2_0;
    thrust::device_vector<value_t *> d_b_list40(4); d_b_list40[0] = d_b_0_0; d_b_list40[1] = d_b_0_1; d_b_list40[2] = d_b_1_0; d_b_list40[3] = d_b_1_1;
    thrust::device_vector<accum_t *> d_c_list40(1); d_c_list40[0] = d_c_3_3;
    thrust::device_vector<value_t> coeff_a_list40(4); coeff_a_list40[0] = 1.0; coeff_a_list40[1] = -1.0; coeff_a_list40[2] = -1.0;
    thrust::device_vector<value_t> coeff_b_list40(4); coeff_b_list40[0] = 1.0; coeff_b_list40[1] = 1.0; coeff_b_list40[2] = 1.0;
    thrust::device_vector<accum_t> coeff_c_list40(1); coeff_c_list40[0] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_40( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list40[0] ), thrust::raw_pointer_cast( &coeff_a_list40[0] ), thrust::raw_pointer_cast( &d_b_list40[0] ), thrust::raw_pointer_cast( &coeff_b_list40[0] ), thrust::raw_pointer_cast( &d_c_list40[0] ), thrust::raw_pointer_cast( &coeff_c_list40[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,4,1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_40);
    }

    // M41 = (-1.0 * a_0_1 + 1.0 * a_0_3 + 1.0 * a_2_1 + -1.0 * a_2_3) * (1.0 * b_0_2 + 1.0 * b_0_3 + 1.0 * b_1_2 + 1.0 * b_1_3);  c_3_0 += 1.0 * M41;
    {
    thrust::device_vector<value_t *> d_a_list41(4); d_a_list41[0] = d_a_0_3; d_a_list41[1] = d_a_2_1; d_a_list41[2] = d_a_0_1; d_a_list41[3] = d_a_2_3;
    thrust::device_vector<value_t *> d_b_list41(4); d_b_list41[0] = d_b_0_2; d_b_list41[1] = d_b_0_3; d_b_list41[2] = d_b_1_2; d_b_list41[3] = d_b_1_3;
    thrust::device_vector<accum_t *> d_c_list41(1); d_c_list41[0] = d_c_3_0;
    thrust::device_vector<value_t> coeff_a_list41(4); coeff_a_list41[0] = 1.0; coeff_a_list41[1] = -1.0; coeff_a_list41[2] = -1.0;
    thrust::device_vector<value_t> coeff_b_list41(4); coeff_b_list41[0] = 1.0; coeff_b_list41[1] = 1.0; coeff_b_list41[2] = 1.0;
    thrust::device_vector<accum_t> coeff_c_list41(1); coeff_c_list41[0] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_41( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list41[0] ), thrust::raw_pointer_cast( &coeff_a_list41[0] ), thrust::raw_pointer_cast( &d_b_list41[0] ), thrust::raw_pointer_cast( &coeff_b_list41[0] ), thrust::raw_pointer_cast( &d_c_list41[0] ), thrust::raw_pointer_cast( &coeff_c_list41[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,4,1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_41);
    }

    // M42 = (1.0 * a_1_0 + 1.0 * a_1_3 + -1.0 * a_3_0 + -1.0 * a_3_3) * (1.0 * b_2_0 + 1.0 * b_2_3 + 1.0 * b_3_0 + 1.0 * b_3_3);  c_0_0 += 1.0 * M42;  c_0_3 += 1.0 * M42;
    {
    thrust::device_vector<value_t *> d_a_list42(4); d_a_list42[0] = d_a_1_0; d_a_list42[1] = d_a_1_3; d_a_list42[2] = d_a_3_0; d_a_list42[3] = d_a_3_3;
    thrust::device_vector<value_t *> d_b_list42(4); d_b_list42[0] = d_b_2_0; d_b_list42[1] = d_b_2_3; d_b_list42[2] = d_b_3_0; d_b_list42[3] = d_b_3_3;
    thrust::device_vector<accum_t *> d_c_list42(2); d_c_list42[0] = d_c_0_0; d_c_list42[1] = d_c_0_3;
    thrust::device_vector<value_t> coeff_a_list42(4); coeff_a_list42[0] = 1.0; coeff_a_list42[1] = -1.0; coeff_a_list42[2] = -1.0;
    thrust::device_vector<value_t> coeff_b_list42(4); coeff_b_list42[0] = 1.0; coeff_b_list42[1] = 1.0; coeff_b_list42[2] = 1.0;
    thrust::device_vector<accum_t> coeff_c_list42(2); coeff_c_list42[0] = 1.0; coeff_c_list42[1] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_42( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list42[0] ), thrust::raw_pointer_cast( &coeff_a_list42[0] ), thrust::raw_pointer_cast( &d_b_list42[0] ), thrust::raw_pointer_cast( &coeff_b_list42[0] ), thrust::raw_pointer_cast( &d_c_list42[0] ), thrust::raw_pointer_cast( &coeff_c_list42[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,4,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_42);
    }

    // M43 = (1.0 * a_1_2 + 1.0 * a_1_3 + -1.0 * a_3_2 + -1.0 * a_3_3) * (1.0 * b_2_0 + 1.0 * b_3_0);  c_0_2 += 1.0 * M43;  c_0_3 += -1.0 * M43;
    {
    thrust::device_vector<value_t *> d_a_list43(4); d_a_list43[0] = d_a_1_2; d_a_list43[1] = d_a_1_3; d_a_list43[2] = d_a_3_2; d_a_list43[3] = d_a_3_3;
    thrust::device_vector<value_t *> d_b_list43(2); d_b_list43[0] = d_b_2_0; d_b_list43[1] = d_b_3_0;
    thrust::device_vector<accum_t *> d_c_list43(2); d_c_list43[0] = d_c_0_2; d_c_list43[1] = d_c_0_3;
    thrust::device_vector<value_t> coeff_a_list43(4); coeff_a_list43[0] = 1.0; coeff_a_list43[1] = -1.0; coeff_a_list43[2] = -1.0;
    thrust::device_vector<value_t> coeff_b_list43(2); coeff_b_list43[0] = 1.0;
    thrust::device_vector<accum_t> coeff_c_list43(2); coeff_c_list43[0] = 1.0; coeff_c_list43[1] = -1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_43( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list43[0] ), thrust::raw_pointer_cast( &coeff_a_list43[0] ), thrust::raw_pointer_cast( &d_b_list43[0] ), thrust::raw_pointer_cast( &coeff_b_list43[0] ), thrust::raw_pointer_cast( &d_c_list43[0] ), thrust::raw_pointer_cast( &coeff_c_list43[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,2,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_43);
    }

    // M44 = (1.0 * a_1_0 + -1.0 * a_3_0) * (1.0 * b_2_1 + -1.0 * b_2_3 + 1.0 * b_3_1 + -1.0 * b_3_3);  c_0_1 += 1.0 * M44;  c_0_3 += 1.0 * M44;
    {
    thrust::device_vector<value_t *> d_a_list44(2); d_a_list44[0] = d_a_1_0; d_a_list44[1] = d_a_3_0;
    thrust::device_vector<value_t *> d_b_list44(4); d_b_list44[0] = d_b_2_1; d_b_list44[1] = d_b_3_1; d_b_list44[2] = d_b_2_3; d_b_list44[3] = d_b_3_3;
    thrust::device_vector<accum_t *> d_c_list44(2); d_c_list44[0] = d_c_0_1; d_c_list44[1] = d_c_0_3;
    thrust::device_vector<value_t> coeff_a_list44(2); coeff_a_list44[0] = -1.0;
    thrust::device_vector<value_t> coeff_b_list44(4); coeff_b_list44[0] = 1.0; coeff_b_list44[1] = -1.0; coeff_b_list44[2] = -1.0;
    thrust::device_vector<accum_t> coeff_c_list44(2); coeff_c_list44[0] = 1.0; coeff_c_list44[1] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_44( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list44[0] ), thrust::raw_pointer_cast( &coeff_a_list44[0] ), thrust::raw_pointer_cast( &d_b_list44[0] ), thrust::raw_pointer_cast( &coeff_b_list44[0] ), thrust::raw_pointer_cast( &d_c_list44[0] ), thrust::raw_pointer_cast( &coeff_c_list44[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,4,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_44);
    }

    // M45 = (1.0 * a_1_3 + -1.0 * a_3_3) * (-1.0 * b_2_0 + 1.0 * b_2_2 + -1.0 * b_3_0 + 1.0 * b_3_2);  c_0_0 += 1.0 * M45;  c_0_2 += 1.0 * M45;
    {
    thrust::device_vector<value_t *> d_a_list45(2); d_a_list45[0] = d_a_1_3; d_a_list45[1] = d_a_3_3;
    thrust::device_vector<value_t *> d_b_list45(4); d_b_list45[0] = d_b_2_2; d_b_list45[1] = d_b_3_2; d_b_list45[2] = d_b_2_0; d_b_list45[3] = d_b_3_0;
    thrust::device_vector<accum_t *> d_c_list45(2); d_c_list45[0] = d_c_0_0; d_c_list45[1] = d_c_0_2;
    thrust::device_vector<value_t> coeff_a_list45(2); coeff_a_list45[0] = -1.0;
    thrust::device_vector<value_t> coeff_b_list45(4); coeff_b_list45[0] = 1.0; coeff_b_list45[1] = -1.0; coeff_b_list45[2] = -1.0;
    thrust::device_vector<accum_t> coeff_c_list45(2); coeff_c_list45[0] = 1.0; coeff_c_list45[1] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_45( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list45[0] ), thrust::raw_pointer_cast( &coeff_a_list45[0] ), thrust::raw_pointer_cast( &d_b_list45[0] ), thrust::raw_pointer_cast( &coeff_b_list45[0] ), thrust::raw_pointer_cast( &d_c_list45[0] ), thrust::raw_pointer_cast( &coeff_c_list45[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 2,4,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_45);
    }

    // M46 = (1.0 * a_1_0 + 1.0 * a_1_1 + -1.0 * a_3_0 + -1.0 * a_3_1) * (1.0 * b_2_3 + 1.0 * b_3_3);  c_0_0 += -1.0 * M46;  c_0_1 += 1.0 * M46;
    {
    thrust::device_vector<value_t *> d_a_list46(4); d_a_list46[0] = d_a_1_0; d_a_list46[1] = d_a_1_1; d_a_list46[2] = d_a_3_0; d_a_list46[3] = d_a_3_1;
    thrust::device_vector<value_t *> d_b_list46(2); d_b_list46[0] = d_b_2_3; d_b_list46[1] = d_b_3_3;
    thrust::device_vector<accum_t *> d_c_list46(2); d_c_list46[0] = d_c_0_1; d_c_list46[1] = d_c_0_0;
    thrust::device_vector<value_t> coeff_a_list46(4); coeff_a_list46[0] = 1.0; coeff_a_list46[1] = -1.0; coeff_a_list46[2] = -1.0;
    thrust::device_vector<value_t> coeff_b_list46(2); coeff_b_list46[0] = 1.0;
    thrust::device_vector<accum_t> coeff_c_list46(2); coeff_c_list46[0] = 1.0; coeff_c_list46[1] = -1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_46( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list46[0] ), thrust::raw_pointer_cast( &coeff_a_list46[0] ), thrust::raw_pointer_cast( &d_b_list46[0] ), thrust::raw_pointer_cast( &coeff_b_list46[0] ), thrust::raw_pointer_cast( &d_c_list46[0] ), thrust::raw_pointer_cast( &coeff_c_list46[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,2,2><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_46);
    }

    // M47 = (-1.0 * a_1_0 + 1.0 * a_1_2 + 1.0 * a_3_0 + -1.0 * a_3_2) * (1.0 * b_2_0 + 1.0 * b_2_1 + 1.0 * b_3_0 + 1.0 * b_3_1);  c_0_3 += 1.0 * M47;
    {
    thrust::device_vector<value_t *> d_a_list47(4); d_a_list47[0] = d_a_1_2; d_a_list47[1] = d_a_3_0; d_a_list47[2] = d_a_1_0; d_a_list47[3] = d_a_3_2;
    thrust::device_vector<value_t *> d_b_list47(4); d_b_list47[0] = d_b_2_0; d_b_list47[1] = d_b_2_1; d_b_list47[2] = d_b_3_0; d_b_list47[3] = d_b_3_1;
    thrust::device_vector<accum_t *> d_c_list47(1); d_c_list47[0] = d_c_0_3;
    thrust::device_vector<value_t> coeff_a_list47(4); coeff_a_list47[0] = 1.0; coeff_a_list47[1] = -1.0; coeff_a_list47[2] = -1.0;
    thrust::device_vector<value_t> coeff_b_list47(4); coeff_b_list47[0] = 1.0; coeff_b_list47[1] = 1.0; coeff_b_list47[2] = 1.0;
    thrust::device_vector<accum_t> coeff_c_list47(1); coeff_c_list47[0] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_47( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list47[0] ), thrust::raw_pointer_cast( &coeff_a_list47[0] ), thrust::raw_pointer_cast( &d_b_list47[0] ), thrust::raw_pointer_cast( &coeff_b_list47[0] ), thrust::raw_pointer_cast( &d_c_list47[0] ), thrust::raw_pointer_cast( &coeff_c_list47[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,4,1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_47);
    }

    // M48 = (1.0 * a_1_1 + -1.0 * a_1_3 + -1.0 * a_3_1 + 1.0 * a_3_3) * (1.0 * b_2_2 + 1.0 * b_2_3 + 1.0 * b_3_2 + 1.0 * b_3_3);  c_0_0 += 1.0 * M48;
    {
    thrust::device_vector<value_t *> d_a_list48(4); d_a_list48[0] = d_a_1_1; d_a_list48[1] = d_a_3_3; d_a_list48[2] = d_a_1_3; d_a_list48[3] = d_a_3_1;
    thrust::device_vector<value_t *> d_b_list48(4); d_b_list48[0] = d_b_2_2; d_b_list48[1] = d_b_2_3; d_b_list48[2] = d_b_3_2; d_b_list48[3] = d_b_3_3;
    thrust::device_vector<accum_t *> d_c_list48(1); d_c_list48[0] = d_c_0_0;
    thrust::device_vector<value_t> coeff_a_list48(4); coeff_a_list48[0] = 1.0; coeff_a_list48[1] = -1.0; coeff_a_list48[2] = -1.0;
    thrust::device_vector<value_t> coeff_b_list48(4); coeff_b_list48[0] = 1.0; coeff_b_list48[1] = 1.0; coeff_b_list48[2] = 1.0;
    thrust::device_vector<accum_t> coeff_c_list48(1); coeff_c_list48[0] = 1.0;
    stra2_param_pack<value_t, accum_t, epilogue_op_t> stra2_pack_48( ms, ns, ks, lda, ldb, ldc, k_split0, epilogue_op, thrust::raw_pointer_cast( &d_a_list48[0] ), thrust::raw_pointer_cast( &coeff_a_list48[0] ), thrust::raw_pointer_cast( &d_b_list48[0] ), thrust::raw_pointer_cast( &coeff_b_list48[0] ), thrust::raw_pointer_cast( &d_c_list48[0] ), thrust::raw_pointer_cast( &coeff_c_list48[0] ) );
    stra2_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 4,4,1><<< config.grid, config.block, dynamic_smem_bytes, stream >>>(stra2_pack_48);
    }

