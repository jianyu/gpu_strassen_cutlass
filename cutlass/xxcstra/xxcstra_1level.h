

    int lda, ldb, ldc;
    gen_leading_dim<TransformA>( m, k, lda );
    gen_leading_dim<TransformB>( k, n, ldb );
    gen_leading_dim<matrix_transform_t::NonTranspose>( m, n, ldc );
    int ms, ks, ns;
    int md, kd, nd;
    int mr, kr, nr;
    mr = m % ( 2 ), kr = k % ( 2 ), nr = n % ( 2 );
    md = m - mr, kd = k - kr, nd = n - nr;
    ms=md, ks=kd, ns=nd;
    value_t *d_a_0, *d_a_1, *d_a_2, *d_a_3;
    xxcstra_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 0, d_a, &d_a_0 );
    xxcstra_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 0, 1, d_a, &d_a_1 );
    xxcstra_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 0, d_a, &d_a_2 );
    xxcstra_acquire_mpart<value_t,TransformA>( ms, ks, lda, 2, 2, 1, 1, d_a, &d_a_3 );
    ms=md, ks=kd, ns=nd;
    value_t *d_b_0, *d_b_1, *d_b_2, *d_b_3;
    xxcstra_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 0, d_b, &d_b_0 );
    xxcstra_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 0, 1, d_b, &d_b_1 );
    xxcstra_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 0, d_b, &d_b_2 );
    xxcstra_acquire_mpart<value_t,TransformB>( ks, ns, ldb, 2, 2, 1, 1, d_b, &d_b_3 );
    ms=md, ks=kd, ns=nd;
    accum_t *d_c_0, *d_c_1, *d_c_2, *d_c_3;
    xxcstra_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 0, d_c, &d_c_0 );
    xxcstra_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 0, 1, d_c, &d_c_1 );
    xxcstra_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 0, d_c, &d_c_2 );
    xxcstra_acquire_mpart<accum_t,matrix_transform_t::NonTranspose>( ms, ns, ldc, 2, 2, 1, 1, d_c, &d_c_3 );
    ms=ms/2, ks=ks/2, ns=ns/2;

    epilogue_op_t epilogue_op2( 1.0, 0.0 );
    epilogue_op_t epilogue_op3( 1.0, 1.0 );
    float *d_a_t, *d_b_t, *d_m_t;
    float one = 1.0, negone = -1.0, zero = 0.0;
    int lda_t = ms, ldb_t = ks, ldm_t = ms;
    cudaMalloc((void **)&d_a_t, ms * ks * sizeof(float));
    cudaMalloc((void **)&d_b_t, ks * ns * sizeof(float));
    cudaMalloc((void **)&d_m_t, ms * ns * sizeof(float));

    cudaStream_t stra_stream[ 2 ];
    cudaStreamCreate( &stra_stream[ 0 ] );
    cudaStreamCreate( &stra_stream[ 1 ] );

    //printf( "flag1\n" );
    // M6 = (1 * d_a_1 + -1 * d_a_3) * (1 * d_b_2 + 1 * d_b_3);  d_c_0 += 1 * M6;
    // A_t = A1 - A3
    cublasSgeam( g_cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N, ms, ks, &one, d_a_1, lda, &negone, d_a_3, lda, d_a_t, lda_t );
    // B_t = B2 + B3
    cublasSgeam( g_cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N, ks, ns, &one, d_b_2, ldb, &one, d_b_3, ldb, d_b_t, ldb_t );
    // M_t = A_t * B_t
    // C0 += M_t
    xxcstra_param_pack<value_t, accum_t, epilogue_op_t> xxcstra_pack_6( ms, ns, ks, lda_t, ldb_t, ldc, k_split0, epilogue_op3, d_a_t, d_b_t, d_c_0, NULL );
    xxcstra_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 0><<< config.grid, config.block, dynamic_smem_bytes, stra_stream[ 0 ] >>>(xxcstra_pack_6);


    // M3 = (1 * d_a_3) * (-1 * d_b_0 + 1 * d_b_2);  d_c_0 += 1 * M3;  d_c_2 += 1 * M3;
    // A_t = A3
    //cublasSgeam( g_cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N, ms, ks, &one, d_a_3, lda, &zero, d_a_3, lda, d_a_t, lda_t );
    // B_t = B2 - B0
    cublasSgeam( g_cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N, ks, ns, &one, d_b_2, ldb, &negone, d_b_0, ldb, d_b_t, ldb_t );
    // M_t = A_t * B_t
    // C0 += M_t
    // C2 += M_t
    xxcstra_param_pack<value_t, accum_t, epilogue_op_t> xxcstra_pack_3( ms, ns, ks, lda, ldb_t, ldc, k_split0, epilogue_op3, d_a_3, d_b_t, d_c_0, d_c_2 );
    xxcstra_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 1><<< config.grid, config.block, dynamic_smem_bytes, stra_stream[ 0 ] >>>(xxcstra_pack_3);


    // M2 = (1 * d_a_0) * (1 * d_b_1 + -1 * d_b_3);  d_c_1 += 1 * M2;  d_c_3 += 1 * M2;
    // A_t = A0
    //cublasSgeam( g_cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N, ms, ks, &one, d_a_0, lda, &zero, d_a_0, lda, d_a_t, lda_t );
    // B_t = B1 - B3
    cublasSgeam( g_cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N, ks, ns, &one, d_b_1, ldb, &negone, d_b_3, ldb, d_b_t, ldb_t );
    // M_t = A_t * B_t
    // C1 += M_t
    // C3 += M_t
    xxcstra_param_pack<value_t, accum_t, epilogue_op_t> xxcstra_pack_2( ms, ns, ks, lda, ldb_t, ldc, k_split1, epilogue_op3, d_a_0, d_b_t, d_c_1, d_c_3 );
    xxcstra_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 1><<< config.grid, config.block, dynamic_smem_bytes, stra_stream[ 1 ] >>>(xxcstra_pack_2);

    // M5 = (-1 * d_a_0 + 1 * d_a_2) * (1 * d_b_0 + 1 * d_b_1);  d_c_3 += 1 * M5;
    // A_t = A2 - A0
    cublasSgeam( g_cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N, ms, ks, &one, d_a_2, lda, &negone, d_a_0, lda, d_a_t, lda_t );
    // B_t = B0 + B1
    cublasSgeam( g_cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N, ks, ns, &one, d_b_0, ldb, &one, d_b_1, ldb, d_b_t, ldb_t );
    // M_t = A_t * B_t
    // C3 += M_t
    xxcstra_param_pack<value_t, accum_t, epilogue_op_t> xxcstra_pack_5( ms, ns, ks, lda_t, ldb_t, ldc, k_split1, epilogue_op3, d_a_t, d_b_t, d_c_3, NULL );
    xxcstra_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 0><<< config.grid, config.block, dynamic_smem_bytes, stra_stream[ 1 ] >>>(xxcstra_pack_5);

    //printf( "flag5\n" );
    cudaStreamSynchronize( stra_stream[ 0 ] );
    cudaStreamSynchronize( stra_stream[ 1 ] );

    // M4 = (1 * d_a_0 + 1 * d_a_1) * (1 * d_b_3);  d_c_0 += -1 * M4;  d_c_1 += 1 * M4;
    // A_t = A0 + A1
    cublasSgeam( g_cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N, ms, ks, &one, d_a_0, lda, &one, d_a_1, lda, d_a_t, lda_t );
    // B_t = B3
    //cublasSgeam( g_cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N, ks, ns, &one, d_b_3, ldb, &zero, d_b_3, ldb, d_b_t, ldb_t );
    // M_t = A_t * B_t
    // C0 -= M_t
    // C1 += M_t
    xxcstra_param_pack<value_t, accum_t, epilogue_op_t> xxcstra_pack_4( ms, ns, ks, lda_t, ldb, ldc, k_split0, epilogue_op3, d_a_t, d_b_3, d_c_0, d_c_1 );
    xxcstra_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, -1, 1><<< config.grid, config.block, dynamic_smem_bytes, stra_stream[ 0 ] >>>(xxcstra_pack_4);


    // M1 = (1 * d_a_2 + 1 * d_a_3) * (1 * d_b_0);  d_c_2 += 1 * M1;  d_c_3 += -1 * M1;
    // A_t = A2 + A3
    cublasSgeam( g_cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N, ms, ks, &one, d_a_2, lda, &one, d_a_3, lda, d_a_t, lda_t );
    // B_t = B0
    //cublasSgeam( g_cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N, ks, ns, &one, d_b_0, ldb, &zero, d_b_0, ldb, d_b_t, ldb_t );
    // M_t = A_t * B_t
    // C2 += M_t
    // C3 -= M_t
    xxcstra_param_pack<value_t, accum_t, epilogue_op_t> xxcstra_pack_1( ms, ns, ks, lda_t, ldb, ldc, k_split1, epilogue_op3, d_a_t, d_b_0, d_c_2, d_c_3 );
    xxcstra_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, -1><<< config.grid, config.block, dynamic_smem_bytes, stra_stream[ 1 ] >>>(xxcstra_pack_1);

    //printf( "flag8\n" );
    cudaStreamSynchronize( stra_stream[ 0 ] );
    cudaStreamSynchronize( stra_stream[ 1 ] );

    // M0 = (1 * d_a_0 + 1 * d_a_3) * (1 * d_b_0 + 1 * d_b_3);  d_c_0 += 1 * M0;  d_c_3 += 1 * M0;
    // A_t = A0 + A3
    cublasSgeam( g_cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N, ms, ks, &one, d_a_0, lda, &one, d_a_3, lda, d_a_t, lda_t );
    // B_t = B0 + B3
    cublasSgeam( g_cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N, ks, ns, &one, d_b_0, ldb, &one, d_b_3, ldb, d_b_t, ldb_t );
    // M_t = A_t * B_t
    //C0 += M_t; C3 += M_t;
    xxcstra_param_pack<value_t, accum_t, epilogue_op_t> xxcstra_pack_0( ms, ns, ks, lda_t, ldb_t, ldc, k_split0, epilogue_op3, d_a_t, d_b_t, d_c_0, d_c_3 );
    xxcstra_kernel<math_op,block_task_policy_t, TransformA, LdgAlignA, TransformB, LdgAlignB, value_t, accum_t, epilogue_op_t, LdgAlignC, AllowRaggedTiles, 1, 1><<< config.grid, config.block, dynamic_smem_bytes, stra_stream[ 0 ] >>>(xxcstra_pack_0);
    //cublasSgemm( g_cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N, ms, ns, ks, &one, d_a_t, lda_t, d_b_t, ldb_t, &zero, d_m_t, ldm_t );


    //printf( "flag10\n" );
    cudaStreamSynchronize( stra_stream[ 0 ] );
    cudaStreamSynchronize( stra_stream[ 1 ] );

    //printf( "flag11\n" );

    //cudaStreamDestroy( stra_stream[ 0 ] );
    //cudaStreamDestroy( stra_stream[ 1 ] );

    cudaFree( d_a_t );
    cudaFree( d_b_t );
    cudaFree( d_m_t );

