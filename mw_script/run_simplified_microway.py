from sbatch_stra_microway import run_job
from sbatch_stra_microway import get_label
from sbatch_stra_microway import get_routine
import subprocess
import os
import shutil

path_prefix = '/home/jihu/Project/experiment/'

exec_prefix = path_prefix

orig_directory = os.getcwd()

test_exe_dir = path_prefix + 'test6/'

if not os.path.exists(test_exe_dir):
    os.makedirs(test_exe_dir)

# change to the working test_exe_dir
os.chdir( path_prefix )


#1. core number: 1, 2, 4, 8, 16
#2. matrix type: rankk (m=n=8192, k vary from 1024..1024..8192 ), fixk (k=1024, m=n from 1024..1024..20480), square(m=n=k, vary from [256..256..20480] )
#3. range: [256, 256, 16000], 
#4. routine: stra (1level), stra_2level(2level)
#ibrun tacc_affinity ./run_stra_square_4core_mnk_256_256_20480.sh
#ibrun tacc_affinity ./run_mulstra_rankk_2core_mn_8192_k_256_256_8192.sh



# Matrix type: rankk (m=n=16384/15360, k vary from 1024..1024..16384/15360), fixk (k=1024, m=n from 1024..1024..20480), square(m=n=k, vary from [1024..1024..20480] )
# Variation (level + impl): 1/2 level abc, 1/2 level xxx, cutlass
# Arch: P100, V100, Titan-V
# Precision: dp, sp, half
# range: [1024, 1024, 16000], 
## routine: stra (1level), stra_2level(2level)


#level_list=[ 1 ]
level_list=[ 2 ]

copy_executable_flag = True
#test_executable_flag = False
test_executable_flag = True


#matrix_type_list = ['square', 'rankk', 'fixk'] # determine the test_range
matrix_type_list = ['square'] # determine the test_range
#matrix_type_list = ['regular'] # determine the test_range

#test_routine_list = ['test_stra_1level_blis.x', 'test_stra_2level_blis.x', 'test_stra_1levelref_blis.x', 'test_stra_2levelref_blis.x', 'test_mkl_blis.x', 'test_stra_2level2_blis.x', 'test_stra_1level2_blis.x' ]
#test_label_list   = ['stra_1level', 'stra_2level', 'stra_1levelref_par', 'stra_2levelref_par', 'mkl_gemm', 'stra_2level2', 'stra_1level2' ]

#test_range = [ [256,256,20480] ]


#arch_list = ['P100', 'V100', 'V1002', 'Titan-V']
#arch_list = ['P100']
arch_list = ['V100']
#arch_list = ['Titan-V']

#impl_list = ['abc', 'xxx', 'cutlass' ]
#impl_list = ['abc', 'cutlass' ]
impl_list = ['xxx' ]

#precision_list = ['dp', 'sp', 'hp']
precision_list = ['dp', 'sp']
#precision_list = ['hp', 'ip']

# transpose_list = ['nn', 'nt', 'tn', 'tt']

#test_label_list = ['stra1', 'stra2']

#                   'abc', 'xxx', 'cutlass',
#                   'nn',
#                   'P100'
#                   '1-level'
#                   'sp'
#
#'sstra1_nn_sm60_nvcc_9.0', 'dstra2_nn_sm70_nvcc_9.0'
#'strassen-dp.x', 'strassen-sp.x'
#'sgemm_nn_sm60_nvcc_9.0', 'dgemm_nn_sm70_nvcc_9.0'


### put a copy of the executable in the run test_exe_dir to use with core files
#if copy_executable_flag:
#    #for level in level_num_list:
#    #    for pack_type in pack_type_list:
#    #        jobname = 'strassen_' + str(level) + '_' + pack_type
#    #        os.system( 'cp ' + jobname + '/test/test_' + jobname + '.x ' + test_exe_dir )
#    for i in range( len( test_routine_list ) ):
#        test_routine = test_routine_list[i]
#        test_label   = test_label_list[i]
#        os.system( 'cp ' + test_routine + ' ' + test_exe_dir + test_label + '.x' )


# change to the working test_exe_dir
os.chdir( test_exe_dir )

i = 0

arg_list = ['nodes', 'procspernode', 'matrix_type', 'test_routine', 'test_label', 'arch', 'impl', 'level', 'jobname', 'test_exe_dir']

num_nodes=1
procspernode=1


# loop over parameters
for arch in arch_list:
    for impl in impl_list:
        for precision in precision_list:
            for level in level_list:

                if ( impl == 'cutlass' and level >= 2 ):
                    continue

                test_label = get_label( impl, arch, level, precision )
                test_routine = get_routine( impl, arch, level, precision )

                if copy_executable_flag:
                    os.system( 'cp ' + exec_prefix + test_routine + '.x ' + test_exe_dir )

                for matrix_type in matrix_type_list:

                    jobname = test_label + '_' + str( matrix_type )  

                    inputs = [num_nodes, procspernode, matrix_type, test_routine, test_label, arch, impl, level, jobname, test_exe_dir]
        
                    #print len(inputs)
                    
                    input_dict = dict(zip(arg_list, inputs))
                    
                    tacc_script_filename = run_job(input_dict)

                    i = i + 1
        
                    if test_executable_flag:
                        #res = subprocess.Popen('/usr/bin/sbatch ' + tacc_script_filename, shell=True)
                        res = subprocess.Popen('sbatch ' + tacc_script_filename, shell=True)
                        print "res:" + str(res)

                   

# return to the original directory
os.chdir(orig_directory)




