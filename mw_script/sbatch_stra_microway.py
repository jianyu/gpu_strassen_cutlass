# This function prints a batch script to the command line suitable for sending to sbatch
import shutil
import os, sys, stat
import datetime

def get_type_id( matrix_type ):
    if ( matrix_type == 'square' ): return 0
    elif ( matrix_type == 'rankk' ): return 1
    else: # matrix_type == 'fixk'
        return 2

def get_routine_args( impl, matrix_type ):
    if ( impl == 'cutlass' or impl == 'abc' ):
        return '--i=3 --device=0 --alpha=1 --beta=1 --schmoo=20 --shape=' + str( get_type_id( matrix_type ) )
    elif ( impl == 'xxx' ):
        print "Error!"
        return 'Error'
    

def get_gpu_queue( arch ):
    if ( arch == 'P100' ):
        return 'gpu:Tesla-P100-PCIE-16GB:1'
    elif ( arch == 'V100' ):
        return 'gpu:Tesla-V100-PCIE-16GB:1'
    elif ( arch == 'V1002' ):
        return 'gpu:Tesla-V100-PCIE-16GB:1'
    elif ( arch == 'Titan-V' ):
        return 'gpu:TITAN-V:1'
    else:
        print "arch is not supported"
        return "Error"

def get_label( impl, arch, level, precision ):
    if ( impl == 'cutlass' ):
        return arch + '-' + impl + '-' + precision
        #if ( arch == 'P100' ):
        #    if ( precision = 'dp' ):
        #        return 'P100-cutlass-dp'
        #    elif ( precision = 'sp' ):
        #        return 'P100-cutlass-sp'
        #    elif ( precision = 'hp' ):
        #        return 'P100-cutlass-hp'
        #    else:
        #        print ("Precision is not supproted!")
        #        return "Error"
        #elif ( arch == 'V100' ):
        #    print "Not supported"
        #else print
    else: 
        return arch + '-' + str(level) + 'level-' + impl + '-' + precision


def smDict( arch ):
    if ( arch == 'P100' ):
        return 'sm60'
    elif ( arch == 'V100' ):
        return 'sm70'
    elif ( arch == 'V1002' ):
        return 'sm60'
    elif ( arch == 'Titan-V' ):
        return 'sm70'
    else:
        print "arch is not supported"
        return "Error"

def get_routine( impl, arch, level, precision ):
    if ( impl == 'cutlass' ):
        return precision[0] + 'gemm_nn_' + smDict( arch ) + '_nvcc_9.0'
        #return arch + '-' + impl + precision
        #if ( arch == 'P100' ):
        #    if ( precision = 'dp' ):
        #        return 'dgemm_nn_sm60_nvcc_9.0.x'
        #    elif ( precision = 'sp' ):
        #        return 'sgemm_nn_sm60_nvcc_9.0.x'
        #    elif ( precision = 'hp' ):
        #        return 'hgemm_nn_sm60_nvcc_9.0.x'
        #    else:
        #        print ("Precision is not supported!")
        #        return "Error"
        #elif ( arch == 'V100' ):
        #    print "Not supported"
        #else print
    elif ( impl == 'abc' ):
        return precision[0] + impl + 'stra' + str(level) + '_nn_' + smDict( arch ) + '_nvcc_9.0'
    else:  # impl == 'xxx'
        #return arch + '-' + str(level) + 'level-' + impl + precision
        return 'strassen-' + precision




# takes a dictionary of argument value pairs
def run_job(args):
    # number of compute cores per node
    try: 
        num_cores_per_node = args['num_cores_per_node']
    except:
        num_cores_per_node = 20
        #num_cores_per_node = 16

    # allowed time
    try: 
        walltime = args['walltime']
    except:
        walltime = '08:00:00'
        #walltime = '00:30:00'
        #walltime = '00:05:00'
        # walltime = '12:00:00'
        
    # the job queue
    try: 
        queue = args['queue']
    except:
        #queue = 'vis'
        queue = 'gpu'
        # queue = 'normal'

    # flag for email notifications when jobs complete
    #send_email = True
    send_email = False

    # email address for job completion notifications
    useremail='hjyahead@gmail.com'

    # first, get the number of processors, etc.
    # if these aren't specified, we default to one MPI rank on one node and
    # use the shared executable
    try: 
        nodes = args['nodes']
    except:
        nodes = 1
        
    try:
        procpernode = args['procpernode']
    except:
        procpernode = 1 
        
    do_nested = False
    #do_nested = True

    num_threads = num_cores_per_node / procpernode    


    # add a timestamp to the cout file
    timestamp = str(datetime.datetime.now())
    timestamp = timestamp.replace(' ', '.')
    timestamp = timestamp.replace(':', '.')


    # number of MPI processes to run
    npRequested = nodes * procpernode
    
    try:
        my_jobname=args['jobname']
    except:
        my_jobname='strassen'


    my_test_routine = args['test_routine']
    my_test_label   = args['test_label']
    my_matrix_type  = args['matrix_type']
    my_arch         = args['arch']
    my_impl         = args['impl']
    my_level        = args['level']

    my_gpu_queue    = get_gpu_queue( my_arch )

    my_core_num     = 1

    test_exe_dir      = args['test_exe_dir']

    tacc_bash_filename   = test_exe_dir + "run_"  + my_jobname + ".sh"
    tacc_script_filename = test_exe_dir + "mw_" + my_jobname + ".sh"

    my_test_routine_path = test_exe_dir + my_test_routine + '.x' 

    #shutil.copy2( my_test_routine_path, test_exe_dir )

    insert_step_string = "";

    if my_matrix_type == 'square':
        test_range_string  = 'k_start=1024\n'
        #test_range_string  = 'k_start=10752\n'
        #test_range_string  = 'k_start=10240\n'
        #test_range_string += 'k_end=20480\n'
        #test_range_string += 'k_end=12032\n'
        test_range_string += 'k_end=20480\n'
        test_range_string += 'k_blocksize=1024\n'

        my_m = "$k"
        my_k = "$k"
        my_n = "$k"
       
    elif my_matrix_type == 'rankk': #7400, 400, 17000; 200, 200, 16000

        #bound = 16384
        bound = 15360

        test_range_string  = 'k_start=1024\n'
        #test_range_string += 'k_end=16384\n'
        test_range_string += 'k_end=' + str(bound) + '\n'
        test_range_string += 'k_blocksize=1024\n'
        my_m = bound
        my_k = "$k"
        my_n = bound

        #insert_point_list = [ 8192, 10240 ]
        #insert_point_list = [ 9000, 10600 ]
        insert_point_list = [  ]

        for insert_point in insert_point_list:
            insert_step_string += \
"""
    if [ $k -eq {0} ]
    then
        k_blocksize=$(( k_blocksize * 2 ))
    fi\n
""".format(insert_point)
            
    elif my_matrix_type == 'fixk':

        test_range_string  = 'k_start=1024\n'
        test_range_string += 'k_end=20480\n'
        test_range_string += 'k_blocksize=1024\n'
        my_m = "$k"
        my_k = "1024"
        my_n = "$k"
    elif my_matrix_type == 'fixk1440':
        test_range_string  = 'k_start=240\n'
        test_range_string += 'k_end=20000\n'
        test_range_string += 'k_blocksize=240\n'
        my_m = "$k"
        my_k = "1440"
        my_n = "$k"
    elif my_matrix_type == 'fixk1500':
        test_range_string  = 'k_start=240\n'
        test_range_string += 'k_end=20000\n'
        test_range_string += 'k_blocksize=240\n'
        my_m = "$k"
        my_k = "1500"
        my_n = "$k"
    elif my_matrix_type == 'fixk1512':
        test_range_string  = 'k_start=240\n'
        test_range_string += 'k_end=20000\n'
        test_range_string += 'k_blocksize=240\n'
        my_m = "$k"
        my_k = "1512"
        my_n = "$k"
    elif my_matrix_type == 'fixk1548':
        test_range_string  = 'k_start=240\n'
        test_range_string += 'k_end=20000\n'
        test_range_string += 'k_blocksize=240\n'
        my_m = "$k"
        my_k = "1548"
        my_n = "$k"
    elif my_matrix_type == 'regular':
        test_range_string  = 'k_start=0\n'
        test_range_string += 'k_end=0\n'
        test_range_string += 'k_blocksize=1\n'
        my_m = ""
        my_k = ""
        my_n = ""
    else:
        print "wrong branch! my_matrix_type is wrong!"

    if not os.path.exists( test_exe_dir + 'matlab_result' ):
        os.makedirs( test_exe_dir + 'matlab_result' )

    test_exp_string_xxx   = \
"""
echo \"% m n k\tstrassenTime classicTime\t speedup\t strassenGflops classicGflops \" >> {7}/{6}.m
echo \"sb_{4}=[\" >> {7}/{6}.m
for (( k=k_start; k<=k_end; k+=k_blocksize ))
do
    {5}
    {0} {1} {2} {3} 3 0 {8} >> {7}/{6}.m
done
echo \"];\" >> {7}/{6}.m
""".format( my_test_routine_path, my_m, my_n, my_k, my_test_label, insert_step_string, my_jobname, test_exe_dir + 'matlab_result', my_level )


    test_exp_string_abc   = \
"""
echo \"% m n k\tstrassenTime classicTime\t speedup\t strassenGflops classicGflops \" >> {7}/{6}.m
echo \"sb_{4}=[\" >> {7}/{6}.m
for (( k=k_start; k<=k_end; k+=k_blocksize ))
do
    {5}
    {0} --i=3 --device=0 --alpha=1 --beta=1 --m={1} --n={2} --k={3} >> {7}/{6}.m
done
echo \"];\" >> {7}/{6}.m
""".format( my_test_routine_path, my_m, my_n, my_k, my_test_label, insert_step_string, my_jobname, test_exe_dir + 'matlab_result' )



    bash_string   = '#!/bin/bash' + '\n'



    if ( my_impl == 'xxx' ):
        bash_string  += test_range_string + '\n' + test_exp_string_xxx +'\n'

    else:
        bash_string  += test_range_string + '\n' + test_exp_string_abc +'\n'
        #my_routine_args = get_routine_args( my_impl, my_matrix_type )
        #bash_string += my_test_routine_path + ' ' + my_routine_args

    
    # now, create the bash script
    script_string = '#!/bin/bash' + '\n'
    script_string = script_string + '#SBATCH --nodes=1\n'
    script_string = script_string + '#SBATCH --cpus-per-task=20\n'
    script_string = script_string + '#SBATCH --ntasks=1\n'
    #script_string = script_string + '#SBATCH --mem=81920\n'
    script_string = script_string + '#SBATCH --mem=102400\n'
    script_string = script_string + '#SBATCH --gres=' + my_gpu_queue +'\n'

    script_string = script_string + '#SBATCH --time=01:00:00\n'
    #script_string = script_string + '#SBATCH --time=00:01:00\n'
    #script_string = script_string + '#SBATCH --time=00:03:00\n'
    script_string = script_string + '#SBATCH --job-name=' + my_jobname + '\n'

    # there is a limit on the size of filenames, which these might exceed
    my_outfilename = my_jobname + '-%j' + '-' + timestamp
    # replace '/' in the string
    my_outfilename = my_outfilename.replace('/', '.')

    script_string = script_string + '#SBATCH --error=' + 'error_' + my_outfilename + '\n'
    script_string = script_string + '#SBATCH -o ' + 'output_' + my_outfilename + '\n'
    
    if send_email:
        script_string = script_string + '#SBATCH --mail-user=' + useremail + '\n'
        script_string = script_string + '#SBATCH --mail-type=end' + '\n'


    script_string = script_string + 'source /etc/slurm/scripts/slurm.jobstart_messages.sh' + '\n'


    script_string = script_string + 'module --redirect list 2> /dev/null' + '\n'
    script_string = script_string + 'echo; echo;' + '\n'
    script_string = script_string + 'nvidia-smi' + '\n'


    # script_string = script_string + bash_string  + '\n'

    script_string = script_string + tacc_bash_filename + '\n'

    #print bash_string
    #print script_string
    #print tacc_bash_filename
    #print tacc_script_filename

    tacc_bash_file   = open(tacc_bash_filename, 'w')
    tacc_bash_file.write(bash_string)
    tacc_bash_file.close()

    #os.chmod(tacc_bash_filename, stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH )
    os.chmod(tacc_bash_filename, 0770)
    #os.chmod(tacc_bash_filename, stat.S_IRWXG | stat.S_IWGRP | stat.S_IXGRP )
    #os.chmod(tacc_bash_filename, 0777)

    tacc_script_file = open(tacc_script_filename, 'w')
    tacc_script_file.write(script_string)
    tacc_script_file.close()

    os.chmod(tacc_script_filename, 0770)

    return tacc_script_filename

